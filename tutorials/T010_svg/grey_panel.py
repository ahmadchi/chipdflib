from reportlab.platypus import SimpleDocTemplate
from reportlab.platypus.paragraph import *
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus.flowables import ParagraphAndImage, Image
from svglib.svglib import svg2rlg

def scale_svg(fileName):
        #svg_file = '45357.svg'
        image = svg2rlg(fileName)
        # Scale the image.
        scale = 0.1
        image.scale(scale, scale)
        image.width *= scale
        image.height *= scale

        return image

def SVGtoImage(image_path, xsize=50, ysize=50, fitType="fit"):
    drawing = svg2rlg(image_path)
    xL, yL, xH, yH = drawing.getBounds()
    print(xL, yL, xH, yH)
    sx = xsize / (xH - xL)
    sy = ysize / (yH - yL)
    
    drawing.height = ysize
    drawing.width = xsize

    s = sx
    if(sy<sx): s = sy

    drawing.width = (xH - xL) * s
    drawing.height = (yH - yL) * s
    
    drawing.scale(s, s)
    xL, yL, xH, yH = drawing.getBounds()
    print(xL, yL, xH, yH)
    return drawing

def test0():
        "This makes one long multi-page paragraph."

        # Build story.
        story = []
        styleSheet = getSampleStyleSheet()
        para_text = styleSheet['BodyText']
        text = '''If you imagine that the box of X's tothe left is
        an image, what I want to be able to do is flow a
        series of paragraphs around the image
        so that once the bottom of the image is reached, then text will flow back to the
        left margin. I know that it would be possible to something like this
        using tables, but I can't see how to have a generic solution.
        There are two examples of this in the demonstration section of the reportlab
        site.
        If you look at the "minimal" euro python conference brochure, at the end of the
        timetable section (page 8), there are adverts for "AdSu" and "O'Reilly". I can
        see how the AdSu one might be done generically, but the O'Reilly, unsure...
        I guess I'm hoping that I've missed something, and that
        it's actually easy to do using platypus.
        '''
        
        image = scale_svg( '45357.svg')
        # story.append(ParagraphAndImage(Image(gif), Paragraph(text,para_text)))
        description = 'Current Medications'
        # story.append(ParagraphAndImage(Paragraph(description, para_text),image,side='left'))
        ww= 50;
        hh = 50
        image = SVGtoImage( 'progress-report/quality.svg', ww, hh, 'fit')
        story.append(image)

        image = SVGtoImage( 'progress-report/assignment.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/attachment.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/biotech.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/bmi.svg', ww, hh, 'fit')
        story.append(image)

        image = SVGtoImage( 'progress-report/bp.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/bsr.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/calendar.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/flag.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/flag_yellow.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/med-compliance.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/pill.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/pulse.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/quality.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/search.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/temp.svg', ww, hh, 'fit')
        story.append(image)
        image = SVGtoImage( 'progress-report/weight.svg', ww, hh, 'fit')
        story.append(image)


        doc = SimpleDocTemplate('paragraph_and_image_left.pdf')
        doc.multiBuild(story)


test0()