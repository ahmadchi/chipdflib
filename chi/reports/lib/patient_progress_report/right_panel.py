from reportlab.platypus import Table
from reportlab.platypus.flowables import ParagraphAndImage
from datetime import datetime
from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from .styles.message_styles import ProgressReportStyles


def addVitalTable(vitalData):
    styles = ProgressReportStyles()

    data = []
    for vital in vitalData:
        I = SVGtoImage(vital["icon"], 20, 20)
        dtime = datetime.fromtimestamp(vital["date"])
        p1a = Paragraph(str(dtime), styles.p1a)
        p1b = Paragraph(vital['title'], styles.p1b)
        p1c1 = Paragraph(str(vital['value']), styles.p1c1)
        p1c2 = Paragraph(vital['unit'], styles.p1c2)

        data.append([I, [p1b, p1a], [p1c1, p1c2]])

    t = Table(data, style=[
        # ('GRID',(0,0),(3,3),1,colors.green)
    ],
              colWidths=[30, 100, 80]
              )
    return t


def addMedCompliance1(report, data, styles, type='Medicine'):
    if 'compliance_data' in data:
        if type in data['compliance_data']:
            medCompliance = data['compliance_data'][type]
            if len(medCompliance) > 0:
                path = report.dir_path + '../patient_progress_report/images/progress-report/search.svg'
                image = SVGtoImage(path, 20, 20, 'fit')
                report.story.append(
                    ParagraphAndImage(Paragraph(type + " Compliance", styles.activeMedsH1), image, side='left'))
                compliance = "Not Available"
                currentComp = str(medCompliance['current'])
                diffCompliance = str(medCompliance['diff'])
                if currentComp != "999":
                    compliance = currentComp + "%  "
                if diffCompliance != "999":
                    if (medCompliance['diff'] >= 0):
                        diffCompliance = "(+" + diffCompliance + ")"
                    else:
                        diffCompliance = "(" + diffCompliance + ")"

                    compliance += diffCompliance + "%"

                report.appendPara(compliance, styles.activeMedsH1Comp)


def add_surgeries(report, data, styles):
    surgeries = data['surgeries']
    if surgeries is not None:
        assert isinstance(surgeries, list), "Surgeries should be type of Array"
    if len(surgeries) > 0:
        path = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
        image = SVGtoImage(path, 25, 25, 'fit')
        report.story.append(ParagraphAndImage(Paragraph("Surgeries", styles.activeMedsH1), image, side='left'))
        count = 1

        for item in surgeries:
            report.appendPara(str(count) + ". " + item, styles.activeDiagnosis)
            count += 1
        report.appendPara("__________________________________________", styles.activeDiagnosis)


def addRightPanel(report, data, styles):
    path = report.dir_path + '../patient_progress_report/images/progress-report/search.svg'
    image = SVGtoImage(path, 20, 20, 'fit')
    if len(data['diagnoses']) > 0:
        report.story.append(ParagraphAndImage(Paragraph("Diagnosis", styles.activeMedsH1), image, side='left'))
    count = 1
    if data["diagnoses"] is not None:
        assert isinstance(data["diagnoses"], list), "diagnoses should be type of Array"
    for diagnosis in data["diagnoses"]:
        report.appendPara(str(count) + ". " + diagnosis, styles.items)
        count += 1
    if len(data['diagnoses']) > 0:
        report.appendPara("__________________________________________", styles.activeDiagnosis)
    recent_result = data["recent_result"]
    if recent_result is not None:
        assert isinstance(data["recent_result"], list), "recent_result should be type of Array"
    if len(recent_result) > 0:
        path = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
        image = SVGtoImage(path, 25, 25, 'fit')
        report.story.append(ParagraphAndImage(Paragraph("Vitals", styles.activeMedsH1), image, side='left'))
        vitalTable = addVitalTable(recent_result)
        report.story.append(vitalTable)
        report.appendPara("__________________________________________", styles.activeDiagnosis)
    add_surgeries(report, data, styles)
    allergies = data['allergies']
    if allergies is not None:
        assert isinstance(allergies, list), "messages should be type of Array"
    if len(allergies) > 0:
        path = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
        image = SVGtoImage(path, 25, 25, 'fit')
        report.story.append(ParagraphAndImage(Paragraph("Allergies", styles.activeMedsH1), image, side='left'))
        count = 1

        for item in allergies:
            report.appendPara(str(count) + ". " + item, styles.activeDiagnosis)
            count += 1
        report.appendPara("__________________________________________", styles.activeDiagnosis)
