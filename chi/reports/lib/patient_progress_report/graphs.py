from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Paragraph, Table
from reportlab.platypus.flowables import ParagraphAndImage
from datetime import datetime
from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Paragraph
from chi.reports.lib.core.utils import plot_utils as utils
from .progress_report import patientHeader, titlePageAnnual
from .styles.message_styles import ProgressReportStyles
from reportlab.lib import colors

from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle





def addVitalGraphDataTable(vitalGraphData):
    return
    styles = ProgressReportStyles()
    styleSheet = getSampleStyleSheet()

    data = []
    data1 = []

    arr = ["min", "mean", "max"]
    for item in arr:
        a = vitalGraphData["data"]["summary"][item].split("/")
        if (len(a) < 2): continue
        a1 = "<font color=orange>" + a[0] + " /""</font>"
        a2 = "<font color=blue>" + " " + a[1] + "</font>"
        p1a = Paragraph(item.title() + ": " + a1 + a2, styles.p1d)
        data1.append(p1a)


    val1 = vitalGraphData["data"]["summary"]["readings"][0]["val"].split("/")

    if (len(val1)) < 2: return None
    val1a = "<font color=orange>" + val1[0] + " /""</font>"
    val1b = "<font color=blue>" + " " + val1[1] + "</font>"

    val2 = vitalGraphData["data"]["summary"]["readings"][1]["val"].split("/")
    val2a = "<font color=orange>" + val2[0] + " /""</font>"
    val2b = "<font color=blue>" + " " + val2[1] + "</font>"

    p1d = Paragraph("Last Readings", styles.p1d)
    unit = vitalGraphData["data"]["summary"]["unit"]
    p1e = Paragraph(val1a + val1b + " " + unit, styles.p1d)
    p1f = Paragraph(val2a + val2b + " " + unit, styles.p1d)

    data.append([data1[0], data1[1], data1[2]])
    data.append([p1d, p1e, p1f])

    type0 = vitalGraphData["data"]["summary"]["readings"][0]["type"]
    type1 = vitalGraphData["data"]["summary"]["readings"][1]["type"]

    if ((type0 == "Warning") and (type1 == "Warning")):
        grid = ('GRID', (1, 1), (3, 1), 1, colors.orange)
    elif (type0 == "Warning"):
        grid = ('GRID', (1, 1), (1, 1), 1, colors.orange)
    elif (type1 == "Warning"):
        grid = ('GRID', (2, 1), (2, 1), 1, colors.orange)
    else:
        grid = ('GRID', (0, 0), (3, 1), 0, colors.lavender)

    t = Table(data,
              style=[grid,
                     ('BACKGROUND', (0, 0), (3, 1), colors.lavender)],
              colWidths=[100, 110, 110])
    return t


def addVitalGraph(report, data, styles, path):
    ww = 270
    hh = 200
    graph_icon = report.dir_path + '../patient_progress_report/images/progress-report/bsr.svg'
    icon = SVGtoImage(graph_icon, 15, 15, 'fit')
    # print(image)


    config = utils.getDefaultConfig()
    config['savePath'] = path
    plt_data = utils.prepareDataForVitalsGraphNew(data['vitalGraphDataNew'])
    for gData in plt_data:
        graphTitle = ""
        # for key in gData["keys"]:
        #     graphTitle += key + " "
        graphTitle = gData["title"]
        graph_title = ParagraphAndImage(Paragraph(graphTitle, styles.activeMedsH1), icon, side='left')
        report.story.append(graph_title)
        graph_title.keepWithNext = True
        # report.story[-1].keepWithNext
        utils.plotDataNew(gData, config)
        image = SVGtoImage(config['savePath'], ww, hh, 'fit')
        image.hAlign = 'CENTER'
        report.story.append(image)






def addComplianceGraph(report, data, styles, path):
    if data["complianceData"] is None or len(data["complianceData"]) == 0:
        return

    ww = 320
    hh = 200
    image = SVGtoImage(path, 15, 15, 'fit')

    graphTitle = "Medication Compliance"
    report.story.append(ParagraphAndImage(Paragraph(graphTitle, styles.activeMedsH1), image, side='left'))

    config = utils.getDefaultConfig()
    config['savePath'] = path
    plt_data = utils.prepareDataForComplianceGraph(data["complianceData"])
    utils.plotData(plt_data, config)

    image = SVGtoImage(config['savePath'], ww, hh, 'fit')
    image.hAlign = 'CENTER'

    report.story.append(image)



# GRAPH
def addGraphs(report, data, styles):
    plot_file_path = data.get('plot_file_path', None)
    vitalGraphData = data["vitalGraphDataNew"]

    if None not in [vitalGraphData, plot_file_path]:
        assert isinstance(vitalGraphData, list), "vitalGraphData should be type of List"

        if len(data["vitalGraphDataNew"]) > 0:
            addVitalGraph(report, data, styles, plot_file_path)
            report.appendPara("                ", styles.activeMedsH1)
            t1 = addVitalGraphDataTable(vitalGraphData)
            if t1 is not None:
                report.story.append(t1)

    if None not in [data["complianceData"], plot_file_path]:
        assert isinstance(data["complianceData"], list), "complianceData should be type of Array"
        addComplianceGraph(report, data, styles, plot_file_path)
