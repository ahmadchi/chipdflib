import json

from datetime import datetime

from chi.reports.lib.core.chi_image import SVGtoImage
from chi.reports.lib.core.chi_rect_graph_bar import CHIRectGraphBar

from reportlab.platypus import Paragraph, Spacer, Table, TableStyle
from reportlab.platypus.flowables import ParagraphAndImage


# from math import ceil

# from decimal import Decimal, ROUND_HALF_UP, ROUND_HALF_DOWN

# use in rounding floating numbers
#Decimal(str(655.665)).quantize(Decimal('1.11'), rounding=ROUND_HALF_UP)

#c = CHIUtils()

def preprocessing_dosage_time(report, medicines, styles):
    table_data = [[]]
    dd = " ("
    tFlag = 0

    for schedules in medicines['schedule']['days']:
        for times in schedules['times']:
            grey_time_font = "<font color=white>" + str(report.chiUtils.time_from_timestamp(times['time'])) + "</font>"
            time = Paragraph(grey_time_font, styles.time__)
            dx = str(times['dosage'])
            if tFlag == 0:
                tFlag = 1
                dd += dx
            else:
                dd += " + " + dx


            dosage = Paragraph(dx, styles.dosage__)
            if "day" in schedules:
                day = schedules['day']
                dosage = Paragraph(str(times['dosage']) + "-" + str(day), styles.dosage__)
            table_data[0].append([[dosage, time]])
    t1 = Table(table_data, colWidths=[35])
    t1.hAlign = 'LEFT'
    dd += ")"
    return dd

# def preprocessing_dosage_time(report, medicines, styles):
#     table_data = [[]]
#     for schedules in medicines['schedule']['days']:
#         for times in schedules['times']:
#             grey_time_font = "<font color=white>" + str(report.chiUtils.time_from_timestamp(times['time'])) + "</font>"
#             time = Paragraph(grey_time_font, styles.time__)
#             dosage = Paragraph(str(times['dosage']), styles.dosage__)
#             if "day" in schedules:
#                 day = schedules['day']
#                 dosage = Paragraph(str(times['dosage']) + "-" + str(day), styles.dosage__)
#             table_data[0].append([[dosage, time]])
#     t1 = Table(table_data, colWidths=[35])
#     t1.hAlign = 'LEFT'
#     return t1


# def processActiveMedications1(report, data, styles):
#     count = 0

#     for medicines in data:
#         count += 1
#         name = medicines["name"]
#         #med_strength = "<font fontSize = 6>" + str(medicines["strength"]) + "</font>"
#         med_strength = str(medicines["strength"]) 
#         medicine_names = Paragraph(str(count) + "-" + name + " " + med_strength, styles.dosage)
#         report.story.append(medicine_names)
#         frequency = Paragraph(medicines['schedule']["frequency"], styles.frequency)
#         start_date = datetime.fromtimestamp(medicines['schedule']["start_date"])
#         current_medication_date = start_date.strftime("%d-%m-%Y")
#         # print("current_medication_date[0]", current_medication_date[0])
#         medicine_start_date = Paragraph(current_medication_date, styles.current_medication__start_date)
#         table_data = [
#             [
#                 [ medicine_start_date],
#                 [frequency],

#             ],

#         ]
#         t2 = Table(table_data, colWidths=[125])
#         t2.hAlign = 'LEFT'
#         t1 = preprocessing_dosage_time(report, medicines, styles)
#         data = [(t2, t1)]
#         t = Table(data, [180, 100])
#         t.hAlign = "LEFT"
#         report.story.append(t)
#         separator = Paragraph("_" * 35, styles.current_medication_spacer)
#         report.story.append(separator)



def processActiveMedications(report, data, styles):
    count = 0

    for medicines in data:
        count += 1
        name = medicines["name"]
        med_strength = str(medicines["strength"]) 
        medicine_name = str(count) + " - " + name + " " + med_strength #, styles.dosage)
        start_date = datetime.fromtimestamp(medicines['schedule']["start_date"])
        current_medication_date = start_date.strftime("%d-%m-%Y")
        frequency = medicines['schedule']["frequency"]


        
        t = preprocessing_dosage_time(report, medicines, styles)
        # data = [(t2, t1)]
        # t = Table(data, [180, 100])
        # t.hAlign = "LEFT"
        # var = medicine_name + " "+frequency +""+current_medication_date+ " "+ t
        medicine_nameP = Paragraph(medicine_name + " "+ t, styles.actMeds)
        report.story.append(medicine_nameP)

        var = frequency +"--  starting date:"+current_medication_date
        tp = Paragraph(var, styles.actMeds1)
        report.story.append(tp)


    tp = Paragraph("  ", styles.actMeds1)
    report.story.append(tp)
    tp = Paragraph("  ", styles.actMeds1)
    report.story.append(tp)





        # separator = Paragraph("_" * 35, styles.current_medication_spacer)
        # report.story.append(separator)


def addActiveMedications(report, data, styles):
    """
    Current Medications
    """
    activeMedicationsData = data['active_medications']
    if activeMedicationsData == []:
        return

    for activeMedications in activeMedicationsData:
        if activeMedications == {} or None:
            return

    path1 = report.dir_path + '../patient_progress_report/images/progress-report/pill.svg'
    image1 = SVGtoImage(path1, 10, 10, 'fit')
    currMedTitle = ParagraphAndImage(Paragraph("Current Medication", styles.activeMedications), image1, side='left')
    report.story.append(currMedTitle)

    if activeMedicationsData is not None:
        assert isinstance(activeMedicationsData, list), "active_medications should be type of Array"
        processActiveMedications(report, activeMedicationsData, styles)
    elif not activeMedicationsData:
        return

    """
    Compliance Bar Graphs
    """

    path2 = report.dir_path + '../patient_progress_report/images/progress-report/med-compliance.svg'
    image2 = SVGtoImage(path2, 10, 10, 'fit')
    medCompTitle = ParagraphAndImage(Paragraph("Medication Compliance ", styles.activeMedsH1), image2, side='left')

    if 'compliance_data' in data:
        homecare_compliance = ParagraphAndImage(Paragraph("Homecare Compliance ", styles.activeMedsH1), image2,
                                                side='left')
        medComp = data['compliance_data']['Medicine']
        vitComp = data['compliance_data']['Vital']
        if medComp == 999:
            medComp = 0
        #
        # med_compliance_percnt = (Decimal(str(medComp)).quantize(Decimal('1.5')))
        # vital_compliance_percnt = (Decimal(str(vitComp)).quantize(Decimal('1.5')))

        med_compliance_percnt = round(medComp,1)
        vital_compliance_percnt = round(vitComp,1)

        #print(med_compliance_percnt)
        #print(vital_compliance_percnt)

        medicine_compliance_bar = CHIRectGraphBar(percentage=med_compliance_percnt)
        vital_compliance_bar = CHIRectGraphBar(percentage=vital_compliance_percnt)
        space = Spacer(0, 1)

    else:
        report.appednPara("Medication Compliance ", styles.activeMedsH1)

    compliance_table_data = [
        [medCompTitle, homecare_compliance],
        [medicine_compliance_bar, vital_compliance_bar],
        [space]

    ]

    compliance_table = Table(compliance_table_data, colWidths=[170, 170])
    compliance_table.hAlign = 'LEFT'
    report.story.append(compliance_table)
    # exit()
