from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Paragraph, Table
from reportlab.platypus.flowables import ParagraphAndImage
from datetime import datetime
import arabic_reshaper
from bidi.algorithm import get_display


def processMessages(report, msgData, styles):
    report.appendPara(msgData["title"], styles.message_subject, keepWithNext=True)
    # return
    for msg in msgData["messages"]:
        dtime = datetime.fromtimestamp(msg["time"])
        date_and_time = str(datetime.fromtimestamp(msg["time"])).split(" ")
        dtime = date_and_time[1]
        time = dtime[:5]


        if msg['title'] == "Medication Change":
            print("======================================", date_and_time)
            print(msg['message']['medicine_name'] + msg['message']['strength'])

            continue

        time = "<font fontSize=8>" + time + "</font>"
        time_1 = "<font color=gray>" + str(time) + "</font>"
        patient_fullname = "<font fontSize=10>" + msg["full_name"] + "</font>"
        if (msg["full_name"] == "Command  Center"):
            print("skipping message ......")
            continue

        if msg["group_type"] == "Patient":
            report.appendPara(patient_fullname + " " + time_1, styles.message_to, keepWithNext=True)
            msggg = msg["message"]
            # print(msggg)
            arabic_text = arabic_reshaper.reshape(msggg)  # join characters
            msggg = get_display(arabic_text)  # change orientation by using bidi

            report.appendPara(msggg, styles.message_body_to, keepWithNext=False)
        else:
            report.appendPara(patient_fullname + " (" + msg["group_type"] + ")" + " " + time_1, styles.message_from,
                              keepWithNext=True)
            msggg = msg["message"]
            arabic_text = arabic_reshaper.reshape(msggg)  # join characters
            msggg = get_display(arabic_text)  # change orientation by using bidi
            report.appendPara(msggg, styles.message_body_from, keepWithNext=False)

# Function not yet tested. Usage not prefered 
def appendTable(report, table_data, show_boundry = None):
    
    t = Table(table_data, colWidths=[110, 160])
    # rowHeights=None, style=None,
    #             repeatRows=0, repeatCols=0, splitByRow=1, emptyTableAction=None, ident=None,
    #             hAlign=None, vAlign=None, normalizedData=0, cellStyles=None, rowSplitRange=None,
    #             spaceBefore=None, spaceAfter=None
    t.hAlign = 'LEFT'
    if show_boundry is True:
        t.setStyle([('GRID', (0, 0), (-1, -1), 1, colors.black),
                    ])
    report.story.append(t)


def addMessages(report, data, styles):
    messages = data["messages"]
    if messages is not None:
        assert isinstance(messages, list), "messages should be type of Array"
        for dateWiseMessages in messages:
            date_and_time = str(datetime.fromtimestamp(dateWiseMessages["date"])).split(" ")
            dtime = date_and_time[0]

            report.appendPara(dtime, styles.message_date, keepWithNext=True)

            for msg in dateWiseMessages["medicines"]:
                table_data = []
                if msg["type"] == "update":
                    med_name = "<font color=white>" + msg["medicine_name"] + "</font>"
                    added_meds = Paragraph("Medication Added :" + " ", styles.active_medicine_name)
                    medication_names = Paragraph(med_name, styles.active_medicine)
                    table_data.append([added_meds, medication_names])
                else:
                    medc_name = "<font color=white>" + "<strike>" + msg["medicine_name"] + "</strike>" + "</font>"
                    medication_names = Paragraph(medc_name, styles.cancelled_medicine)
                    cancelled_meds = Paragraph("Cancelled:" + " ", styles.cancelled_medicine_name)
                    table_data.append([[cancelled_meds], [medication_names]])

                appendTable(report, table_data, show_boundry=False)

            for msg in dateWiseMessages["subjects"]:
                processMessages(report, msg, styles)
