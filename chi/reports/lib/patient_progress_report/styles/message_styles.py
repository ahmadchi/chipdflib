from reportlab.lib.colors import white, black, grey, Color, yellow
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY
from reportlab.lib import colors


class ProgressReportStyles():
    def __init__(self):
        styles = getSampleStyleSheet()

        self.level1 = ParagraphStyle('level 1',
                                     # spaceBefore = 0,
                                     # spaceAfter=0,
                                     parent=styles['Normal'],
                                     # lineSpacing = 0,
                                     leftIndent=30,

                                     leading=12. * 1.2,
                                     fontSize=12)

        self.items = ParagraphStyle('yourtitle1',
                                    parent=styles['Normal'],
                                    leftIndent=30,
                                    leading=12. * 1.2,
                                    fontSize=12)

        self.itemsSeparator = ParagraphStyle('yourtitle1',
                                             spaceBefore=10,
                                             spaceAfter=10,
                                             parent=styles['Normal'],
                                             leftIndent=30,
                                             fontSize=12)

        self.p1a = ParagraphStyle('yourtitle1',
                                  spaceBefore=0,
                                  spaceAfter=0,
                                  parent=styles['Normal'],
                                  lineSpacing=0,
                                  fontSize=8)
        self.p1b = ParagraphStyle('yourtitle',
                                  fontName="Helvetica-Bold",
                                  parent=styles['Normal'],
                                  spaceBefore=0,
                                  spaceAfter=0,
                                  lineSpacing=0,
                                  fontSize=10)

        self.p1c1 = ParagraphStyle('yourtitle',
                                   fontName="Helvetica-Bold",
                                   parent=styles['Normal'],
                                   spaceBefore=0,
                                   spaceAfter=0,
                                   lineSpacing=0,
                                   fontSize=10,
                                   alignment=TA_RIGHT
                                   )

        self.p1c2 = ParagraphStyle('yourtitle1',
                                   spaceBefore=0,
                                   spaceAfter=0,
                                   parent=styles['Normal'],
                                   lineSpacing=0,
                                   fontSize=8,
                                   alignment=TA_RIGHT)

        self.p1d = ParagraphStyle('yourtitle',
                                  fontName="Helvetica-Bold",
                                  parent=styles['Normal'],
                                  spaceBefore=0,
                                  spaceAfter=0,
                                  lineSpacing=0,
                                  alignment=TA_CENTER,
                                  # fontColor='#FF0000',
                                  fontSize=10,
                                  # textColor=Color(0,0,0,1)
                                  )

        self.activeMeds = ParagraphStyle('yourtitle',
                                         fontName="Helvetica-Bold",
                                         parent=styles['Normal'],
                                         fontSize=8)

        self.current_medication = ParagraphStyle('yourtitle',
                                                 fontName="Helvetica",
                                                 parent=styles['Normal'],
                                                 spaceAfter=2,
                                                 spaceBefore=2,
                                                 fontSize=9)

        self.activeDiagnosis = ParagraphStyle('yourtitle',
                                              fontName="Helvetica",
                                              parent=styles['Normal'],
                                              leftIndent=20,
                                              fontSize=8)
        self.dosage = ParagraphStyle('yourtitle1',
                                     fontName="Helvetica",
                                     parent=styles['Normal'],
                                     fontSize=9,
                                     spaceAfter=0,
                                     alignment=TA_LEFT
                                     )
        self.current_medication__start_date = ParagraphStyle('yourtitle1',
                                                             fontName="Helvetica",
                                                             parent=styles['Normal'],
                                                             fontSize=9,
                                                             spaceAfter=0,
                                                             alignment=TA_CENTER,
                                                             backColor="#D3D3D3",
                                                             rightIndent=50,
                                                             lefttIndent=50,

                                                             )
        self.dosage__ = ParagraphStyle('yourtitle1',
                                       fontName="Helvetica",
                                       parent=styles['Normal'],
                                       fontSize=7,
                                       spaceAfter=0,
                                       alignment=TA_CENTER,
                                       leading=10,
                                       )
        self.dosage__prev = ParagraphStyle('yourtitle1',
                                           fontName="Helvetica",
                                           parent=styles['Normal'],
                                           fontSize=7,
                                           spaceAfter=0,
                                           alignment=TA_CENTER,
                                           leading=10,
                                           )
        self.time__ = ParagraphStyle('yourtitle1',
                                     fontName="Helvetica",
                                     parent=styles['Normal'],
                                     fontSize=7,
                                     backColor='#696969',
                                     spaceAfter=0,
                                     # rightIndent=50,
                                     # lefttIndent=50,
                                     alignment=TA_CENTER
                                     )
        self.time__prev = ParagraphStyle('yourtitle1',
                                         fontName="Helvetica",
                                         parent=styles['Normal'],
                                         fontSize=7,
                                         backColor='#696969',
                                         spaceAfter=0,
                                         alignment=TA_JUSTIFY
                                         )
        self.strength = ParagraphStyle('yourtitle1',
                                       fontName="Helvetica",
                                       parent=styles['Normal'],
                                       fontSize=7,
                                       spaceAfter=70
                                       )
        self.frequency = ParagraphStyle('yourtitle1',
                                        fontName="Helvetica",
                                        parent=styles['Normal'],
                                        fontSize=10,
                                        # spaceAfter=70
                                        )
        self.activeMedications = ParagraphStyle('yourtitle1',
                                                fontName="Helvetica",
                                                parent=styles['Normal'],
                                                fontSize=10,
                                                spaceAfter=5
                                                )
        self.activeMedications = ParagraphStyle('yourtitle1',
                                                fontName="Helvetica-Bold",
                                                parent=styles['Normal'],
                                                fontSize=12,
                                                )

        self.listitem_medications = ParagraphStyle('yourtitle1',
                                                   fontName="Helvetica-Bold",
                                                   parent=styles['Normal'],
                                                   fontSize=9,
                                                   )

        self.listitem_escalations = ParagraphStyle('yourtitle1',
                                                   fontName="Helvetica-Bold",
                                                   parent=styles['Normal'],
                                                   fontSize=9,
                                                   # rightIndent = 7
                                                   # spaceAfter=1
                                                   )

        self.activeMedsH1 = ParagraphStyle('yourtitle1',
                                           fontName="Helvetica-Bold",
                                           parent=styles['Normal'],
                                           fontSize=12,
                                           spaceAfter=5
                                           )
        self.esc_msg_heading = ParagraphStyle('yourtitle1',
                                              fontName="Helvetica-Bold",
                                              parent=styles['Normal'],
                                              fontSize=10,
                                              leftIndent=0,
                                              rightIndent=10,
                                              # spaceAfter=5
                                              )

        self.med_bullet = ParagraphStyle('yourtitle1',
                                         fontName="Helvetica-Bold",
                                         parent=styles['Normal'],
                                         fontSize=9,
                                         # leftIndent=7,
                                         rightIndent=97,
                                         borderRadius=8,
                                         borderColor="#808000",
                                         backColor='#808000',
                                         borderWidth=1.5,
                                         borderPadding=2
                                         )
        self.med_bullet2 = ParagraphStyle('yourtitle1',
                                          fontName="Helvetica-Bold",
                                          parent=styles['Normal'],
                                          fontSize=9,
                                          # leftIndent=6,
                                          # rightIndent=75,
                                          borderRadius=8,
                                          borderColor="#808000",
                                          backColor='#808000',
                                          borderWidth=1.5,
                                          borderPadding=2,
                                          alignment=TA_CENTER,
                                          )
        self.med_bullet_subject = ParagraphStyle('yourtitle1',
                                                 fontName="Helvetica-Bold",
                                                 parent=styles['Normal'],
                                                 fontSize=9,
                                                 # leftIndent=6,
                                                 # rightIndent=75,
                                                 borderRadius=8,
                                                 borderColor="#FF8C00",
                                                 backColor='#FF8C00',
                                                 borderWidth=1.5,
                                                 borderPadding=2,
                                                 alignment=TA_CENTER,
                                                 )
        self.med_change_bullet = ParagraphStyle('yourtitle1',
                                                 fontName="Helvetica-Bold",
                                                 parent=styles['Normal'],
                                                 fontSize=9,
                                                 borderRadius=8,
                                                 borderColor="#808000",
                                                 backColor='#808000',
                                                 borderWidth=1.5,
                                                 borderPadding=2,
                                                 alignment=TA_CENTER,
                                                 )
        self.escalations = ParagraphStyle('yourtitle1',
                                          fontName="Helvetica-Bold",
                                          parent=styles['Normal'],
                                          fontSize=12,
                                          rightIndent=100,
                                          )
        self.subjects_under_graph = ParagraphStyle('yourtitle1',
                                                   fontName="Helvetica",
                                                   parent=styles['Normal'],
                                                   fontSize=9,
                                                   spaceAfter=5,
                                                   alignment=TA_LEFT,
                                                   # backColor = '#FF8C00'
                                                   )
        self.subjectsss = ParagraphStyle('yourtitle1',
                                                   fontName="Helvetica",
                                                   parent=styles['Normal'],
                                                   fontSize=10,
                                                   spaceAfter=5,
                                                   alignment=TA_LEFT,
                                                   # backColor = '#FF8C00'
                                                   )
        self.subjects_date = ParagraphStyle('yourtitle1',
                                            fontName="Helvetica",
                                            parent=styles['Normal'],
                                            fontSize=10,
                                            spaceAfter=5,
                                            alignment=TA_LEFT,
                                            )
        self.surgeries = ParagraphStyle('yourtitle1',
                                        fontName="Helvetica",
                                        parent=styles['Normal'],
                                        fontSize=10,
                                        spaceAfter=5
                                        )
        self.current_medication_spacer = ParagraphStyle('yourtitle1',
                                                        fontName="Helvetica-Bold",
                                                        parent=styles['Normal'],
                                                        fontSize=12,
                                                        spaceAfter=10,
                                                        alignment=TA_CENTER,
                                                        )

        self.activeMedsH1Comp = ParagraphStyle('yourtitle1',
                                               fontName="Helvetica-Bold",
                                               parent=styles['Normal'],
                                               fontSize=12,
                                               spaceAfter=5,
                                               leftIndent=35
                                               )

        self.message_date = ParagraphStyle('yourtitle',
                                           fontName="Helvetica-Bold",
                                           fontSize=10,
                                           backColor='#FFFFFF',
                                           borderRadius=1,
                                           borderColor=None,
                                           borderWidth=1,
                                           borderPadding=5,
                                           parent=styles['Normal'],
                                           alignment=TA_LEFT,
                                           spaceBefore=15,
                                           spaceAfter=15,
                                           textcolor='#FFFFFF')
        self.update_medicine = ParagraphStyle('yourtitle',
                                              fontName="Helvetica-Bold",
                                              fontSize=10,
                                              leading=15,
                                              leftIndent=-8,
                                              rightIndent=10,
                                              backColor='#898989',
                                              borderRadius=0,
                                              borderColor=None,
                                              borderWidth=1,
                                              borderPadding=1,
                                              parent=styles['BodyText'],
                                              alignment=TA_LEFT,
                                              )
        self.update_medicine_label = ParagraphStyle('yourtitle',
                                                    fontName="Helvetica-Bold",
                                                    fontSize=10,
                                                    leading=13,
                                                    # leftIndent=10,
                                                    # rightIndent=10,
                                                    backColor='#898989',
                                                    borderRadius=0,
                                                    borderColor=None,
                                                    borderWidth=1,
                                                    borderPadding=1,
                                                    parent=styles['Normal'],
                                                    alignment=TA_CENTER,
                                                    )
        self.active_medicine_label = ParagraphStyle('yourtitle',
                                                    fontName="Helvetica-Bold",
                                                    fontSize=10,
                                                    leading=13,
                                                    # leftIndent=1,
                                                    # rightIndent=1,
                                                    # spaceAfter=7,
                                                    backColor='#32CD32',
                                                    borderRadius=0,
                                                    borderColor=None,
                                                    borderWidth=1,
                                                    borderPadding=1,
                                                    parent=styles['BodyText'],
                                                    alignment=TA_CENTER,
                                                    )

        self.cancelled_medicine_label = ParagraphStyle('yourtitle',
                                                       fontName="Helvetica-Bold",
                                                       fontSize=10,
                                                       leading=13,
                                                       # leftIndent=8,
                                                       # rightIndent=8,
                                                       backColor='#8B0000',
                                                       borderRadius=0,
                                                       borderColor=None,
                                                       borderWidth=1,
                                                       borderPadding=1,
                                                       parent=styles['BodyText'],
                                                       alignment=TA_CENTER,
                                                       )

        self.simple_label = ParagraphStyle('yourtitle',
                                           fontName="Helvetica-Bold",
                                           fontSize=7,
                                           backColor = None,
                                           # parent=styles['BodyText'],
                                           alignment=TA_JUSTIFY,
                                           leftIndent=120,
                                           rightIndent=80,
                                           )

        self.hard_coded_esc_table = ParagraphStyle('yourtitle',
                                           fontName="Helvetica-Bold",
                                           fontSize=7,
                                           # backColor = None,
                                           # parent=styles['BodyText'],
                                           alignment=TA_LEFT,
                                           # leftIndent=100,
                                           # rightIndent=80,
                                           )

        self.completed_medicine_label = ParagraphStyle('yourtitle',
                                                       fontName="Helvetica-Bold",
                                                       fontSize=10,
                                                       leading=13,
                                                       # leftIndent=10,
                                                       # rightIndent=10,
                                                       backColor='#8A2BE2',
                                                       borderRadius=0,
                                                       borderColor='#8A2BE2',
                                                       borderWidth=1,
                                                       borderPadding=1,
                                                       parent=styles['BodyText'],
                                                       alignment=TA_CENTER,
                                                       )
        self.completed_medicine = ParagraphStyle('yourtitle',
                                                 fontName="Helvetica-Bold",
                                                 fontSize=10,
                                                 leading=15,
                                                 leftIndent=-8,
                                                 rightIndent=10,
                                                 backColor='#8A2BE2',
                                                 borderRadius=0,
                                                 borderColor='#8A2BE2',
                                                 borderWidth=1,
                                                 borderPadding=3,
                                                 parent=styles['BodyText'],
                                                 alignment=TA_LEFT,
                                                 )
        self.sch_change = ParagraphStyle('yourtitle',
                                         fontName="Helvetica",
                                         fontSize=10,
                                         leading=15,
                                         parent=styles['BodyText'],
                                         alignment=TA_LEFT,
                                         leftIndent=-8,
                                         rightIndent=30,
                                         backColor='#A9A9A9',
                                         borderRadius=0,
                                         borderColor='#A9A9A9',
                                         borderWidth=1,
                                         borderPadding=2,
                                         )

        self.completed2 = ParagraphStyle('yourtitle',
                                         fontName="Helvetica",
                                         fontSize=10,
                                         leading=15,
                                         parent=styles['BodyText'],
                                         alignment=TA_LEFT,
                                         leftIndent=-8,
                                         rightIndent=10,
                                         backColor='#8A2BE2',
                                         borderRadius=0,
                                         borderColor=None,
                                         borderWidth=1,
                                         borderPadding=3,
                                         )

        self.cancelled2 = ParagraphStyle('yourtitle',
                                         fontName="Helvetica",
                                         fontSize=10,
                                         leading=15,
                                         parent=styles['BodyText'],
                                         alignment=TA_LEFT,
                                         leftIndent=-8,
                                         rightIndent=10,
                                         backColor='#FF3333',
                                         borderRadius=0,
                                         borderColor=None,
                                         borderWidth=1,
                                         borderPadding=3,
                                         )

        self.frequency = ParagraphStyle('yourtitle',
                                        fontName="Helvetica-Bold",
                                        fontSize=7,
                                        leading=15,
                                        parent=styles['BodyText'],
                                        alignment=TA_LEFT,

                                        )

        self.actMeds = ParagraphStyle('yourtitle',
                                        fontName="Helvetica-Bold",
                                        fontSize=10,
                                        parent=styles['BodyText'],
                                        leftIndent=20,
                                        )

        self.actMeds1 = ParagraphStyle('yourtitle',
                                        fontName="Helvetica",
                                        fontSize=10,
                                        parent=styles['BodyText'],
                                        leftIndent=40,
                                         )

        self.assigned = ParagraphStyle('yourtitle',
                                        fontName="Helvetica",
                                        fontSize=10,
                                        parent=styles['BodyText'],
                                        leftIndent=40,
                                         )

        
        self.time_dosage = ParagraphStyle('yourtitle',
                                          fontName="Helvetica-Bold",
                                          fontSize=10,
                                          backColor=None,
                                          borderRadius=0,
                                          rightIndent=30,
                                          borderColor=None,
                                          borderWidth=1,
                                          parent=styles['BodyText'],
                                          alignment=TA_LEFT,
                                          spaceBefore=-5,
                                          spaceAfter=-5,
                                          )
        self.active_medicine = ParagraphStyle('yourtitle',
                                              fontName="Helvetica-Bold",
                                              fontSize=10,
                                              leading=15,
                                              leftIndent=-8,
                                              rightIndent=9,
                                              backColor='#32CD32',
                                              borderPadding=2,
                                              parent=styles['BodyText'],
                                              alignment=TA_LEFT,

                                              )

        self.active_medicine_name = ParagraphStyle('yourtitle',
                                                   fontName="Helvetica-Bold",
                                                   fontSize=10,
                                                   backColor='#FFFFFF',
                                                   borderRadius=0,
                                                   borderColor=None,
                                                   borderWidth=0,
                                                   borderPadding=-1,
                                                   parent=styles['BodyText'],
                                                   alignment=TA_RIGHT,
                                                   spaceBefore=-10,
                                                   spaceAfter=-5,
                                                   textcolor='#000000',
                                                   )
        self.cancelled_medicine = ParagraphStyle('yourtitle',
                                                 fontName="Helvetica-Bold",
                                                 fontSize=10,
                                                 leftIndent=-8,
                                                 rightIndent=10,
                                                 leading=15,
                                                 backColor='#FF3333',
                                                 borderRadius=0,
                                                 borderColor=None,
                                                 borderWidth=1,
                                                 borderPadding=0,
                                                 parent=styles['BodyText'],
                                                 alignment=TA_LEFT,

                                                 )

        self.cancelled_medicine_name = ParagraphStyle('yourtitle',
                                                      fontName="Helvetica-Bold",
                                                      fontSize=10,
                                                      backColor='#FFFFFF',
                                                      borderRadius=0,
                                                      borderColor=None,
                                                      borderWidth=1,
                                                      borderPadding=5,
                                                      parent=styles['BodyText'],
                                                      alignment=TA_LEFT,
                                                      )
        self.medication_change = ParagraphStyle('yourtitle',
                                                fontName="Helvetica-Bold",
                                                fontSize=10,
                                                # leftIndent=5,
                                                rightIndent=40,
                                                backColor='#8fae22',
                                                borderRadius=0,
                                                borderColor=None,
                                                borderWidth=1,
                                                borderPadding=5,
                                                parent=styles['Normal'],
                                                alignment=TA_LEFT,
                                                spaceBefore=25,
                                                spaceAfter=4,
                                                textcolor=Color(1, 1, 1, 1))

        self.message_subject = ParagraphStyle('yourtitle',
                                              fontName="Helvetica-Bold",
                                              fontSize=10,
                                              # leftIndent=5,
                                              rightIndent=40,
                                              backColor='#FFA500',
                                              borderRadius=0,
                                              borderColor=None,
                                              borderWidth=1,
                                              borderPadding=5,
                                              parent=styles['Normal'],
                                              alignment=TA_LEFT,
                                              spaceBefore=25,
                                              spaceAfter=10,
                                              )

        self.message_body_to = ParagraphStyle('yourtitle',
                                              fontName='Arabic-normal', fontSize=12, leading=12. * 1.2,

                                              leftIndent=50,
                                              # rightIndent = 5,
                                              backColor="#ccFFcc",
                                              borderColor=None,
                                              borderRadius=2,
                                              borderWidth=0.5,
                                              borderPadding=5,
                                              parent=styles['Normal'],
                                              alignment=TA_RIGHT,
                                              spaceAfter=5,
                                              )

        self.message_body_from = ParagraphStyle('yourtitle',
                                                fontName='Arabic-normal', fontSize=12, leading=12. * 1.2,
                                                # fontName="Helvetica",
                                                # fontSize=10,
                                                leftIndent=5,
                                                rightIndent=50,
                                                backColor=colors.lavender,
                                                borderColor=None,
                                                borderRadius=15,
                                                borderWidth=0.5,
                                                borderPadding=5,
                                                parent=styles['Normal'],
                                                alignment=TA_LEFT,
                                                spaceAfter=5,
                                                )

        self.message_to = ParagraphStyle('yourtitle',
                                         fontName="Helvetica",
                                         fontSize=9,
                                         leftIndent=45,
                                         rightIndent=5,
                                         backColor=None,
                                         borderColor=None,
                                         borderWidth=0,
                                         borderPadding=1,
                                         parent=styles['Italic'],
                                         alignment=TA_RIGHT,
                                         spaceAfter=5,
                                         )

        self.message_from = ParagraphStyle('yourtitle',
                                           fontName="Helvetica",
                                           fontSize=9,
                                           leftIndent=5,
                                           rightIndent=45,
                                           backColor=None,
                                           borderColor=None,
                                           borderWidth=0,
                                           borderPadding=1,
                                           parent=styles['Italic'],
                                           alignment=TA_LEFT,
                                           spaceBefore=10,
                                           spaceAfter=5,
                                           )
