import json
import os
from .styles.message_styles import ProgressReportStyles
from reportlab.lib.styles import getSampleStyleSheet
from datetime import datetime
from svglib.svglib import svg2rlg
from reportlab.platypus import Image, Paragraph, Table, FrameBreak
from reportlab.lib import colors
from chi.reports.lib.core.chi_image import SVGtoImage


# import arabic_reshaper
# from bidi.algorithm import get_display
# ==========================publishing=====================

def patientHeader(canvas, doc):
    pData = doc.data["patient_info"]
    # print(json.dumps(pData, indent=4))

    if canvas.getPageNumber() == 1:
        titlePageAnnual(canvas, doc)

    paint = doc.paint
    rightEdge = doc.pagesize[0] - doc.rightMargin
    topEdge = doc.pagesize[1] - doc.topMargin
    leftEdge = doc.leftMargin
    paint.setFillColor("#555555")

    # paint.drawBoundary()
    paint.setAlign("LEFT")
    fontSize = 50
    paint.setAnchor(leftEdge, topEdge - fontSize)
    paint.setFontSize(fontSize)

    im = os.path.join(os.getenv('BASE_PATH'), 'lib/images/logo1x.png')
    paint.paintImage(im)

    paint.setAnchor(leftEdge + paint.fontSize * 1.2, topEdge)
    paint.setFontSize(18)
    paint.setFontName("Helvetica-Bold")
    # paint.paintText(["PATIENT", "PROGRESS", "REPORT"])

    paint.setAlign("RIGHT")
    paint.setAnchor(rightEdge, topEdge)
    paint.setFontSize(18)

    pData = doc.bpb
    full_name = pData["full_name"]
    if full_name != "":
        hospital_no = pData["hospital_no"]
        gender = pData["gender"]
        age = pData["age"]
        paint.paintText([full_name, gender + " | " + age, "Hospital : " + hospital_no])

    paint.setFontName("Helvetica")

    paint.setAlign("CENTER")
    paint.setAnchor(doc.pagesize[0] / 2, topEdge)
    paint.setFontSize(10)
    tm = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    paint.paintText(["Report Generated on", tm])

    paint.curY = topEdge - 65
    paint.separator()

    # Footer

    paint.setAlign("RIGHT")
    paint.setAnchor(rightEdge, doc.bottomMargin + 15)
    paint.paintText(str(canvas.getPageNumber()))

    paint.setAlign("LEFT")
    paint.setAnchor(leftEdge, doc.bottomMargin + 15)
    footnote = f"Added by Admin CHI on {datetime.date(datetime.now())}"
    paint.paintText(footnote)

    paint.curY = doc.bottomMargin + 20
    paint.separator()
    pw = doc.pagesize[0]
    ph = doc.pagesize[1]
    sc = .8
    y = ph / 2 - doc.width * sc / 2  # - doc.bottomMargin


def titlePageAnnual(canvas, doc):
    paint = doc.paint
    rightEdge = doc.pagesize[0] - doc.rightMargin
    topEdge = doc.pagesize[1] - doc.topMargin
    leftEdge = doc.leftMargin

    pw = doc.pagesize[0]
    ph = doc.pagesize[1]
    sc = .8
    y = ph / 2 - doc.width * sc / 2  # - doc.bottomMargin

    im = os.path.join(os.getenv('BASE_PATH'), 'lib/patient_progress_report/images/steth.jpg')

    paint.paintImageRect(im, 0, y, doc.pagesize[0], doc.width * sc)
    paint.drawRect(leftEdge, topEdge, doc.width, doc.height, line_width=5)
    paint.drawRect(pw / 3 + 20, ph, pw / 2 + 20, ph, color="#ffd000", transparent=0.8, line_width=0)
    paint.drawRect(pw / 3 + 40, ph - 285, 10, 300, color="black", line_width=1)

    paint.setAlign("LEFT")
    paint.setAnchor(pw / 2.5 + 20, ph - 350)
    paint.setFontSize(40)

    paint.setFontName("Helvetica-Bold")
    paint.paintText(["PATIENT", "PROGRESS"])
    paint.setFontSize(60)
    paint.setFillColor("white")
    paint.paintText("REPORT")

    paint.setFontSize(20)
    paint.paintText([""])
    paint.setFillColor("black")
    paint.setFontName("Helvetica")
    fullName = doc.data["patient_info"]["full_name"]

    tm = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    paint.paintText(["Report Generated on", tm])
    canvas.showPage()
