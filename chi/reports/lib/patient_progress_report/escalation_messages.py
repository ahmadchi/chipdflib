import json
import os
from datetime import datetime

from reportlab.lib import colors
from reportlab.platypus import Paragraph, Table
from reportlab.platypus.flowables import ParagraphAndImage, ListFlowable, ListItem, Spacer

from chi.reports.lib.core.chi_image import SVGtoImage


def add_escalations_messages(report, patientData, styles):
    messages = patientData['messages']
    if len(messages) > 0:
        path = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
        image = SVGtoImage(path, 25, 25, 'fit')
        esc_msg_image = ParagraphAndImage(Paragraph("Escalations / Medication Changes", styles.activeMedsH1), image, side='left')
        report.story.append(esc_msg_image)        
        medication_change = "<font color=#808000>" + "  Medication Change" + "</font>"
        escalations = "<font color=#FF8C00>" + "  Escalations" + "</font>"
        medicine_change = ListFlowable(
            [
                ListItem(
                    Paragraph(medication_change, styles.listitem_medications),
                    leftIndent=20, value='circle',
                    bulletColor="#808000",
                    bulletFontSize=8
                )
            ],
            bulletType='bullet',
            start='circle',
            leftIndent=10
        )
        escalation = ListFlowable(
            [
                ListItem(
                    Paragraph(escalations, styles.listitem_escalations),
                    leftIndent=-10, value='circle',
                    bulletColor="#FF8C00",
                    bulletFontSize=8
                ),
            ],
            bulletType='bullet',
            start='circle',
            leftIndent=10
        )
        data = [

            [ medicine_change, escalation],

        ]
        t = Table(data, colWidths=[ 135, 135])
        t.hAlign = "LEFT"
        t.setStyle(
            [
                # ('GRID', (0, 0), (-1, -1), 1, colors.black)

            ]
        )
        report.story.append(t)
        labels(report, styles)
        summary(report, patientData, styles)
        report.breakPage()
    else:
        return


def labels1(report, styles):
    space = Spacer(0, 10)
    assigned = "<font  color=white backColor=#32CD32>" \
               + "<i>" \
               + "<font color=#32CD32>" + "" + "</font>" + "Assigned" + \
               "</i>" + \
               "<font color=#32CD32>" + "" + "</font>" \
               + "</font>"

    unassigned = "<font color=white backColor=#8B0000>" \
                 + "<i>" \
                 + "<font color=#8B0000>" + "" + "</font>" + "Unassigned" + \
                 "</i>" + \
                 "<font color=#8B0000>" + "" + "</font>" \
                 + "</font>"
    updated = "<font color=white backColor=#A9A9A9>" \
              + "<i>" \
              + "<font color=#A9A9A9>" + "" + "</font>" + "Updated" + \
              "</i>" + \
              "<font color=#A9A9A9>" + "" + "</font>" \
              + "</font>"
    completed = "<font color=white backColor=#8A2BE2>" \
                + "<i>" \
                + "<font color=#8A2BE2>" + "" + "</font>" + "Completed" + \
                "</i>" + \
                "<font color=#8A2BE2>" + "" + "</font>" \
                + "</font>"

    paragraph_ = Paragraph("<font  color=white>" + "" + "</font>" +
                           assigned +
                           "<font  color=white>" + "" + "</font>" +
                           unassigned + "<font  color=white>" + "" + "</font>" +
                           updated + "<font  color=white>" + "" + "</font>" +
                           completed + "<font  color=white>" + "" + "</font>",
                           styles.simple_label)

    # paragraph_.vAlign = "BOTTOM"
    # paragraph_.hAlign = "LEFT"
    # report.story.append(space)
    report.story.append(paragraph_)
    # report.story.append(space)

def makeLabel(text, color):
    assigned = "<font  color="+color+">" + text + "</font>"
    return assigned

def labels(report, styles):
    space = Spacer(0, 10)
    assignedP = makeLabel("Assigned", "#FFFFFF") 
    assignedP = Paragraph(assignedP, styles.active_medicine_label)

    unassignedP = makeLabel("Unassigned", "#FFFFFF") 
    unassignedP = Paragraph(unassignedP, styles.cancelled_medicine_label)

    completedP = makeLabel("Deleted", "#FFFFFF") 
    completedP = Paragraph(completedP, styles.completed_medicine_label)

    updatedP = makeLabel("Updated", "#FFFFFF") 
    updatedP = Paragraph(updatedP, styles.update_medicine_label)

    table_data = [
        [assignedP, unassignedP, completedP, updatedP],

    ]
    colWidths = [70,70,70,70]

    t = Table(table_data, colWidths=colWidths)

    t.setStyle(
        [
            # ('GRID', (0, 0), (-1, -1), 1, colors.black)
        ]
    )
    t.hAlign = "LEFT"
    report.story.append(t)



def medication_change(report, medicines, styles):
    medications = []
    for meds in medicines:
        if meds["type"] == "update":
            med_strength = "<font fontSize=8>" + meds["strength"] + "</font>"
            med_name = "<font color=white>" + "<b>" + meds["medicine_name"] + " " + med_strength + "</b>" + "</font>"
            sch = "<i>" + "<font color=white>" + "(Schedule changed)" + "</font>" + "</i>"
            medication_names = Paragraph(med_name + " " + sch, styles.sch_change)
            medications.append(medication_names)
        elif meds["type"] == "add":
            med_strength = "<font fontSize=8>" + meds["strength"] + "</font>"
            med_name = "<font color=white>" + meds["medicine_name"] + " " + med_strength + "</font>"
            medication_names = Paragraph(med_name, styles.active_medicine)
            medications.append(medication_names)
        else:
            med_strength = "<font fontSize=8>" + meds["strength"] + "</font>"
            med_name = "<font color=white>" + "<strike>" + meds["medicine_name"] + " " + med_strength + "</strike>" + "</font>"
            medication_names = Paragraph(med_name, styles.cancelled_medicine)
            medications.append(medication_names)
    return medications


def summary(report, data, styles):
    count = 0
    for datewise_messages in data['messages']:
        
            
        if datewise_messages['subjects']:
            count += 1
            subjects(report, datewise_messages, count, styles)
        
        if datewise_messages['medicines']:
            count += 1
            # print("med found")
            # exit()
            medicnez(report, datewise_messages, count, styles)
        

def medicnez(report, datewise_messages, count, styles):
    med_change = Paragraph("<font fontSize = 8 >" + "<b>" + "Med. Change :" + "</b>" + "</font>",
                           styles.subjects_under_graph)
    date1 = str(datetime.fromtimestamp(datewise_messages['date']))
    date_ = datetime.strptime(date1, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
    date = "<font color=gray>" + str(date_) + "</font>"

    subject_change_date = Paragraph(date, styles.subjects_under_graph)
    medicine_change = medication_change(report, datewise_messages['medicines'], styles)
    bullet_number = Paragraph("<font color=white>" + str(count) + "</font>", styles.med_change_bullet)
    table_data = [
        [bullet_number, med_change, medicine_change, subject_change_date],

    ]
    colWidths = [25, 70, 135, 120]

    t = Table(table_data, colWidths=colWidths)

    t.setStyle(
        [
            # ('GRID', (0, 0), (-1, -1), 1, colors.black)

        ]
    )
    t.hAlign = "LEFT"
    report.story.append(t)


def subjects(report, datewise_messages, count, styles):
    space = Spacer(0, 1)
    bullet_number = Paragraph("<font color=white>" + str(count) + "</font>", styles.med_bullet_subject)
    date1 = str(datetime.fromtimestamp(datewise_messages['date']))
    #date_ = datetime.strptime(date1, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %I:%M:%S %p")
    date_ = datetime.strptime(date1, "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d")
    
    date = "<font color=gray>" + str(date_) + "</font>"
    subject_date = Paragraph(date, styles.subjects_under_graph)

    for subjects in datewise_messages['subjects']:
        sbj_str = "<font fontSize = 8 >" + "<b>" + "Subject :" + "</b>" + "</font>"
        s = subjects['title'].split(" ")
        separator = " "
        ss = separator.join(s[:-3])
        subject = Paragraph(sbj_str + str(ss), styles.subjectsss)

        table_data = [
            [bullet_number, subject, subject_date],
        ]
        colWidths = [25, 195, 120]
        ts = Table(table_data, colWidths=colWidths)
        ts.setStyle(
            [
                # ('GRID', (0, 0), (-1, -1), 1, colors.black)

            ]
        )
        ts.hAlign = "LEFT"
        report.story.append(ts)
