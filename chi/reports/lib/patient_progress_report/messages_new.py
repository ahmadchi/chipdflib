import json
from datetime import datetime

import arabic_reshaper

from reportlab.lib import colors
from reportlab.platypus import Paragraph, Table, Spacer
from bidi.algorithm import get_display

from ..core.chi_utils import CHIUtils

c = CHIUtils()


def appendTable(report, table_data, show_boundry=None):
    t = Table(table_data, colWidths=[110, 60, 70])
    t.hAlign = 'LEFT'
    if show_boundry is True:
        t.setStyle([('GRID', (0, 0), (-1, -1), 1, colors.black),
                    ])
    report.story.append(t)


def process_messages(report, messages, styles):
    title = "<font color = white >" + messages["title"] + "</font>"
    report.appendPara(title, styles.message_subject, keepWithNext=True)

    for message in messages['messages']:
        time = c.time_from_timestamp(message['time'])
        # What's this for here?
        if message['title'] == "Medication Change":
            continue

        time_1 = "<font color=gray>" + str(time) + "</font>"
        patient_fullname = "<font fontSize=10>" + message["full_name"] + "</font>"

        if message["full_name"] == "Command  Center":
            continue

        if message["group_type"] == "Patient":
            report.appendPara(patient_fullname + " " + time_1, styles.message_to, keepWithNext=True)
            arabic_text = arabic_reshaper.reshape(message["message"])
            msggg = get_display(arabic_text)
            report.appendPara(msggg, styles.message_body_to, keepWithNext=False)
        else:
            report.appendPara(patient_fullname + " (" + message["group_type"] + ")" + " " + time_1, styles.message_from,
                              keepWithNext=True)
            arabic_text = arabic_reshaper.reshape(message["message"])
            msggg = get_display(arabic_text)
            report.appendPara(msggg, styles.message_body_from, keepWithNext=False)


def preprocessing_dosage_time(report, medicines, styles):
    frequency = Paragraph(medicines['current']['frequency'], styles.frequency)
    table_data = [[frequency]]

    for times in medicines['current']['days']:
        if "day" in times:
            day = times['day']
            for times_ in times['times']:
                grey_time_font = "<font color=white>" + str(c.time_from_timestamp(times_['time'])) + "</font>"
                dosage_time = Paragraph(grey_time_font, styles.time__)
                dosage = Paragraph(str(times_['dosage']) + "-" + day, styles.dosage__)
                table_data[0].append([[dosage, dosage_time]])
        else:
            for times_ in times['times']:
                grey_time_font = "<font color=white>" + str(c.time_from_timestamp(times_['time'])) + "</font>"
                dosage_time = Paragraph(grey_time_font, styles.time__)
                dosage = Paragraph(str(times_['dosage']), styles.dosage__)
                table_data[0].append([[dosage, dosage_time]])

    t = Table(table_data, colWidths=[30])
    t.hAlign = 'LEFT'
    t.setStyle(
        [('GRID', (0, 0), (-1, -1), 1, colors.gray),
         ]
    )
    return t


def previous_dosage_time(report, medicines, styles):
    frequency = Paragraph(medicines['current']['frequency'], styles.frequency)
    table_data = [[frequency]]
    if "prev" in medicines:
        for prev_times in medicines['prev']['days']:
            if "day" in prev_times:
                for times__ in prev_times['times']:
                    grey_time_font = "<font color=white>" + "<strike> " + str(
                        c.time_from_timestamp(times__['time'])) + "</strike>" + "</font> "
                    dosage_time = Paragraph(grey_time_font, styles.time__)
                    dosage = Paragraph("<strike> " + str(times__['dosage']) + "-" + prev_times['day'] + "</strike>",
                                       styles.dosage__)
                    table_data[0].append([dosage, dosage_time])
            else:
                for times__ in prev_times['times']:
                    grey_time_font = "<font color=white>" + "<strike> " + str(
                        c.time_from_timestamp(times__['time'])) + "</strike>" + "</font> "
                    dosage_time = Paragraph(grey_time_font, styles.time__)
                    dosage = Paragraph("<strike> " + str(times__['dosage']) + "</strike>",
                                       styles.dosage__)
                    table_data[0].append([dosage, dosage_time])
    else:
        return

    t = Table(table_data, colWidths=[30])
    t.hAlign = 'CENTER'
    t.setStyle(
        [('GRID', (0, 0), (-1, -1), 1, colors.gray),
         ]
    )
    return t


def process_medication_change(report, data, styles):
    count = 0
    for medicines in data:
        count += 1

        _type = medicines['type']
        table_data = []

        if _type == "update":
            med_name = "<font color=white>" + medicines["medicine_name"] + " " + medicines["strength"] + "</font>"
            medication_names = Paragraph(med_name, styles.update_medicine)
            table_data.append([medication_names])
        elif _type == "add":
            med_name = "<font color=white>" + medicines["medicine_name"] + " " + medicines["strength"] + "</font>"
            medication_names = Paragraph(med_name, styles.active_medicine)
            table_data.append([medication_names])
        else:
            med_name = "<font color=white>" + "<strike>" + medicines["medicine_name"] + " " + medicines[
                "strength"] + "</strike>" + "</font>"
            medication_names = Paragraph(med_name, styles.cancelled_medicine)
            table_data.append([medication_names])

        table_data = [
            [
                [medication_names],

            ],
        ]
        t1 = Table(table_data, colWidths=[90, 45])
        # t1.setStyle([('GRID', (0, 0), (-1, -1), 1, colors.black),
        #  ])
        t1.hAlign = 'LEFT'

        t2 = preprocessing_dosage_time(report, medicines, styles)
        t3 = previous_dosage_time(report, medicines, styles)

        data = [
            [t1],
            [t2],
            [t3]

        ]
        t = Table(data, [119, 55, 60])
        # t = Table(data, [119, 50])
        t.hAlign = "LEFT"
        # t.setStyle(
        #     [
        #     ('GRID', (0, 0), (-1, -1), 1, colors.green),
        #      ]
        # )
        report.story.append(t)


def add_messages(report, data, styles):
    messages = data['messages']
    if messages is not None:
        assert isinstance(messages, list), "messages should be type of Array"
    else:
        raise TypeError("Invalid data type")

    for date_wise_messages in messages:
        date_ = c.date_from_timestamp(date_wise_messages['date'])
        no_of_dashes = 30
        for messagess in date_wise_messages['subjects']:
            report.appendPara("-" * no_of_dashes + str(date_) + "-" * no_of_dashes, styles.message_date,
                              keepWithNext=True)
            process_messages(report, messagess, styles)

        medicines = date_wise_messages['medicines']
        if not medicines:
            continue
        else:
            report.appendPara("-" * no_of_dashes + str(date_) + "-" * no_of_dashes, styles.message_date,
                              keepWithNext=True)
            medication_change_title = "<font color = white >" + "Medication Change" + "</font>"
            report.appendPara(medication_change_title, styles.medication_change, keepWithNext=True)
            process_medication_change(report, medicines, styles)
