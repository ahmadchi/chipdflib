from reportlab.platypus import Frame, PageTemplate,FrameBreak
from chi.reports.lib.core.chi_frame import CHIFrame
from chi.reports.lib.core.chi_report_template import CHIReportTemplate
from chi.reports.lib.patient_progress_report.add_active_medications import addActiveMedications

from .escalation_messages import add_escalations_messages
from .messages_new import add_messages
from .styles.message_styles import ProgressReportStyles
from .progress_report import patientHeader
from .graphs import addGraphs
from .right_panel import addRightPanel

from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from .add_patient_info import addPatientInfo
from chi.reports.lib.core.chi_before_page_break import CHIBeforePageBreak
from ..core.chi_utils import CHIUtils

def addPageTemplates(doc):
    sb = 0
    footerHeight = 20 + 10

    doc.headerHeight = 65

    leftMargin = doc.leftMargin
    bottomMargin = doc.bottomMargin + 20 + 15
    width2 = doc.width / 2
    width3 = doc.width / 2
    frameHeight = doc.height - doc.headerHeight - footerHeight - 10
    incWidth = 50

    frame1 = Frame(leftMargin + 0 * width2, bottomMargin, width2 + incWidth, frameHeight, id='col1', showBoundary=sb)
    frame2 = CHIFrame(leftMargin + 1 * width2 + incWidth, bottomMargin - 10, width2 - incWidth, frameHeight + 10,
                      id='col2', showBoundary=sb, backgroundColor="#EEEEEE")

    frame3 = Frame(leftMargin + 0 * width3, bottomMargin, width3, frameHeight, id='col3', showBoundary=sb)
    frame4 = Frame(leftMargin + 1 * width3, bottomMargin, width3, frameHeight, id='col4', showBoundary=sb)

    frame01 = Frame(leftMargin + 0 * width3, bottomMargin, width3 - 5, frameHeight, id='col3', showBoundary=sb)
    frame02 = Frame(leftMargin + 1 * width3 + 5, bottomMargin, width3 - 5, frameHeight, id='col4', showBoundary=sb)

    twoColumnPage = PageTemplate(id='two_col', frames=[frame2, frame1])
    threeColumnPage = PageTemplate(id='messages_two_col', frames=[frame3, frame4])
    tocPage = PageTemplate(id='tocPage', frames=[frame01, frame02])

    doc.addPageTemplates([tocPage, twoColumnPage, threeColumnPage])


def PatientProgressReport(dataArr):
    headerHeight = 65
    report = CHIReportTemplate(headerHeight, tableOfContents=True, titlePage=False)
    report.chiUtils = CHIUtils()
    report.pageCallback = patientHeader
    addPageTemplates(report)
    styles = ProgressReportStyles()
    pdfmetrics.registerFont(TTFont('Arabic-normal', 'fonts/trado/trado.ttf'))

    for data in dataArr:
        report.data = data
        pData = data["patient_info"]
        full_name = pData["full_name"]
        hospital_no = pData["hospital_no"]
        gender = pData["gender"]
        birth_date = pData["birth_date"]
        age = pData["age"]
        chiBPB = CHIBeforePageBreak(
            {
                "full_name": full_name,
                "hospital_no": hospital_no,
                "gender": gender,
                "age": age,
            }, report)
        report.story.append(chiBPB)
        report.breakPage()
        addRightPanel(report, data, styles)
        report.changePageStyle("messages_two_col")
        report.story.append(FrameBreak())
        addPatientInfo(report, data, styles)
        addActiveMedications(report, data, styles)
        addGraphs(report, data, styles)
        # report.breakPage()
        add_escalations_messages(report, data, styles)
        # report.breakPage()
        add_messages(report, data, styles)
        report.changePageStyle("two_col")

    chiBPB = CHIBeforePageBreak({"full_name": ""}, report)
    report.story.append(chiBPB)
    print("==========================publishing====2=================")
    report.publish()

    return report
