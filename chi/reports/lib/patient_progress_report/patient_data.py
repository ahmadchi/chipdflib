from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Image, Paragraph
from reportlab.platypus.flowables import ParagraphAndImage
from chi.reports.lib.core.chi_report_template import CHIReportTemplate


def process_diagnoses(data):
    report = CHIReportTemplate(headerHeight)
    report.data = data
    report.pageCallback = patientHeader
    if len(data['diagnoses']) > 0:
        path = report.dir_path + '../patient_progress_report/images/progress-report/search.svg'
        image = SVGtoImage(path, 20, 20, 'fit')
        report.story.append(ParagraphAndImage(Paragraph("Diagnosis", styles.activeMedsH1), image, side='left'))

    count = 1
    if data["diagnoses"] is not None:
        assert isinstance(data["diagnoses"], list), "diagnoses should be type of Array"
    for diagnosis in data["diagnoses"]:
        report.appendPara(str(count) + ". " + diagnosis, styles.activeDiagnosis)
        count += 1

    if len(data['diagnoses']) > 0:
        report.appendPara("__________________________________________", styles.activeDiagnosis)
