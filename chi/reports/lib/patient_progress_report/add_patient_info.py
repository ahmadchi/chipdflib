from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph
from reportlab.platypus.flowables import ParagraphAndImage




def addPatientInfo(report, data, styles):


        path = report.dir_path + '../patient_progress_report/images/progress-report/pill.svg'
        image = SVGtoImage(path, 10, 10, 'fit')
        report.story.append(ParagraphAndImage(Paragraph("Patient Information", styles.activeMedsH1), image, side='left'))

        pData = report.data["patient_info"]
        full_name = pData["full_name"]  
    
        # full_name = pData["full_name"]
        hospital_no = pData["hospital_no"]
        gender = pData["gender"]
        birth_date = pData["birth_date"]
        age = pData["age"]
        

        # paint.paintText([full_name, gender + " | " + age,  "Hospital : " + hospital_no])
        #print(styles)
        # exit()
        doctorName = ""
        if "physician.full_name" in data["doctor_info"][0]:
            doctorName = data["doctor_info"][0]["physician.full_name"]
        else:
            doctorName = data["doctor_info"][0]["name"]
        pInfo = full_name + "\n(" + doctorName +")"
        report.appendPara(pInfo, styles.level1)
        report.appendPara(hospital_no, styles.items)
        report.appendPara(gender, styles.items)
        # report.appendPara(birth_date, styles.activeMeds)
        report.appendPara(age, styles.items)

        report.appendPara(" ", styles.itemsSeparator)


        report.story.append(ParagraphAndImage(Paragraph("PHYSICIAN INFORMATION", styles.activeMedsH1), image, side='left'))
        count = 1
        for doctor in data["doctor_info"]:  
            report.appendPara(str(count) + ". "+doctorName, styles.items)
            count += 1
        report.appendPara(" ", styles.itemsSeparator)
        
        # report.appendPara(age, styles.activeMeds)
        # print(age)
        # exit()
