import json
import os

from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch

def addTotalTable(report,styles):
    billingData = report.data
    patientInfo = billingData["patient_info"]

    
    fullName = patientInfo["full_name"]
    details =  patientInfo["details"] 

    detailArr = []
    for detail in details:
        a = Paragraph("<i>" + detail + "</i>",styles.infoTable)
        detailArr.append(a) 

    #if "patient_info" in billingData:
    #p = Spacer(0, 0.1*inch)
    
    a1 = Paragraph(fullName,styles.activeMedsH1)

    a3 = Paragraph("TOTAL PAID:",styles.totalTable)
    b2 = Paragraph("PKR. 71,500.0",styles.totalTable)

    data = []
    data=[
        [[a1,detailArr], [a3,b2]]
        ]

    t = Table(data,style = [
        #'GRID',(0,0),(4,1),1,'#000000'),
        #('HALIGN',(1,0),(1,0),'RIGHT') ,
        ('VALIGN',(0,0),(0,0),'TOP')

        ],colWidths=(265,265))
    report.story.append(t)
   
    p = Spacer(0, 0.5*inch)
    report.story.append(p)
    #report.appendPara("__________________________________________________________________________________________________________________________",styles.separator)
