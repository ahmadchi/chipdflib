import os
from chi.reports.lib.core.chi_circle import CHICircle
from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch

def addLabTestTable(report,styles):
        path1 = report.dir_path + '../patient_progress_report/images/progress-report/calendar.svg'
        image1 = SVGtoImage(path1, 10, 10, 'fit')
        currMedTitle = ParagraphAndImage(Paragraph("Escalation/Messages", styles.activeMedsH1), image1, side='left')

        a2 = Paragraph("<font color='#9A7D0A'>"+"Medication Change "+"</font>" +"<font color='#F39C12'>"+"Escalations"+"</font>",styles.common)
        
        green = ["<font backColor='#229954  ' color=white>","</font>"]
        red = ["<font backColor=red  color=white>","</font>"]
        gray = ["<font backColor=gray color=white>","</font>"]
        blue = ["<font backColor=blue color=white>","</font>"]
        
        z1 = green[0] + "Assigned" + green[1]
        z2 = red[0] + "Unassigned" + red[1] 
        z3 = gray[0] + "Updated" + gray[1]
        z4 = blue[0] + "Completed" + blue[1]

        a4 = Paragraph(z1+" "+z2+" "+z3+" "+z4,styles.change)

        circle = CHICircle(radius=10,text="",color = '#0000FF')
        data=[
            [currMedTitle,[circle, a2]],
            ["", a4]
        ]
        t = Table(data,style = [('GRID',(0,0),(10,10),1,'#000000')],colWidths=[265,265])
        report.story.append(t)

        b1 = Paragraph("Med. Change:"+green[0]+"Glucophage XR"+green[1],styles.common)
        b2 = Paragraph("Med. Change:"+gray[0]+"Glucophage XR"+gray[1],styles.common)
        b3 = Paragraph("Med. Change:"+green[0]+"Glucophage XR"+green[1],styles.common)
        b4 = Paragraph("Med. Change:"+red[0]+"Glucophage XR"+red[1],styles.common)
        b5 = Paragraph("Med. Change:"+green[0]+"Glucophage XR"+green[1],styles.common)
        b6 = Paragraph("Med. Change:"+gray[0]+"Glucophage XR"+gray[1],styles.common)

        b7 = Paragraph("3/09/20 - 03:15 PM",styles.common)

        data1 = [
            [b1, b7],
            [b2, b7],
            [b3, b7],
            [b4, b7],
            [b5, b7],
            [b6, b7]
        ]

        t = Table(data1,
        style = [('GRID',(0,0),(10,10),1,'#000000'),
        ('VALIGN',(0,0),(5,1),'MIDDLE')],
            colWidths=[400,130])
        report.story.append(t)


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    # prescData = report.data
    # # prescData = json.load(jsonFile)
    # data = []
    # testArr = []
    # # return
    # if "lab_tests" in prescData:
    #     for test in prescData["lab_tests"]:
    #         if test["checked"]:
    #             a = Paragraph("-"+test["name"]+"<font size=6>"+"  complete as directed"+"</font>",styles.labTestTable)
    #             testArr.append(a)
    #             b = Paragraph("_________________________________________________________________________________________________________________",styles.separatorTable)
    #             testArr.append(b)   
    #     path1 = report.dir_path + '../patient_progress_report/images/progress-report/pill.svg'
    #     image1 = SVGtoImage(path1, 15, 15, 'fit')
    #     currMedTitle = ParagraphAndImage(Paragraph(" Lab Tests", styles.activeMedsH1), image1, side='left')
    #     p = Spacer(0, 0.1*inch)

    #     data=[[[currMedTitle,p, testArr]]]
    #     t = Table(data)
    #     report.story.append(t)