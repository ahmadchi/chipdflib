import json
import os

from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch

def addDescriptionTable(report,styles):
    billingData = report.data
    if "services" in billingData:
        title = ["ID", "SERVICES", "Qty.", "Tax (%)", "Discount (PKR)", "Unit Price (PKR)", "Sub-Total (PKR)"]
        data0 = [title]
        t = Table(data0,style = [
            #('GRID',(0,0),(6,3),1,'#000000'),
            ('ALIGN',(2,0),(6,0),'CENTER'),
            ('ALIGN',(0,0),(0,0),'CENTER'),
            ('BACKGROUND',(0,0),(6,0),'#DDDDDD')],
            colWidths=[20,170,40,50,80,80,80])
        report.story.append(t)
        services = billingData["services"]
        id = []
        serv = []
        qty = []
        tax = []
        disc = []
        uPrice = []
        subTotal = []

        for service in services:
            id.append(service["id"])
            serv.append(service["service"])
            qty.append(service["quantity"])
            tax.append(service["tax"])
            disc.append(service["discount"])
            uPrice.append(service["unit_price"])
            subTotal.append(service["sub_total"])
        
        data1 = []
        for i in range(len(id)):
            data1 = [
                [id[i], serv[i], str(qty[i]), str(tax[i]), 
                str(disc[i]), str(uPrice[i]), str(subTotal[i])]
            ]
            t = Table(data1, style = [
                ('GRID',(0,0),(6,3),0.8,'#111111'),
                ('ALIGN',(4,0),(6,0),'RIGHT'),
                ('ALIGN',(1,0),(1,0),'LEFT'),
                ('ALIGN',(2,0),(3,0),'CENTER')
                ],
                colWidths=[20,170,40,50,80,80,80])
            report.story.append(t)
    



        # t = Table(data0, style = [
        #     ('GRID',(0,0),(6,3),1,'#000000'),
        #     ('BACKGROUND',(0,0),(6,0),'#DDDDDD')
        #     ],colWidths=[40,150,40,40,80,80,100])
        # report.story.append(t)
    
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
    # a1 = Paragraph("Mr.Sarfaraz Arshad",styles.activeMedsH1)
    # a2 = Paragraph("+923450513438<br/>MR No: 123456",styles.infoTable)

    # a3 = Paragraph("TOTAL PAID:",styles.totalTable)
    # b2 = Paragraph("PKR. 71,500.0",styles.totalTable)

    # data = []
    # data=[
    #     [[a1,a2], [a3,b2]]
    #     #["", b2]
    #     ]

    # t = Table(data,style = [
    #     #'GRID',(0,0),(4,1),1,'#000000'),
    #     #('HALIGN',(1,0),(1,0),'RIGHT') ,
    #     ('VALIGN',(0,0),(0,0),'TOP')

    #     ],colWidths=(265,265))
    # report.story.append(t)
    #report.appendPara("__________________________________________________________________________________________________________________________",styles.separator)
