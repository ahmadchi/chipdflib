import os
from datetime import datetime

from chi.reports.lib.core.chi_image import SVGtoImage

def letterHead(canvas, doc):
    # pData = doc.data["patient_info"]
    # doctData = doc.data["doctor_info"]
    tData = doc.letterHeadTemplate

    if doc.titlePage:
        if canvas.getPageNumber() == 1:
            titlePageAnnual(canvas, doc)

    paint = doc.paint

    for block in tData["blocks"]:
      
        if block["type"] == "TEXT":
            paint.setAlign(block["align"])
            paint.setAnchor(block["anchorX"], block["anchorY"])
            paint.setFontSize(block["fontSize"])
            paint.setFontName(block["fontName"])
            paint.paintText(block["text"])

        elif block["type"] == "IMAGE":
            if block["file_type"] == "png":
                im = os.path.join(os.getenv('BASE_PATH'), f"lib/images/{block['filename']}")
                paint.paintImageRect(im, block["x"], block["y"], block["w"], block["h"])

            if block["file_type"] == "svg":
                grey = os.path.join(os.getenv('BASE_PATH'), f"lib/images/" + block["filename"])
                paint.paintSVG(grey, block["x"], block["y"], block['w'], block['h'])

        elif block["type"] == 'LOGO_TEXT':
            paint.setAlign(block["align"])
            paint.setAnchor(block["anchorX"], block["anchorY"])
            paint.setFontSize(block["fontSize"])
            paint.setFontName(block["fontName"])
            paint.paintText(block["text"])

        # elif block['type'] == 'SEPARATOR':
        #     add_separator(paint, block['anchorY'])
        #     bottom_left_separator(canvas)
        #     vertical_separator(canvas, )
        else:
            print("Type not supported yet", block["type"])

    return


def add_separator(paint, y):
    paint.custom_separator(y)


def bottom_left_separator(canvas):
    canvas.line(122, 110, 580, 110)

def vertical_separator(canvas):
    x = 360
    canvas.line(x, 60, x, 92)


# def titlePageAnnual(canvas, doc):
#     paint = doc.paint
#     rightEdge = doc.pagesize[0] - doc.rightMargin
#     topEdge = doc.pagesize[1] - doc.topMargin
#     leftEdge = doc.leftMargin

#     pw = doc.pagesize[0]
#     ph = doc.pagesize[1]
#     sc = .8
#     y = ph / 2 - doc.width * sc / 2  #

#     im = os.path.join(os.getenv('BASE_PATH'), 'lib/patient_progress_report/images/steth.jpg')

#     paint.paintImageRect(im, 0, y, doc.pagesize[0], doc.width * sc)
#     paint.drawRect(leftEdge, topEdge, doc.width, doc.height, line_width=5)
#     paint.drawRect(pw / 3 + 20, ph, pw / 2 + 20, ph, color="#ffd000", transparent=0.8, line_width=0)
#     paint.drawRect(pw / 3 + 40, ph - 285, 10, 300, color="black", line_width=1)

#     paint.setAlign("LEFT")
#     paint.setAnchor(pw / 2.5 + 20, ph - 350)
#     paint.setFontSize(40)

#     paint.setFontName("Helvetica-Bold")
#     paint.paintText(["PATIENT", "PRESCRIPTION"])
#     paint.setFontSize(60)
#     paint.setFillColor("white")
#     paint.paintText("REPORT")

#     paint.setFontSize(20)
#     paint.paintText([""])
#     paint.setFillColor("black")
#     paint.setFontName("Helvetica")
#     fullName = doc.data["patient_info"]["full_name"]

#     tm = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

#     paint.paintText([fullName, "Report Generated on", tm])
#     canvas.showPage()
