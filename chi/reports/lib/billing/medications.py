from reportlab.platypus import Table, Paragraph, ParagraphAndImage, Image
from chi.reports.lib.core.chi_image import SVGtoImage
from chi.reports.lib.prescription.detect_arabic_font import get_arabic_font
from chi.reports.lib.prescription.styles.message_styles import PrescriptionReportStyles
import json


def add_medication_list(report, style):
    styles = PrescriptionReportStyles()

    path = report.dir_path + '../patient_progress_report/images/progress-report/pill.svg'
    image = SVGtoImage(path, 15, 15, 'fit')

    report.story.append(ParagraphAndImage(Paragraph("Medication", styles.activeMedsH1), image, side='left'))

    prescData = report.data
    sections = prescData.get('sections', [])
    for section in sections:
        if len(section["medicines"]) > 0:
            report.story.append(Paragraph(section["section_name"], styles.activeMedsH1))
            count = 0

            data = []
            
            for medicine in section["medicines"]:
                count += 1
                med = medicine["medicine_name"]
                strength = "<font size=6>" + medicine["strength"] + "</font>"
                medication = Paragraph(str(count) + "- " + med + "  " + strength, styles.common)

                no_of_days = "For " + str(medicine['no_of_days']) + " days"
                noOfDays = Paragraph(no_of_days, styles.med_strength)

                dosage = json.loads(medicine["dosage"])
                morning = dosage[0]["value"]
                noon = dosage[1]["value"]
                evening = dosage[2]["value"]
                dose = morning + " + " + noon + " + " + evening
                dosage = Paragraph(str(dose), styles.common)
                doseInt = int(morning) + int(noon) + int(evening)
                if doseInt == 3:
                    a = (get_arabic_font(u"صبح ، دوپهر ، شام"))
                elif doseInt == 2:
                    a = (get_arabic_font(u"صبح ، شام"))
                elif doseInt == 1:
                    a = (get_arabic_font(u"ايك بار"))
                else:
                    a = (get_arabic_font(u"- "))

                data = [[medication, noOfDays, dosage, a]]
                t = Table(data,  # style=[('GRID', (0, 0), (4, 3), 1, '#000000')],
                        colWidths=[200, 100, 100, 130])
                report.story.append(t)
                b = Paragraph(
                    "_________________________________________________________________________________________________________________",
                    styles.separatorTable)
                report.story.append(b)
