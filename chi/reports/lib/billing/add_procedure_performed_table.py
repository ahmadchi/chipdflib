import os

from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch

def addProcPerformedTable(report,styles):
    prescData = report.data

    data = []
    procPerformedArr = [] 
    procedureArr = []   
    
    if "procedure_performed" in prescData:
        for procedure in prescData["procedure_performed"]:
            a = Paragraph(procedure, styles.labTestTable)
            procPerformedArr.append(a)
    else:
        procPerformedArr = Paragraph("Data Not Available", styles.labTestTable)

    if "diagnosis/procedures" in prescData:
        for procedure in prescData["diagnosis/procedures"]:
            a = Paragraph(procedure, styles.labTestTable)
            procedureArr.append(a)
    else:
        procedureArr = Paragraph("Data Not Available", styles.labTestTable)
    path1 = report.dir_path + '../patient_progress_report/images/progress-report/assignment.svg'
    image1 = SVGtoImage(path1, 10, 10, 'fit')
    title1 = ParagraphAndImage(Paragraph("Procedure Performed", styles.activeMedsH1), image1, side='left')
   
    path2 = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
    image2 = SVGtoImage(path2, 10, 10, 'fit')
    title2 = ParagraphAndImage(Paragraph("Diagnosis/Procedures", styles.activeMedsH1), image2, side='left')
   
   
    # p = Spacer(0, 0.1*inch)

    data = [[[title1,procPerformedArr], [title2,procedureArr]]]


    t = Table(data, colWidths=[265,265], 
    style=[
        #('GRID',(0,0),(1,0),1,'#000000'),
        ('VALIGN',(0,0),(1,0),'TOP'),
        ('ALIGN',(0,0),(1,0),'CENTER'),
        ('INNERGRID', (0, 0), (1, 0), 0.8, '#CCCCCC')


        ])
    report.story.append(t)