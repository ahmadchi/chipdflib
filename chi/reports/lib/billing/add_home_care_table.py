import json
import os

from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage


def addHomeCareTable(report, styles):
    
    prescData = report.data

    data1 = []
    data2 = []
    data3 = []

    if "homecare" in prescData:
        for items in prescData["homecare"]:
            a = Paragraph(" -  " + items["name"], styles.homeCareTable)
            data1.append(a)

            b = Paragraph(items["timings"], styles.homeCareTable)
            data2.append(b)
            
            c = Paragraph(items["days"], styles.homeCareTable)
            data3.append(c)

        path = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
        image = SVGtoImage(path, 15, 15, 'fit')
        title = ParagraphAndImage(Paragraph("Homecare", styles.activeMedsH1), image, side='left')
        
        timing = Paragraph("Timings", styles.homeCareTable)
        day = Paragraph("Days", styles.homeCareTable)
        p = Paragraph("_________________________________________________________________________________________________________________",styles.labTestTable)

        data4 = []
        data4 = [[title, timing, day]]
        t = Table(data4,colWidths=[200, 200, 155])
        report.story.append(t)

        data5 = []
        for i in range(len(data1)):
            data5 = [[data1[i], data2[i], data3[i]]]
            t = Table(data5,colWidths=[200, 200, 155])
            report.story.append(t)
            if i == (len(data1)-1):
                report.appendPara("________________________________________________________________________________________________________________________",styles.separator)
            else:
                report.appendPara("_________________________________________________________________________________________________________________",styles.separatorTable)