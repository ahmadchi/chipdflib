import json
import os

from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch

def addInfoTable(report,styles):
    billingData = report.data
    if "clinic_info" in billingData:
        info = billingData["clinic_info"]
        receipt = "<font size=10>"+"<b>" + "RECEIPT: " + "</b>"+"</font>"
        date = "<font size=10>"+"<b>" + "DATE: " + "</b>"+"</font>" + info["date"]
        invoice = "<font size=10>"+"<b>" + "INVOICE: " + "</b>"+"</font>" + info["invoice"]

        a1 = Paragraph( receipt + info["receipt"],styles.infoTable)
        a2 = Paragraph("ADDRESS",styles.activeMedsH1)
        a3 = Paragraph("PHONE",styles.activeMedsH1)
        a4 = Paragraph("EMAIL",styles.activeMedsH1)
        a5 = Paragraph("WEB",styles.activeMedsH1)

        b1 = Paragraph(date,styles.infoTable)
        b2 = Paragraph(invoice,styles.infoTable)
        
        address = Paragraph(info["address"],styles.infoTable)
        phone = Paragraph(info["phone"],styles.infoTable)
        
        emailArr = []
        for email in info["email"]:
            eml = Paragraph(email,styles.infoTable)
            emailArr.append(eml)

        webArr = []
        for web in info["web"]:
            wb = Paragraph(web,styles.infoTable)
            webArr.append(wb)

        space1 = Spacer(0, 0.02*inch)
        space2 = Spacer(0, 0.15*inch)
        
        data = []
        data=[
            [a1, a2, a3, a4, a5],
            [space1, space1, space1, space1, space1],
            [[b1, space2, b2], address, phone, emailArr, webArr]
            ]

        t = Table(data,style = [
            #('GRID',(0,0),(4,1),1,'#000000'),
            ('VALIGN',(0,2),(4,2),'TOP') ,
            ('INNERGRID', (1, 0), (4, 0), 0.8, '#CCCCCC'),
            ('INNERGRID', (1, 1), (4, 1), 0.8, '#CCCCCC'),
            ('INNERGRID', (1, 2), (4, 2), 0.8, '#CCCCCC')
            ],colWidths=(100,110,70,155,120))
        report.story.append(t)
        p3 = Spacer(0, 0.7*inch)
        report.story.append(p3)