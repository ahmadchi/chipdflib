from reportlab.platypus import KeepInFrame

from chi.reports.lib.core.chi_image import SVGtoImage


def addTopSection(report, styles, title, image, text):

    report.appendPara(title, styles.p1b)
    report.appendPara(text, styles.diagnoses)
    report.appendPara("________________________________________________________________________________________________________________________",styles.separator)

def addTopSections(report, styles):
	top_sections = report.data["top_sections"]
	for top_section in top_sections:
		title = top_section["title"]
		image = top_section["icon"]
		text = top_section["text"]
		
		addTopSection(report, styles, title, image, text)