import arabic_reshaper
from bidi.algorithm import get_display
from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.platypus import Paragraph
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY


def get_arabic_font(text):
    styles = getSampleStyleSheet()
    arabic_text_style = ParagraphStyle(
        'border',
        parent=styles['Normal'],
        # borderColor='#333333',
        borderWidth=1,
        borderPadding=2,
        fontName="Arabic",
        fontSize = 12,
        alignment = TA_RIGHT
    )
    pdfmetrics.registerFont(TTFont('Arabic', 'fonts/trado/trado.ttf'))
    arabic_text = text
    rehaped_text = arabic_reshaper.reshape(arabic_text)
    bidi_text = get_display(rehaped_text)

    arabic_text = Paragraph(bidi_text, arabic_text_style)
    return arabic_text


