from chi.reports.lib.core.chi_line import CHILine
from reportlab.platypus import Spacer

def addLine(report, x=0, y=0, color='#AAAAAA', width=0.8, lineSpace=5, keepWithNext=False):

    line = CHILine(x, y, strokeColor=color, lineWidth=width)
    p = Spacer(0,lineSpace)
    if keepWithNext == True:
        p.keepWithNext = True
        line.keepWithNext = True
    report.story.append(p)
    report.story.append(line)
    report.story.append(p)
    