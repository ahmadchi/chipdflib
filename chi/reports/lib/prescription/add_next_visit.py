import json
import os
from .add_line import addLine
from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch

def addNextVisit(report,styles):
    prescData = report.data
    data = []
    if "next_preferred_visit" in prescData:
        a = Paragraph("on "+prescData["next_preferred_visit"],styles.prescription)
        path = report.dir_path + '../patient_progress_report/images/progress-report/calendar.svg'
        image = SVGtoImage(path, 10, 10, 'fit')
        title = ParagraphAndImage(Paragraph("Next Preferred Visit", styles.prescriptionTitle), image, side='left')
        #p = Spacer(0, 0.1*inch)

        data=[[title, a]]


        t = Table(data
        ,style = [
            # ('GRID',(0,0),(0,1),1,'#000000')
            ('ALIGN',(1,0),(1,0),'RIGHT')
            ]
        ,colWidths=(265,265))
        report.story.append(t)
        t.keepWithNext = True
        addLine(report, 20, -3, lineSpace=10)
        #report.appendPara("__________________________________________________________________________________________________________________________",styles.separator)
