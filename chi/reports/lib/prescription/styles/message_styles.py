from reportlab.lib.colors import white
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY
from reportlab.lib import colors


class PrescriptionReportStyles():
    def __init__(self):
        styles = getSampleStyleSheet()

        self.homeCareTable = ParagraphStyle('yourtitle1',
                                            fontName="Helvetica",
                                            spaceBefore=5,
                                            spaceAfter=0,
                                            parent=styles['BodyText'],
                                            lineSpacing=12,
                                            alignment=TA_JUSTIFY,
                                            fontSize=12,
                                            leftIndent=12)
        self.nextPreferredVisit = ParagraphStyle('yourtitle1',
                                            fontName="Helvetica",
                                            spaceBefore=5,
                                            spaceAfter=0,
                                            parent=styles['BodyText'],
                                            lineSpacing=12,
                                            # alignment=TA_RIGHT,
                                            fontSize=12,
                                            leftIndent=12)
        self.labTestTable = ParagraphStyle('yourtitle1',
                                           fontName="Helvetica",
                                           leftIndent=12,
                                           parent=styles['Normal'],
                                           fontSize=12)
        self.separatorTable = ParagraphStyle('yourtitle',
                                             fontName="Helvetica",
                                             parent=styles['Normal'],
                                             lineSpacing=20,
                                             alignment=TA_LEFT,
                                             fontSize=8,
                                             leftIndent=12,
                                             textColor='#CCCCCC',
                                             borderWidth=1,
                                             borderPadding=5)
        self.separator = ParagraphStyle('yourtitle',
                                        fontName="Helvetica",
                                        parent=styles['Normal'],
                                        lineSpacing=20,
                                        alignment=TA_LEFT,
                                        # fontColor='#CCCCCC',
                                        fontSize=8,
                                        leftIndent=0,
                                        textColor='#CCCCCC',
                                        borderWidth=1,
                                        borderPadding=5)

        self.level1 = ParagraphStyle('level 1',
                                     # spaceBefore = 0,
                                     # spaceAfter=0,
                                     parent=styles['Normal'],
                                     # lineSpacing = 0,
                                     leftIndent=30,

                                     leading=12. * 1.2,
                                     fontSize=12)

        self.items = ParagraphStyle('yourtitle1',
                                    # spaceBefore = 0,
                                    # spaceAfter=0,
                                    parent=styles['Normal'],
                                    # lineSpacing = 0,
                                    leftIndent=30,
                                    leading=12. * 1.2,
                                    fontSize=12)

        self.itemsSeparator = ParagraphStyle('yourtitle1',
                                             spaceBefore=10,
                                             spaceAfter=10,
                                             parent=styles['Normal'],
                                             # lineSpacing = 0,
                                             leftIndent=30,
                                             fontSize=12)
        self.prescriptionSummary = ParagraphStyle('yourtitle1',
                                  fontName="Helvetica-Bold",
                                  #spaceBefore=10,
                                  spaceAfter=0,
                                  parent=styles['BodyText'],
                                #   lineSpacing=12,
                                  #alignment = TA_JUSTIFY,
                                  fontSize=12,
                                  leftIndent = 20,
                                #   leading = 10
                                  )

        self.prescription = ParagraphStyle('yourtitle1',
                                                   spaceBefore=10,
                                                   spaceAfter=0,
                                                   parent=styles['Normal'],
                                                #    lineSpacing=10,
                                                   #alignment=TA_JUSTIFY,
                                                   fontSize=10,
                                                   leftIndent = 20,
                                                #    leading = 14
                                                   )

        self.divider = ParagraphStyle('yourtitle',
                                              fontName="Helvetica",
                                              parent=styles['Normal'],
                                              leftIndent=15,
                                              alignment = TA_CENTER,
                                              spaceAfter=10,
                                              fontSize=8)
        self.common = ParagraphStyle('yourtitle',
                                      fontName="Helvetica",
                                      parent=styles['Normal'],
                                      leftIndent=15,
                                      alignment=TA_LEFT,
                                      fontSize=8)

        self.strength = ParagraphStyle('yourtitle',
                                     fontName="Helvetica",
                                     parent=styles['Normal'],
                                     leftIndent=0,
                                     alignment=TA_LEFT,
                                     fontSize=6)

        self.p1a = ParagraphStyle('yourtitle1',
                                  spaceBefore=0,
                                  spaceAfter=0,
                                  parent=styles['Normal'],
                                  lineSpacing=0,
                                  alignment=TA_CENTER,
                                  fontSize=8)

        self.prescriptionTitle = ParagraphStyle('yourtitle',
                                  fontName="Helvetica-Bold",
                                  parent=styles['Normal'],
                                  # spaceBefore=10,
                                  leftIndent=5,
                                  spaceAfter=0,
                                  lineSpacing=0,
                                  fontSize=12)

        self.p1c1 = ParagraphStyle('yourtitle',
                                   fontName="Helvetica-Bold",
                                   parent=styles['Normal'],
                                   spaceBefore=0,
                                   spaceAfter=0,
                                   lineSpacing=0,
                                   fontSize=10,
                                   alignment=TA_RIGHT
                                   )

        self.p1c2 = ParagraphStyle('yourtitle1',
                                   spaceBefore=0,
                                   spaceAfter=0,
                                   parent=styles['Normal'],
                                   lineSpacing=0,
                                   fontSize=8,
                                   alignment=TA_RIGHT)
        self.med_strength = ParagraphStyle('yourtitle1',
                                   spaceBefore=0,
                                   spaceAfter=0,
                                   parent=styles['Normal'],
                                   lineSpacing=0,
                                   fontSize=8,
                                   alignment=TA_LEFT)

        self.p1d = ParagraphStyle('yourtitle',
                                  fontName="Helvetica-Bold",
                                  parent=styles['Normal'],
                                  spaceBefore=0,
                                  spaceAfter=0,
                                  lineSpacing=0,
                                  alignment=TA_CENTER,
                                  # fontColor='#FF0000',
                                  fontSize=10,
                                  # textColor=Color(0,0,0,1)
                                  )

        self.activeMeds = ParagraphStyle('yourtitle',
                                         fontName="Helvetica-Bold",
                                         parent=styles['Normal'],
                                         fontSize=8)

        self.activeDiagnosis = ParagraphStyle('yourtitle',
                                              fontName="Helvetica",
                                              parent=styles['Normal'],
                                              leftIndent=20,
                                              fontSize=8)

        self.activeMedsH1 = ParagraphStyle('yourtitle1',
                                           fontName="Helvetica-Bold",
                                           parent=styles['Normal'],
                                           fontSize=12,
                                           spaceAfter=5
                                           )
        self.medicationSections = ParagraphStyle('yourtitle1',
                                           fontName="Helvetica-Bold",
                                           parent=styles['Normal'],
                                           fontSize=11,
                                           spaceAfter=5,
                                           leftIndent = 18
                                           )
                                           

        self.activeMedsH1Comp = ParagraphStyle('yourtitle1',
                                               fontName="Helvetica-Bold",
                                               parent=styles['Normal'],
                                               fontSize=12,
                                               spaceAfter=5,
                                               leftIndent=35
                                               )

        self.message_date = ParagraphStyle('yourtitle',
                                           fontName="Helvetica-Bold",
                                           fontSize=15,
                                           # leftIndent = 5,
                                           # rightIndent = 5,
                                           backColor='#1EC6DA',
                                           borderRadius=1,
                                           borderColor=None,
                                           borderWidth=1,
                                           borderPadding=5,
                                           parent=styles['Normal'],
                                           alignment=TA_LEFT,
                                           spaceBefore=15,
                                           spaceAfter=15,
                                           textcolor='#FFFFFF')

        self.active_medicine = ParagraphStyle('yourtitle',
                                              fontName="Helvetica-Bold",
                                              # leftIndent = -40,
                                              # rightIndent = 100,
                                              fontSize=10,
                                              backColor='#32CD32',
                                              borderRadius=0,
                                              borderColor=None,
                                              borderWidth=1,
                                              borderPadding=-1,
                                              parent=styles['BodyText'],
                                              alignment=TA_CENTER,
                                              spaceBefore=-5,
                                              spaceAfter=-5,
                                              textcolor='#FFFFFF',
                                              )
        self.active_medicine_name = ParagraphStyle('yourtitle',
                                                   fontName="Helvetica-Bold",
                                                   fontSize=10,
                                                   backColor='#FFFFFF',
                                                   borderRadius=0,
                                                   # leftIndent = -40,
                                                   borderColor=None,
                                                   borderWidth=0,
                                                   borderPadding=-1,
                                                   parent=styles['BodyText'],
                                                   alignment=TA_RIGHT,
                                                   spaceBefore=-10,
                                                   spaceAfter=-5,
                                                   textcolor='#000000',
                                                   )
        self.cancelled_medicine = ParagraphStyle('yourtitle',
                                                 fontName="Helvetica-Bold",
                                                 fontSize=10,
                                                 # leftIndent = 5,
                                                 # rightIndent = 100,
                                                 backColor='#FF3333',
                                                 borderRadius=0,
                                                 borderColor=None,
                                                 borderWidth=1,
                                                 borderPadding=0,
                                                 parent=styles['BodyText'],
                                                 alignment=TA_LEFT,
                                                 spaceBefore=0,
                                                 spaceAfter=-5,
                                                 textcolor=colors.floralwhite
                                                 )

        self.cancelled_medicine_name = ParagraphStyle('yourtitle',
                                                      fontName="Helvetica-Bold",
                                                      fontSize=10,
                                                      # leftIndent = 5,
                                                      # rightIndent = 100,
                                                      backColor='#FFFFFF',
                                                      borderRadius=0,
                                                      borderColor=None,
                                                      borderWidth=1,
                                                      borderPadding=5,
                                                      parent=styles['BodyText'],
                                                      alignment=TA_LEFT,
                                                      spaceBefore=0,
                                                      spaceAfter=-5,
                                                      textcolor='#000000',
                                                      )

        self.message_subject = ParagraphStyle('yourtitle',
                                              fontName="Helvetica-Bold",
                                              fontSize=10,
                                              leftIndent=5,
                                              rightIndent=5,
                                              backColor='#FFA500',
                                              borderRadius=0,
                                              borderColor=None,
                                              borderWidth=1,
                                              borderPadding=5,
                                              parent=styles['Normal'],
                                              alignment=TA_LEFT,
                                              spaceBefore=25,
                                              spaceAfter=10,
                                              textcolor=colors.floralwhite)

        self.message_body_to = ParagraphStyle('yourtitle',
                                              # fontName="Helvetica",
                                              fontName='Arabic-normal', fontSize=12, leading=12. * 1.2,
                                              # fontSize=10,
                                              # leftIndent = 45,
                                              # rightIndent = 5,
                                              backColor="#ccFFcc",
                                              borderColor=None,
                                              borderRadius=2,
                                              borderWidth=0.5,
                                              borderPadding=5,
                                              parent=styles['Normal'],
                                              alignment=TA_RIGHT,
                                              spaceAfter=5,
                                              )

        self.message_body_from = ParagraphStyle('yourtitle',
                                                fontName='Arabic-normal', fontSize=12, leading=12. * 1.2,
                                                # fontName="Helvetica",
                                                # fontSize=10,
                                                leftIndent=5,
                                                rightIndent=45,
                                                backColor=colors.lavender,
                                                borderColor=None,
                                                borderRadius=15,
                                                borderWidth=0.5,
                                                borderPadding=5,
                                                parent=styles['Normal'],
                                                alignment=TA_LEFT,
                                                spaceAfter=5,
                                                )

        self.message_to = ParagraphStyle('yourtitle',
                                         fontName="Helvetica",
                                         fontSize=9,
                                         leftIndent=45,
                                         rightIndent=5,
                                         backColor=None,
                                         borderColor=None,
                                         borderWidth=0,
                                         borderPadding=1,
                                         parent=styles['Italic'],
                                         alignment=TA_RIGHT,
                                         spaceAfter=5,
                                         )

        self.message_from = ParagraphStyle('yourtitle',
                                           fontName="Helvetica",
                                           fontSize=9,
                                           leftIndent=5,
                                           rightIndent=45,
                                           backColor=None,
                                           borderColor=None,
                                           borderWidth=0,
                                           borderPadding=1,
                                           parent=styles['Italic'],
                                           alignment=TA_LEFT,
                                           spaceBefore=10,
                                           spaceAfter=5,
                                           )
