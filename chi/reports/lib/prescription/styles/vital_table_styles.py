from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER
from reportlab.lib import colors

class ProgressReportStyles():
  def __init__(self):

    styles = getSampleStyleSheet()

    self.p1a = ParagraphStyle('yourtitle1',
                                     spaceBefore = 0,
                                     spaceAfter=0,
                                     parent=styles['Normal'],
                                     lineSpacing = 0,
                                     fontSize=8)
    self.p1b = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     parent=styles['Normal'],
                                     spaceBefore = 0,
                                     spaceAfter=0,
                                     lineSpacing = 0,
                                     fontSize=10)

    self.p1c = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     parent=styles['Normal'],
                                     spaceBefore = 0,
                                     spaceAfter=0,
                                     lineSpacing = 0,
                                     fontSize=10,
                                    alignment=TA_RIGHT
                                    )

    self.p1d = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     parent=styles['Normal'],
                                     spaceBefore = 0,
                                     spaceAfter=0,
                                     lineSpacing = 0,
                                     alignment=TA_CENTER,
                                     #fontColor='#FF0000',
                                     fontSize=10,
                                     # textColor=Color(0,0,0,1)
                                     )

    self.activeMeds = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     parent=styles['Normal'],
                                     fontSize=8)

    self.activeDiagnosis = ParagraphStyle('yourtitle',
                                     fontName="Helvetica",
                                     parent=styles['Normal'],
                                     leftIndent = 20,
                                     fontSize=8)


    self.activeMedsH1 = ParagraphStyle('yourtitle1',
                                 fontName="Helvetica-Bold",
                                 parent=styles['Normal'],
                                 fontSize=12,
                                 spaceAfter = 5
                                 )

    self.message_date = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     fontSize=10,
                                     leftIndent = 5,
                                     rightIndent = 5,
                                     backColor='#FF0000',
                                     borderRadius=0,
                                     borderColor=None,
                                     borderWidth=1,
                                     borderPadding=5,
                                     parent=styles['Normal'],
                                     alignment=TA_LEFT,
                                     spaceBefore = 25,
                                     spaceAfter=10,
                                     textcolor=colors.floralwhite)
    self.active_medicine_date = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     fontSize=10,
                                     leftIndent = 5,
                                     rightIndent = 100,
                                     backColor='#32CD32',
                                     borderRadius=0,
                                     borderColor=None,
                                     borderWidth=1,
                                     borderPadding=5,
                                     parent=styles['Normal'],
                                     alignment=TA_LEFT,
                                     spaceBefore = 25,
                                     spaceAfter=10,
                                     textcolor='#FFFFFF',
                                     )
    self.cancelled_medicine_date = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     fontSize=10,
                                     leftIndent = 5,
                                     rightIndent = 100,
                                     backColor='#FF3333',
                                     borderRadius=0,
                                     borderColor=None,
                                     borderWidth=1,
                                     borderPadding=5,
                                     parent=styles['Normal'],
                                     alignment=TA_LEFT,
                                     spaceBefore = 25,
                                     spaceAfter=10,
                                     textcolor=colors.floralwhite
                                     )

    self.message_subject = ParagraphStyle('yourtitle',
                                     fontName="Helvetica-Bold",
                                     fontSize=10,
                                     leftIndent = 5,
                                     rightIndent = 5,
                                     backColor='#FFA500',
                                     borderRadius=0,
                                     borderColor=None,
                                     borderWidth=1,
                                     borderPadding=5,
                                     parent=styles['Normal'],
                                     alignment=TA_LEFT,
                                     spaceBefore = 25,
                                     spaceAfter=10,
                                     textcolor=colors.floralwhite)

    self.message_body_to = ParagraphStyle('yourtitle',
                                  fontName="Helvetica",
                                  fontSize=10,
                                     leftIndent = 45,
                                     rightIndent = 5,
                                  backColor="#ccFFcc",
                                  borderColor=None,
                                  borderWidth=0.5,
                                  borderPadding=5,
                                  parent=styles['Normal'],
                                     alignment=TA_RIGHT,
                                  spaceAfter=5,
                                  )

    self.message_body_from = ParagraphStyle('yourtitle',
                                  fontName="Helvetica",
                                  fontSize=10,
                                     leftIndent = 5,
                                     rightIndent = 45,
                                  backColor=colors.lavender,
                                  borderColor=None,
                                  borderRadius = 15,
                                  borderWidth=0.5,
                                  borderPadding=5,
                                  parent=styles['Normal'],
                                     alignment=TA_LEFT,
                                  spaceAfter=5,
                                  )


    self.message_to = ParagraphStyle('yourtitle',
                                fontName="Helvetica-Oblique",
                                fontSize=9,
                                     leftIndent = 45,
                                     rightIndent = 5,
                                backColor=None,
                                borderColor=None,
                                borderWidth=0,
                                borderPadding=1,
                                parent=styles['Italic'],
                                alignment=TA_RIGHT,
                                spaceAfter=5,
                                )

    self.message_from = ParagraphStyle('yourtitle',
                                fontName="Helvetica-Oblique",
                                fontSize=9,
                                     leftIndent = 5,
                                     rightIndent = 45,
                                backColor=None,
                                borderColor=None,
                                borderWidth=0,
                                borderPadding=1,
                                parent=styles['Italic'],
                                alignment=TA_LEFT,
                                spaceAfter=5,
                                )
