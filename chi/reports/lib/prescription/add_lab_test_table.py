import os
from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import  Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch

from .add_line import addLine

def addLabTestTable(report,styles):
    
    prescData = report.data
    labData = prescData.get("lab_tests",[])

    if len(labData) > 0:

        p = Spacer(0,5)
        path = report.dir_path + '../patient_progress_report/images/progress-report/lab_test.svg'
        image = SVGtoImage(path, 10, 10, 'fit')
        title = ParagraphAndImage(Paragraph("Lab Tests", styles.prescriptionTitle), image, side='left')
        
        data = [[title]]
        t = Table(data
        # ,style = [('GRID',(0,0),(-1,-1),1,'#000000')]
        ,colWidths=[530])
        
        report.story.append(t)
        t.keepWithNext = True
        report.story.append(p)
        p.keepWithNext = True

        for test in prescData["lab_tests"]:
            if test["checked"]:
                a = Paragraph("- "+test["name"]+"<font size=8>"+"  Complete as directed"+"</font>",styles.prescription)
                data=[[a]]
                t = Table(data
                # ,style = [('GRID',(0,0),(-1,-1),1,'#000000')]
                ,colWidths=[530])
                report.story.append(t)
                t.keepWithNext = True
                addLine(report,20,-3,keepWithNext = True)
        p1 = Spacer(0,5)
        report.story.append(p1)
        
                
        