import json
import os
from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage
from .add_line import addLine


def addHomeCareTable(report, styles):
    
    prescData = report.data

    data1 = []
    data2 = []
    data3 = []

    if "homecare" in prescData:
        for items in prescData["homecare"]:
            a = Paragraph(" -  " + items["name"], styles.prescription)
            data1.append(a)

            b = Paragraph(items["timings"], styles.prescription)
            data2.append(b)
            
            c = Paragraph(items["days"], styles.prescription)
            data3.append(c)

        path = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
        image = SVGtoImage(path, 15, 15, 'fit')
        title = ParagraphAndImage(Paragraph("Homecare", styles.prescriptionTitle), image, side='left')
        
        timing = Paragraph("Timings", styles.prescription)
        day = Paragraph("Days", styles.prescription)
        #p = Paragraph("_________________________________________________________________________________________________________________",styles.labTestTable)
        p = Spacer(0,5)
        data4 = []
        data4 = [[title, timing, day]]
        t = Table(data4
        # ,style = [('GRID',(0,0),(-1,-1),1,'#000000')]
        ,colWidths=[200, 180, 150])
         
        report.story.append(t)
        t.keepWithNext = True 
        report.story.append(p)
        p.keepWithNext = True 



        data5 = []
        for i in range(len(data1)):
            data5 = [[data1[i], data2[i], data3[i]]]
            t = Table(data5
            # ,style = [('GRID',(0,0),(-1,-1),1,'#000000')]
            ,colWidths=[200,180,150])
            report.story.append(t)
            t.keepWithNext = True
            if i == (len(data1)-1):
                addLine(report,20,-3,keepWithNext=False)
                # line = CHILine(15,-3, strokeColor='#CCCCCC', lineWidth=0.7)
                # report.story.append(p)
                # report.story.append(line)
                # report.story.append(p)
            else:
                addLine(report,20,-3,keepWithNext=True)
                # line = CHILine(15,-3, strokeColor='#CCCCCC', lineWidth=0.7)
                # report.story.append(p)
                # report.story.append(line)
                # report.story.append(p)