from reportlab.platypus import KeepInFrame
# from chi.reports.lib.core.chi_line import CHILine
from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Spacer, ParagraphAndImage, Paragraph
from .add_line import addLine

def addTopSection(report, styles, title, image, text):
    
    # p = Spacer(0,5)
    # line = CHILine(15,-7, strokeColor='#CCCCCC', lineWidth=0.7)

    if title == "Summary":
        report.appendPara(title, styles.prescriptionSummary)
        report.appendPara(text, styles.prescription)
        addLine(report, 20, -3, lineSpace=10)
        # report.story.append(p)
        # report.story.append(line)
        # report.story.append(p)
        
		
    elif title == "Diagnosis":
        path = report.dir_path + '../patient_progress_report/images/progress-report/search.svg'
        image = SVGtoImage(path, 10, 10, 'fit')
        report.story.append(ParagraphAndImage(Paragraph("Diagnosis", styles.prescriptionTitle), image, side='left'))
        # report.appendPara(title, styles.prescriptionTitle)
        report.appendPara(text, styles.prescription)
        addLine(report, 20, -3, lineSpace=10)
        # report.story.append(p)
        # report.story.append(line)
        # report.story.append(p)
        
	
def addTopSections(report, styles):
	top_sections = report.data["top_sections"]
	for top_section in top_sections:
		title = top_section["title"]
		image = top_section["icon"]
		text = top_section["text"]
		
		addTopSection(report, styles, title, image, text)