

def configureBlueLetterhead(prescData):

    patientData = prescData.get("patient_info", None)
    patient_name = ""
    hospital_no = ""
    gender = ""
    age = ""
    if patientData is not None:
        patient_name = patientData["full_name"]
        hospital_no = patientData["hospital_no"]
        gender = patientData["gender"]
        age = patientData["age"]

    doctorData = prescData.get("doctor_info", [])
    doctor_name = ""
    details = ""
    if doctorData is not []:
        doctor_name = doctorData["full_name"]
        details = doctorData["details"]

    
    letterheadDict = {
        "template": "BLUE_LETTERHEAD",
        "page_size" : [595, 842],
        "leftMargin" : 25,
        "rightMargin" : 35,
        "topMargin" : 20,
        "bottomMargin" : 20,
        "blocks": [
            {
                "type": "IMAGE",
                "filename": "letterhead1a.svg",
                "x": 0,
                "y": 0,
                "w": 595,
                "h": 842,
                "file_type": "svg"
            },
            # {
            #     "type": "IMAGE",
            #     "filename": "letterhead2.svg",
            #     "x": 0,
            #     "y": 0,
            #     "w": 595,
            #     "h": 842,
            #     "file_type": "svg"
            # },
            {
                "type": "TEXT",
                "anchorX": 440,
                "anchorY": 806,
                "fontName": 'Helvetica-Bold',
                "fontSize": 15,
                "align": "LEFT",
                "text": doctor_name
                # "text": prescData["doctor_info"]["full_name"],
            },
            {
                "type": "TEXT",
                "anchorX": 440,
                "anchorY": 790,
                "fontName": 'Helvetica',
                "fontSize": 10,
                "align": "LEFT",
                "text":details
                # "text": prescData["doctor_info"]["details"]
            },
            {
                "type": "TEXT",
                "anchorX": 440,
                "anchorY": 750,
                "fontName": 'Helvetica-Bold',
                "fontSize": 10,
                "align": "LEFT",
                "text": ["Dated: 16/06/2020 12:45 PM"]
            },
            {
                "type": "TEXT",
                # "anchorX": 297,
                # "anchorY": 767,
                "anchorX": 250,
                "anchorY": 775,
                "fontName": 'Helvetica-Bold',
                "fontSize": 10,
                "align": "LEFT",
                "text" : ["Patient: "+patient_name, hospital_no, gender + " | " + f'{age}']
                # "text": [prescData["patient_info"]["full_name"], prescData["patient_info"]["hospital_no"]]
            },
            {
                "type": "TEXT",
                "anchorX": 107,
                "anchorY": 100,
                "fontName": 'Helvetica',
                "fontSize": 8,
                "align": "LEFT",
                "text": ["Printed by Maryam Raza Akhtar on Dated: 28-Nov-2001", "For Appointments call",
                         "(051) 111 121 154 -Ext : 3232 "]
            },

            # {
            #     "type": "SEPARATOR",
            #     "anchorY": 730
            # },

            # {
            #     "type": "RECT",
            #     "x": 30,
            #     "y": 30,
            #     "w": 100,
            #     "h": 200,
            #     "color": "#448888",
            #     "line_width": 1,
            #     "transparent": 1
            # },
            # {
            #     "type": "RECT",
            #     "x": 30,
            #     "y": 30,
            #     "w": 100,
            #     "h": 200,
            #     "color": "#448888",
            #     "line_width": 1,
            #     "transparent": 1
            # },

            # {
            #     "type": "IMAGE",
            #     "filename": "logo1x.png",
            #     "x": 30,
            #     "y": 771,
            #     "w": 50,
            #     "h": 50,
            #     "file_type": "png"
            # },
            # {
            #     "type": "LOGO_TEXT",
            #     "anchorX": 55,
            #     "anchorY": 767,
            #     "fontName": 'Helvetica',
            #     "fontSize": 7,
            #     "align": "CENTER",
            #     "text": ["Cognitive", "Healthcare", "International"]
            # },
            # {
            #     "type": "IMAGE_LOGO",
            #     "filename": "updated-logo.svg",
            #     "x": 0,
            #     "y": 0,
            #     "w": 200,
            #     "h": 200,
            #     "file_type": "svg"
            # },
       
            # {
            #     "type": "IMAGE",
            #     "filename": "BG_Arch.svg",
            #     "x": 0,
            #     "y": 60,
            #     "w": 700,
            #     "h": 600,
            #     "file_type": "svg"
            # },
            # {
            #     "type": "IMAGE",
            #     "filename": "Left_Bottom_Arch.svg",
            #     "x": 0,
            #     "y": 0,
            #     "w": 200,
            #     "h": 200,
            #     "file_type": "svg"
            # },
            # {
            #     "type": "IMAGE",
            #     "filename": "CHI_PROMO.svg",
            #     "x": 380,
            #     "y": 30,
            #     "w": 200,
            #     "h": 200,
            #     "file_type": "svg"
            # },
            # {
            #     "type": "IMAGE",
            #     "filename": "logo1x.png",
            #     "x": 377,
            #     "y": 87,
            #     "w": 20,
            #     "h": 20,
            #     "file_type": "png"
            # }
            {
                "type": "IMAGE",
                "filename": "logo1x.png",
                "x": 347,
                "y": 85,
                "w": 18,
                "h": 18,
                "file_type": "png"
            }
        ]
    }
    return letterheadDict
