import os
from datetime import datetime


def patientHeader(canvas, doc):
    pData = doc.data["patient_info"]
    doctData = doc.data["doctor_info"]

    if (canvas.getPageNumber() == 1):
        titlePageAnnual(canvas, doc)

    paint = doc.paint
    rightEdge = doc.pagesize[0] - doc.rightMargin
    topEdge = doc.pagesize[1] - doc.topMargin
    leftEdge = doc.leftMargin
    paint.setFillColor("#555555")

    paint.setAlign("LEFT")
    fontSize = 50
    paint.setAnchor(leftEdge + 10, topEdge - fontSize)
    paint.setFontSize(fontSize)

    # cwd = os.getcwd()
    # print(cwd)
    # exit()
    im = os.path.join(os.getenv('BASE_PATH'), 'lib/images/logo1x.png')
    paint.paintImage(im)

    paint.setAnchor(leftEdge + (paint.fontSize / 2) + 10, topEdge - paint.fontSize * 1.1)
    paint.setFontSize(7)
    paint.setFontName("Helvetica")
    paint.setAlign("CENTER")
    paint.paintText(["Cognitive", "Healthcare", "International"])

    pData = doc.bpb
    full_name = pData["full_name"]

    if (full_name != ""):
        # hospital_no = pData["hospital_no"]
        # gender = pData["gender"]
        # age = pData["age"]
        # paint.paintText([full_name, gender + " | " + age, "Hospital : " + hospital_no])
        paint.setAlign("CENTER")
        paint.setAnchor(doc.pagesize[0] / 2, topEdge - 55)
        paint.setFontSize(10)
        paint.setFontName("Helvetica-Bold")
        paint.paintText("Patient: " + full_name)

    doct_name = doctData[0]["physician.full_name"]
    if (doct_name != ""):
        paint.setAlign("LEFT")
        paint.setAnchor(rightEdge - 135, topEdge - 15)
        paint.setFontSize(10)
        paint.setFontName("Helvetica-Bold")
        landline = "Ph: (051) " + doctData[0]["landline_phone"]
        mobile = "(+92) " + doctData[0]["mobile_phone"]
        specialist = doctData[0]["specialty"]
        paint.paintText(doct_name)
        paint.setFontName("Helvetica")
        paint.setAnchor(rightEdge - 135, topEdge - 15 - paint.fontSize)
        paint.paintText([specialist, landline, mobile, ""])

    paint.setFontName("Helvetica-Bold")
    # tm = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    tm = datetime.today().strftime('%d-%m-%Y %H:%M %p')
    paint.paintText(["Dated: " + tm])

    # paint.setAlign("CENTER")
    # paint.setAnchor(doc.pagesize[0] / 2, topEdge - 55)
    # paint.setFontSize(10)
    # paint.paintText("Patient: " + full_name)
    # tm = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    # paint.paintText(["Report Generated on", tm])

    paint.curY = topEdge - 85
    paint.separator()

    # Footer
    paint.setAlign("RIGHT")
    paint.setAnchor(rightEdge, doc.bottomMargin + 15)
    # pageNumber = "Page %s" % (doc.pageNumber)
    paint.paintText(str(canvas.getPageNumber()))

    paint.setAlign("LEFT")
    paint.setAnchor(leftEdge, doc.bottomMargin + 15)
    footnote = f"Added by Admin CHI on {datetime.date(datetime.now())}"
    paint.paintText(footnote)

    paint.curY = doc.bottomMargin + 20
    paint.separator()


def titlePageAnnual(canvas, doc):
    paint = doc.paint
    rightEdge = doc.pagesize[0] - doc.rightMargin
    topEdge = doc.pagesize[1] - doc.topMargin
    leftEdge = doc.leftMargin

    pw = doc.pagesize[0]
    ph = doc.pagesize[1]
    sc = .8
    y = ph / 2 - doc.width * sc / 2  # - doc.bottomMargin

    im = os.path.join(os.getenv('BASE_PATH'), 'lib/patient_progress_report/images/steth.jpg')

    paint.paintImageRect(im, 0, y, doc.pagesize[0], doc.width * sc)
    paint.drawRect(leftEdge, topEdge, doc.width, doc.height, line_width=5)
    paint.drawRect(pw / 3 + 20, ph, pw / 2 + 20, ph, color="#ffd000", transparent=0.8, line_width=0)
    paint.drawRect(pw / 3 + 40, ph - 285, 10, 300, color="black", line_width=1)

    paint.setAlign("LEFT")
    paint.setAnchor(pw / 2.5 + 20, ph - 350)
    paint.setFontSize(40)

    paint.setFontName("Helvetica-Bold")
    paint.paintText(["PATIENT", "PRESCRIPTION"])
    paint.setFontSize(60)
    paint.setFillColor("white")
    paint.paintText("REPORT")

    paint.setFontSize(20)
    paint.paintText([""])
    paint.setFillColor("black")
    paint.setFontName("Helvetica")
    fullName = doc.data["patient_info"]["full_name"]

    tm = datetime.today().strftime('%Y-%m-%d %H:%M:%S')

    paint.paintText([fullName, "Report Generated on", tm])
    canvas.showPage()
