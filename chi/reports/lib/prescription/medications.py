from reportlab.platypus import Table, Paragraph, ParagraphAndImage, Image, Spacer
from chi.reports.lib.core.chi_image import SVGtoImage
from chi.reports.lib.prescription.detect_arabic_font import get_arabic_font
from chi.reports.lib.prescription.styles.message_styles import PrescriptionReportStyles
import json
from .add_line import addLine

def add_medication_list(report, style):
    styles = PrescriptionReportStyles()

    p = Spacer(0,5)
    path = report.dir_path + '../patient_progress_report/images/progress-report/medication.svg'
    image = SVGtoImage(path, 10, 10, 'fit')
    report.story.append(ParagraphAndImage(Paragraph("Medication", styles.prescriptionTitle), image, side='left'))
    report.story.append(p)
    prescData = report.data
    sections = prescData.get('sections', [])
    for section in sections:
        if len(section["medicines"]) > 0:
            p = Spacer(0,5)
            report.story.append(p)
            
            p = Paragraph("- " + section["section_name"], styles.medicationSections)
            p.keepWithNext = True
            report.story.append(p)
            count = 0
            data = []
            
            for medicine in section["medicines"]:
                count += 1
                med = medicine["medicine_name"]
                strength = "<font size=6>" + medicine["strength"] + "</font>"
                medication = Paragraph(str(count) + "- " + med + "  " + strength, styles.prescription)

                no_of_days = "For " + str(medicine['no_of_days']) + " days"
                noOfDays = Paragraph(no_of_days, styles.prescription)

                dosage = json.loads(medicine["dosage"])
                morning = dosage[0]["value"]
                noon = dosage[1]["value"]
                evening = dosage[2]["value"]

                if morning == '':
                    morning = '0'
                if noon == '':
                    noon = '0'
                if evening == '':
                    evening = '0'
                 

                dose = morning + " + " + noon + " + " + evening
                dosage = Paragraph(str(dose), styles.prescription)
                doseInt = int(morning) + int(noon) + int(evening)
                if doseInt == 3:
                    a = (get_arabic_font(u"صبح ، دوپهر ، شام"))
                elif doseInt == 2:
                    a = (get_arabic_font(u"صبح ، شام"))
                elif doseInt == 1:
                    a = (get_arabic_font(u"ايك بار"))
                else:
                    a = (get_arabic_font(u"- "))

                data = [[medication, noOfDays, dosage, a]]
                t = Table(data,

                # style=[('GRID', (0, 0), (-1, -1), 1, '#000000')],
                        colWidths=[210, 115, 115, 90])
                report.story.append(t)
                t.keepWithNext = True
                
                addLine(report,20,-3,keepWithNext=True)
                # b = Paragraph(
                #     "_________________________________________________________________________________________________________________",
                #     styles.separatorTable)
                # report.story.append(b)
                # b.keepWithNext = True
            p1 = Spacer(0,1)
            report.story.append(p1)
            # b.keepWithNext = False