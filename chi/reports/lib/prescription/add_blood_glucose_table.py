import json
import os

from chi.reports.lib.core.chi_image import SVGtoImage
from reportlab.platypus import Paragraph, Spacer, Table
from reportlab.platypus.flowables import ParagraphAndImage


def addBloodGlucoseTable(report, styles):
    
    prescData = report.data

    if "bg_monitoring_data" in prescData:
        
        p1 = Spacer(0,15)
        path = report.dir_path + '../patient_progress_report/images/progress-report/bsr1.svg'
        image = SVGtoImage(path, 15, 15, 'fit')
        title = ParagraphAndImage(Paragraph("Blood Glucose Monitoring:", styles.prescriptionTitle), image, side='left')
        report.story.append(title)
        title.keepWithNext = True
        report.story.append(p1)
        p1.keepWithNext = True

        bgData = json.loads(prescData["bg_monitoring_data"])
        data0 = [["Days", "Before \nBraekfast", "2 Hours \nAfter Breakfast", "Before \nLunch", "2 Hours \nAfter Lunch", "Dinner", "Bed Time"]]
        t = Table(data0,style = 
        [('VALIGN',(0,0),(6,0),'MIDDLE'),('GRID',(0,0),(6,0),1,'#000000')],
        colWidths=[40,80,80,80,80,80])
        report.story.append(t)
        t.keepWithNext = True
        
        for index, items in enumerate(bgData["monitoring_data"],1):
            data1 = []
            data2 = []
            background = []
            data1.append("Day"+str(index))
            for index, value in enumerate(items["value"],1):
                if str(value["selected"]) == "True":
                    a = ('BACKGROUND',(index,0),(index,0),'#AAAAAA')
                    background.append(a)

                #data1.append(str(value["selected"]))
                data1.append("")
            data2 = [data1]   
            background.append(('GRID',(0,0),(6,14),1,'#000000'))
            t = Table(data2,style = background,colWidths=[40,80,80,80,80,80])
            #('BACKGROUND',(0,0),'#000000')]
            #,colWidths=[70,70,70,70,70,70])
            report.story.append(t) 
            t.keepWithNext = True
        p2 = Spacer(0,15)
        report.story.append(p2)



        #print(data2)
        # exit()

            
            # t = Table(values)
            # report.story.append(t)
                

        
        # a = Paragraph(" -  " + items["name"], styles.homeCareTable)
        # data1.append(a)

        # b = Paragraph(items["timings"], styles.homeCareTable)
        # data2.append(b)
        
        # c = Paragraph(items["days"], styles.homeCareTable)
        # data3.append(c)

        # path = report.dir_path + '../patient_progress_report/images/progress-report/biotech.svg'
        # image = SVGtoImage(path, 10, 10, 'fit')
        # title = ParagraphAndImage(Paragraph("Homecare", styles.activeMedsH1), image, side='left')
        
        # timing = Paragraph("Timings", styles.homeCareTable)
        # day = Paragraph("Days", styles.homeCareTable)
        # p = Paragraph("_________________________________________________________________________________________________________________",styles.labTestTable)

        # data4 = []
        # data4 = [[title, timing, day]]
        # t = Table(data4,colWidths=[200, 200, 155])
        # report.story.append(t)

        # data5 = []
        # for i in range(len(data1)):
        #     data5 = [[data1[i], data2[i], data3[i]]]
        #     t = Table(data5,colWidths=[200, 200, 155])
        #     report.story.append(t)
        #     if i == (len(data1)-1):
        #         report.appendPara("________________________________________________________________________________________________________________________",styles.separator)
        #     else:
        #         report.appendPara("_________________________________________________________________________________________________________________",styles.separatorTable)