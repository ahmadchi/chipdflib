from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Frame, PageTemplate, KeepInFrame
from reportlab.pdfgen import canvas

from chi.reports.lib.prescription.add_top_sections import addTopSections
from chi.reports.lib.prescription.medications import add_medication_list
from chi.reports.lib.core.chi_report_template import CHIReportTemplate
from chi.reports.lib.core.chi_before_page_break import CHIBeforePageBreak
from chi.reports.lib.core.chi_line import CHILine


from .add_home_care_table import addHomeCareTable
from .add_blood_glucose_table import addBloodGlucoseTable
from .add_next_visit import addNextVisit
from .add_procedure_performed_table import addProcPerformedTable
from .add_lab_test_table import addLabTestTable
from .styles.message_styles import PrescriptionReportStyles
from .letterhead import letterHead
from ..core.chi_image import SVGtoImage

from .letterhead_blue import configureBlueLetterhead
from .letterhead_red import configureRedLetterhead

def addBluePageTemplates(doc):
    sb = 0
    footerHeight = 20 + 10

    doc.headerHeight = 90

    leftMargin = doc.leftMargin
    bottomMargin = doc.bottomMargin + 20 + 15 + 75
    frameHeight = doc.height - doc.headerHeight - footerHeight - 10 - 80


    # print(leftMargin)
    # exit()
    frame1 = Frame(leftMargin, bottomMargin, doc.width, frameHeight, id='col1', showBoundary=sb)
    Page = PageTemplate(id='col1', frames=[frame1])

    doc.addPageTemplates([Page])



def Prescription(dataArr, letterHeadData):
    headerHeight = 90
    
    if (letterHeadData == "BLUE_LETTERHEAD"):
        letterHeadTemplate = configureBlueLetterhead(dataArr)
    if (letterHeadData == "RED_LETTERHEAD"):
        letterHeadTemplate = configureRedLetterhead(dataArr)
    else:
        print("format not found", letterHeadData)
        letterHeadTemplate = configureBlueLetterhead(dataArr)
        
    leftMargin = letterHeadTemplate["leftMargin"]
    rightMargin = letterHeadTemplate["rightMargin"]
    topMargin = letterHeadTemplate["topMargin"]
    bottomMargin = letterHeadTemplate["bottomMargin"]

    # print(leftMargin)
    # exit()

    pagesize = (letterHeadTemplate["page_size"][0], letterHeadTemplate["page_size"][1])
    report = CHIReportTemplate(headerHeight, pagesize, tableOfContents = False, titlePage = False,
                             leftMargin= leftMargin,
                             rightMargin= rightMargin,
                             topMargin= topMargin,
                             bottomMargin= bottomMargin)

    report.pageCallback = letterHead
    report.letterHeadTemplate = letterHeadTemplate
    # return report
    #print(letterHeadTemplate.keys())
    addBluePageTemplates(report)

    styles = PrescriptionReportStyles()

    data = dataArr
    report.data = data
    # report.prescriptionData = data 
    # pData = data["patient_info"]
    # full_name = pData["full_name"]
    # hospital_no = pData["hospital_no"]
    # gender = pData["gender"]
    # age = pData["age"]
    
    # chiBPB = CHIBeforePageBreak(
    #     {
    #         "full_name": full_name,
    #         "hospital_no": hospital_no,
    #         "gender": gender,
    #         "age": age,
    #     }, report)
    # report.story.append(chiBPB)

    if (report.toc):
        report.breakPage()
    
    # line = CHILine()
    # report.story.append(line)

    addTopSections(report, styles)
    add_medication_list(report, data)
    addNextVisit(report, styles)

    addLabTestTable(report, styles)
    addHomeCareTable(report, styles)
     
    addBloodGlucoseTable(report, styles)

   
    
    addProcPerformedTable(report, styles)




    chiBPB = CHIBeforePageBreak({"full_name": ""}, report)
    report.story.append(chiBPB)

    print("==========================publishing=====================")
    report.publish()
    return report
