from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm , inch
from reportlab.platypus import Flowable, Paragraph, Spacer, SimpleDocTemplate

class CHILine(Flowable):
    def __init__(self, x1=0, y=0, x2=100, strokeColor='#000000',lineWidth=1,lineSpacing=0):
        Flowable.__init__(self)
        self.x1 = x1
        self.y1 = y
        self.x2 = x2
        self.y2 = y
        self.strokeColor = strokeColor
        self.lineWidth = lineWidth
        self.lineSpacing = lineSpacing
        self.styles = getSampleStyleSheet()

    def coord(self, x, y, unit=1):
        """
        http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
        Helper class to help position flowables in Canvas objects
        """
        x, y = x * unit, self.y2 -  y * unit
        return x, y

    def wrap(self,availWidth,availHeight):
        #print("w,h ", availWidth, availHeight)
        # self.aw = availWidth
        # self.ah = availHeight
        self.x2 = availWidth

        # self.y1 = availHeight
        # self.y2 = availHeight

        return self.width, self.height
        
    def draw(self):
        """
        Draw the shape, text, etc
        """
        ##Rectangle
        # self.canv.setFillColor(colors.lavender)
        self.canv.setLineWidth(self.lineWidth)
        self.canv.setStrokeColor(self.strokeColor)
        #self.canv.setFillColor('#BBBBBB')
        # Spacer(0,self.lineSpacing)
        self.canv.line(self.x1, self.y1, self.x2, self.y2)
        #self.canv.rect(self.x1, self.y1, self.x2, self.y2, fill=1, stroke=0)
        # Spacer(0,self.lineSpacing)
        # p = Paragraph(self.text, style=self.styles["Normal"])
        # p.wrapOn(self.canv, self.x2, self.y2)
        # p.drawOn(self.canv, *self.coord(38, 3, mm))
    

# doc = SimpleDocTemplate("maaz.pdf",pagesize=A4)
# story=[]
# styles = getSampleStyleSheet()
# box = CHILine()
# story.append(box)
# story.append(Spacer(0,1*inch)) 
# box = CHILine()
# story.append(box)
# doc.build(story)