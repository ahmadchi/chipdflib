from datetime import datetime


class CHIUtils:
    def __init(self, timestamp):
        self.timestamp = timestamp

    def date_from_timestamp(self, timestamp):
        """
        Takes timestamp as input and returns a date format : yy-mm-dd
        """
        date_and_time = str(datetime.fromtimestamp(timestamp)).split(" ")
        date = date_and_time[0]
        return date

    def time_from_timestamp(self, timestamp):
        """
        Takes timestamp as input and returns time in the format hh:mm
        """
        date_and_time = str(datetime.fromtimestamp(timestamp)).split(" ")
        time = date_and_time[1][0:5]
        return time
