from reportlab.lib import colors

from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm
from reportlab.platypus import Flowable, Paragraph, Spacer

class CHIBeforePageBreak(Flowable):
    def __init__(self, headVal, doc):
        Flowable.__init__(self)
        self.head = headVal 
        self.doc = doc

    def coord(self, x, y, unit=1):
        x, y = x * unit, self.height -  y * unit
        return x, y
        
    def draw(self):
        self.doc.bpb = self.head
        pass
