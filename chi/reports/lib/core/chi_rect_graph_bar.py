from reportlab.lib import colors

from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import mm
from reportlab.platypus import Flowable, Paragraph, Spacer

class CHIRectGraphBar(Flowable):
    def __init__(self, x=0, y=0, width=100, height=7, text="", percentage=0):
        Flowable.__init__(self)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.percentage = percentage
        self.text = str(percentage) +" %"
        # print(percentage)
        self.styles = getSampleStyleSheet()

    def coord(self, x, y, unit=1):
        """
        http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
        Helper class to help position flowables in Canvas objects
        """
        x, y = x * unit, self.height -  y * unit
        return x, y
        
    def draw(self):
        """
        Draw the shape, text, etc
        """
        ##Rectangle
        # self.canv.setFillColor(colors.lavender)
        self.canv.setFillColor('#BBBBBB')
        self.canv.rect(self.x, self.y, self.width, self.height, fill=1, stroke=0)
        self.canv.setFillColor(colors.black)
        self.canv.rect(self.x, self.y, self.width*self.percentage/100.0, self.height, fill=1, stroke=0)

        p = Paragraph(self.text, style=self.styles["Normal"])
        p.wrapOn(self.canv, self.width, self.height)
        p.drawOn(self.canv, *self.coord(38, 3, mm))
