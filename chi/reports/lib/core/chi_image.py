# import os
# from .styles.message_styles import ProgressReportStyles
# from reportlab.lib.styles import getSampleStyleSheet
# from datetime import datetime
from chi.reports.lib.core import constants
from reportlab.platypus import Image
from svglib.svglib import svg2rlg
import urllib
import os
# from reportlab.platypus import Image, Paragraph, Table, FrameBreak
# from reportlab.lib import colors
# ==========================publishing=====================

def URLToImage(image_path, localpath='/assets/images/progress-report/'):
    os.makedirs(localpath, exist_ok=True)
    image_name = os.path.basename(image_path)
    image_extension = image_name.split('.')[1]
    savePath = os.path.join(localpath, image_name)
    if not os.path.exists(savePath):
        urllib.request.urlretrieve(image_path, savePath)
    return savePath, image_extension


def GetSVGDrawing(image_path, xsize=50, ysize=50, fitType="fit"):
    drawing = svg2rlg(image_path)
    if drawing is None:
        image_path = '/assets/images/progress-report/bsr.svg'
        drawing = svg2rlg(image_path)
    xL, yL, xH, yH = drawing.getBounds()
    # print(xL, yL, xH, yH)
    sx = xsize / (xH - xL)
    sy = ysize / (yH - yL)

    drawing.height = ysize
    drawing.width = xsize

    s = sx
    if (sy < sx): s = sy

    drawing.width = (xH - xL) * s
    drawing.height = (yH - yL) * s

    drawing.scale(s, s)
    xL, yL, xH, yH = drawing.getBounds()
    # print(xL, yL, xH, yH)
    return drawing

def SVGtoImage(image_path, xsize=50, ysize=50, fitType="fit"):
    # print("Reading the image ....", image_path)
    image_extension = 'svg'
    if urllib.parse.urlparse(image_path).scheme in ['https', 'http']:
        image_path, image_extension = URLToImage(image_path, constants.DOWNLOAD_PATH) #Usman

    if image_extension == 'svg':
        drawing = GetSVGDrawing(image_path, xsize, ysize, fitType)
    elif image_extension in constants.SUPPORTED_IMAGES:
        drawing = Image(image_path, xsize, ysize)
    return drawing
