

def RaiseDictTypeException(data, key, type):
    if data is None:
        raise Exception("Data is None")

    if key not in data:
        raise Exception("{} Doesn't Exist in Data".format(key))

    # if data[key] is None:
    #     raise Exception("Data[{}] have None Value")

    if not isinstance(data[key], type):
        raise Exception("Expected DataType {} but Received {}".format(type, type(data[key])))

