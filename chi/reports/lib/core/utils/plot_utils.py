import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter
from datetime import datetime, timedelta
import copy
import pandas as pd
import os

defualt_config = {
    "savePath": 'plot.svg',
    "date_format": "%d/%m",
    'grid': True,
    "title": "",
    'plot_style': 'seaborn-dark',
    'xlabel': 'Time',
    'ylabel': 'Value',
    'min_max_conf': {'color': 'b', 'size': 0.02},
    "keyargs": {
        "figure": {'figsize': (16, 8)} ,
        "title" : {"size": 25},
        "plot": {'marker':'o', 'markersize': 20, 'alpha': 0.8, 'linewidth': 4.0 , 'markerfacecolor': 'none', 'markeredgewidth': 4.5},
        "legend": {'loc': "lower center", 'fontsize': 25, 'bbox_to_anchor': (0.5, -0.50), 'fancybox': True, 'shadow': True},
        "vlines": {'linestyles': 'dashed', 'linewidth': 6.0, 'alpha': 0.7},
        "xticks": {'fontsize': 25, 'rotation': -45, 'fontweight': 45},
        "yticks": {'fontsize': 25, 'rotation': 45, 'fontweight': 45},
        "xlabel": {'fontsize': 35, 'rotation': 0, 'fontweight': 45},
        "ylabel": {'fontsize': 35, 'rotation': 90, 'fontweight': 45},
        "trendline": {'linestyle': 'dotted', 'linewidth': 5},
        "savefig": {'bbox_inches': 'tight'}
    },
    "show": False
}
# "savefig": {'bbox_inches': 'tight'}
def getDefaultConfig():
    config = copy.deepcopy(defualt_config)
    return config

def prepareDataForVitalsGraph(dataY):
    dataX = copy.deepcopy(dataY)

    keys = dataX['data']['keys']
    dataP = {'checkpoints': dataX['data'].get('checkpoints', []), 'data': {}}
    data = dataX['data']['data']

    for i in range(len(dataP['checkpoints'])):
        dataP['checkpoints'][i]['date'] = datetime.fromtimestamp(dataP['checkpoints'][i]['date'])

    for key in keys:
        if key not in dataP.keys():
            dataP['data'][key] = {}
            dataP['data'][key]['time'] = []
            dataP['data'][key]['value'] = []

        for dt in data:
            if key in dt.keys():
                dataP['data'][key]['time'].append(datetime.fromtimestamp(dt['date']))
                dataP['data'][key]['value'].append(dt[key])
    return dataP

def plotData(data, config=None):
    if config is None:
        config = defualt_config
    figure_args = config['keyargs'].get('figure', {})
    title_args = config['keyargs'].get('title', {})
    plot_args = config['keyargs'].get('plot', {})
    xticks_args = config['keyargs'].get('xticks', {})
    yticks_args = config['keyargs'].get('yticks', {})
    xlabel_args = config['keyargs'].get('xlabel', {})
    ylabel_args = config['keyargs'].get('ylabel', {})
    saveFig_args = config['keyargs'].get('savefig', {})
    vlines_args = config['keyargs'].get('vlines', {})
    legend_args = config['keyargs'].get('legend', {})

    vlines_args.pop('colors', None)
    plot_args.pop('label', None)

    grid = config.get('grid', False)
    title = config.get('title', '')
    xlabel = config.get('xlabel', '')
    ylabel = config.get('ylabel', '')
    vline_colors = { "Escalation": 'r', "Prescription": 'g', 'other': 'b' }

    # plt.style.use(config.get('plot_style', 'seaborn-ticks'))
    fig, ax = plt.subplots(**figure_args)
    ax.set_title(title, **title_args)
    plt.xlabel(xlabel, **xlabel_args)
    plt.ylabel(ylabel, **ylabel_args)
    checkpoints = data['checkpoints']
    data_vals = data['data']
    for key in data_vals.keys():

        x ,y = data_vals[key]['time'], data_vals[key]['value']
        ax.plot(x, y, label=key, **plot_args)

    legend_args['ncol'] = len(data_vals.keys())
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,
                     box.width, box.height * 0.9])
    ax.legend(**legend_args)
    if grid:
        plt.grid()

    plt.xticks(**xticks_args)
    plt.yticks(**yticks_args)

    ymin, ymax = ax.get_ylim()
    if 'ymin' not in vlines_args.keys():
        vlines_args['ymin'] = ymin

    if 'ymax' not in vlines_args.keys():
        vlines_args['ymax'] = ymax

    for checkp in checkpoints:
        ax.vlines(x=checkp['date'], colors=vline_colors[checkp.get('type', 'other')], **vlines_args)


    date_form = DateFormatter(config.get('date_format', "%d/%m"))
    ax.xaxis.set_major_formatter(date_form)

    # print(config)
    os.makedirs(os.path.split(config['savePath'])[0], exist_ok=True)
    plt.savefig(config.get('savePath', 'plot.svg'), **saveFig_args)
    if config.get('show', True):
        plt.show()
    plt.close(fig=fig)



def prepareDataForComplianceGraph(dataY):
    dataX = copy.deepcopy(dataY)

    key = 'compliance'
    dataP = {'checkpoints': [], 'data': { key: {} }}
    dataP['data'][key]['time'] = []
    dataP['data'][key]['value'] = []

    for dt in dataX:
        dataP['data'][key]['time'].append(datetime.fromtimestamp(dt['date']))
        dataP['data'][key]['value'].append(dt['value'])


    return dataP


def prepareDataForVitalsGraphNew(dataY):
    dataX = copy.deepcopy(dataY)
    max_time = (datetime.max - timedelta(days=1)).timestamp()

    for data in dataX:
        checkpoints = {}
        for chkPoint in data['checkpoints']:
            t = chkPoint['date']
            if t not in checkpoints.keys():
                checkpoints[t] = []
            chkPoint['date'] = datetime.fromtimestamp(t)
            checkpoints[t].append(chkPoint)
        data['checkpoints'] = checkpoints
        data['summary']['min_time'] = max_time
        data['summary']['max_time'] = -max_time
        keys = data['keys']

        for key in keys:
            for reading in data['data'][key]:
                data['summary']['min_time'] = min(data['summary']['min_time'], reading['date'])
                data['summary']['max_time'] = max(data['summary']['max_time'], reading['date'])
                reading['date'] = datetime.fromtimestamp(reading['date'])

            tl_data =  data['summary'].get('trendLine', None)
            if tl_data is None:
                continue

            for trendLine in tl_data[key]:
                trendLine['date'] = datetime.fromtimestamp(trendLine['date'])

    return dataX

def plotMinMaxNumber(ax, data, stats, conf):
    if data['min'] == data['max']:
        return
    diff = (stats['max_time'] - stats['min_time']) * conf['size']

    xMin = []
    xMin.append(datetime.fromtimestamp(data['date'].timestamp() - diff))
    xMin.append(datetime.fromtimestamp(data['date'].timestamp() + diff))
    # xMin = [data['date'] - timedelta(hours=1), data['date'] + timedelta(hours=1)]
    y1 = [data['min'], data['min']]
    y2 = [data['max'], data['max']]
    xV = [data['date'], data['date']]
    yV = [data['min'], data['max']]
    ax.plot(xMin, y1, color=conf['color'])
    ax.plot(xMin, y2, color=conf['color'])
    ax.plot(xV, yV, color=conf['color'])

def plotTrendLine(ax, data, color, keyargs, label=''):
    if len(data) != 2:
        print("Warning: Trend Line Data is Either Empty or Not Valid", data)
        return
    x = [data[0]['date'], data[1]['date']]
    y = [data[0]['predicted'], data[1]['predicted']]
    keyargs['label'] = label
    keyargs['color'] = color
    ax.plot(x, y, **keyargs)


def plotCheckPoints(ax, checkpoints, legend_cols, keyargs):
    vline_colors = { "Escalation": 'r', "Prescription": 'g', 'other': 'b' }
    ymin, ymax = ax.get_ylim()
    if 'ymin' not in keyargs.keys():
        keyargs['ymin'] = ymin

    if 'ymax' not in keyargs.keys():
        keyargs['ymax'] = ymax
    diff = ymin - 0
    for timeS in checkpoints:
        total = len(checkpoints[timeS])
        for i, checkp in enumerate(checkpoints[timeS], 1):
            type = checkp.get('type', 'other')
            if type in legend_cols:
                type = ''
            keyargs['ymax'] = ((i / total) * ymax) + diff
            ax.vlines(x=checkp['date'], label=type, colors=vline_colors[checkp.get('type', 'other')], **keyargs)
            keyargs['ymin'] = keyargs['ymax']
            legend_cols.append(checkp.get('type', 'other'))

        keyargs['ymin'] = ymin
        keyargs['ymax'] = ymax

def plotDataNew(data, config=None):
    if config is None:
        config = defualt_config
    figure_args = config['keyargs'].get('figure', {})
    title_args = config['keyargs'].get('title', {})
    plot_args = config['keyargs'].get('plot', {})
    xticks_args = config['keyargs'].get('xticks', {})
    yticks_args = config['keyargs'].get('yticks', {})
    xlabel_args = config['keyargs'].get('xlabel', {})
    ylabel_args = config['keyargs'].get('ylabel', {})
    saveFig_args = config['keyargs'].get('savefig', {})
    vlines_args = config['keyargs'].get('vlines', {})
    legend_args = config['keyargs'].get('legend', {})
    trendline_args = config['keyargs'].get('trendline', {})

    vlines_args.pop('colors', None)
    plot_args.pop('label', None)

    grid = config.get('grid', False)
    title = config.get('title', '')
    xlabel = config.get('xlabel', '')
    ylabel = config.get('ylabel', '')
    minMaxConf = config.get('min_max_conf', {'color': 'g', 'size': 0.01})

    # plt.style.use(config.get('plot_style', 'seaborn-ticks'))
    fig, ax = plt.subplots(**figure_args)
    ax.set_title(title, **title_args)
    plt.xlabel(xlabel, **xlabel_args)
    plt.ylabel(ylabel, **ylabel_args)
    checkpoints = data['checkpoints']
    data_vals = data['data']
    legend_cols = []

    for key in data_vals.keys():
        legend_cols.append(key)
        df = pd.DataFrame(data_vals[key])
        # x ,y = data_vals[key]['time'], data_vals[key]['value']
        if len(df['date']) == 1:
            ax.set_xlim([df['date'][0] - timedelta(days=1), df['date'][0] + timedelta(days=1)])
        p = ax.plot(df['date'], df['value'], label=key, **plot_args)
        for dt in data_vals[key]:
            plotMinMaxNumber(ax, dt, data['summary'], minMaxConf)

        trendData = data['summary'].get('trendLine', None)
        if trendData is None:
            continue
        legend_cols.append(f'{key} TL')
        # plotTrendLine(ax, trendData.get(key, []), keyargs=trendline_args, color=p[-1].get_color(), label=f'{key} TL')
        plotTrendLine(ax, trendData.get(key, []), keyargs=trendline_args, color=p[-1].get_color(), label='')

    if grid:
        plt.grid()

    plt.xticks(**xticks_args)
    plt.yticks(**yticks_args)

    plotCheckPoints(ax, checkpoints=checkpoints, legend_cols=legend_cols, keyargs=vlines_args)

    legend_args['ncol'] = len(set(legend_cols))
    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.1,
                     box.width, box.height * 0.9])

    ax.legend(**legend_args)
    date_form = DateFormatter(config.get('date_format', "%d/%m"))
    ax.xaxis.set_major_formatter(date_form)

    # print(config)
    plt.savefig(config.get('savePath', 'plot.svg'), **saveFig_args)
    if config.get('show', True):
        plt.show()
    plt.close(fig=fig)

