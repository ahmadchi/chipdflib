import plot_utils as utils

config = utils.getDefaultConfig()
config['show'] = True

import json
with open('data.json') as f:
    reqData = json.load(f)

# data = utils.prepareDataForVitalsGraph(reqData['vitalGraphData'])
# utils.plotData(data, config)

plot_data = utils.prepareDataForVitalsGraphNew(reqData['vitalGraphDataNew'])
for data in plot_data:
    utils.plotDataNew(data, config)
    break