import matplotlib.pyplot as plt
from datetime import datetime
from matplotlib.dates import DateFormatter
import json
from datetime import timedelta
import plot_utils as utils

def plotMinMaxNumber(ax, data, color='g'):
    if data['min'] == data['max']:
        return

    xMin = [data['date'] - timedelta(hours=1), data['date'] + timedelta(hours=1)]
    y1 = [data['min'], data['min']]
    y2 = [data['max'], data['max']]
    xV = [data['date'], data['date']]
    yV = [data['min'], data['max']]
    ax.plot(xMin, y1, color=color)
    ax.plot(xMin, y2, color=color)
    ax.plot(xV, yV, color=color)

with open('data.json') as f:
    data = json.load(f)
data = utils.prepareDataForVitalsGraphNew(data['vitalGraphDataNew'])
fig, ax = plt.subplots()
date_form = DateFormatter("%d/%m")
ax.xaxis.set_major_formatter(date_form)
for dt in data[0]['data']['BSR']:
    plotMinMaxNumber(ax, dt)

plt.show()