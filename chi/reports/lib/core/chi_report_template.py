from reportlab.lib import colors
from reportlab.platypus import PageBreak, Frame, PageTemplate, Paragraph, NextPageTemplate, tableofcontents
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.platypus.doctemplate import BaseDocTemplate
from reportlab.lib.pagesizes import letter, A4
from .chi_paint import CHIPaint
from io import BytesIO
import os


class CHIReportTemplate(BaseDocTemplate):

    def __init__(self, headerHeight=65, pagesize=A4, tableOfContents = False, titlePage = False, 
                             leftMargin=20,
                             rightMargin=20,
                             topMargin=20,
                             bottomMargin=20):
        self.buffer = BytesIO()
        BaseDocTemplate.__init__(self, self.buffer,
                                 pagesize=pagesize,
                                 # pagesize = (1700,900),
                                 leftMargin=leftMargin,
                                 rightMargin=rightMargin,
                                 topMargin=topMargin,
                                 bottomMargin=bottomMargin)
        # print(pagesize)
        # exit()
        
        self.story = []
        self.bpb = {"full_name": ""}
        self.titlePage = titlePage

        self.toc = tableOfContents
        if self.toc:
            self.addTableOfContents()
        self.pageCallback = None
        self.paint = CHIPaint(self)
        self.dir_path = os.path.dirname(os.path.realpath(__file__)) + "/"

    def makeTocHeaderStyle(self, level, delta, epsilon, fontName='Times-Roman'):
        assert level >= 0, "Level must be >= 0."
        size = 12
        style = ParagraphStyle(name='Heading' + str(level),
                               fontName=fontName,
                               fontSize=size,
                               leading=size * 1.2,
                               # spaceBefore=size / 4.0,
                               # spaceAfter=size / 8.0,
                               firstLineIndent=-epsilon,
                               leftIndent=level * delta + epsilon)

        return style

    def addTableOfContents(self):
        self.toc = True
        maxLevels = 12
        tocLevelStyles = []
        d, e = tableofcontents.delta, tableofcontents.epsilon
        for i in range(maxLevels):
            tocLevelStyles.append(self.makeTocHeaderStyle(i, d, e))

        # tocLevelStyles.append(styles.chapterTitle)
        # tocLevelStyles.append(styles.sectionLevel1)
        # tocLevelStyles.append(styles.sectionLevel2)
        # tocLevelStyles.append(styles.sectionLevel3)
        # tocLevelStyles.append(styles.sectionLevel4)

        toc = tableofcontents.TableOfContents()
        toc.levelStyles = tocLevelStyles
        # self.story.append()
        self.story.append(toc)
        # self.story.append(PageBreak())


    def appendPara(self, text, style, keepWithNext=False):
        p = Paragraph(text, style)
        if (keepWithNext == True): p.keepWithNext = True
        self.story.append(p)

    def handle_afterPage(self):
        print("...... after page")

    def handle_nextPageTemplate(self, PageTemplateName):
        # pass
        BaseDocTemplate.handle_nextPageTemplate(self, PageTemplateName)
        pass
        # print("template here I am = ", self.pageNumber, PageTemplateName)

    def handle_pageBegin(self):
        BaseDocTemplate.handle_pageBegin(self)
        # self.canv.showPage()
        # self.pageNumber += 1
        if (self.pageCallback):
            self.pageCallback(self.canv, self)

    def handle_frameBegin(self, **kwargs):
        # pass
        BaseDocTemplate.handle_frameBegin(self, **kwargs)

        if hasattr(self.frame, 'background'):
            self.frame.drawBackground(self.canv)

    def breakPage(self):
        self.story.append(PageBreak())

    def changePageStyle(self, pageStyleName):
        # print("Changing Page style to: ", pageStyleName)
        self.story.append(NextPageTemplate([pageStyleName]))

    def publish(self):
        if self.toc == False:
            self.build(self.story)
        else:
            self.multiBuild(self.story)

    def getPDF(self):
        pdf = self.buffer.getvalue()
        return pdf

    def close(self):
        self.buffer.close()

    def afterFlowable(self, flowable):
        if self.toc == False:
            return

        if flowable.__class__.__name__ == 'Paragraph':
            styleName = flowable.style.name
            if styleName == 'level 1':
                # print("StyleName:  ", styleName)
                key = str(hash(flowable))
                self.canv.bookmarkPage(key)

                # Register TOC entries.
                level = 0  # int(styleName[7:])
                text = flowable.getPlainText()
                pageNum = self.page
                # Try calling this with and without a key to test both
                # Entries of every second level will have links, others won't
                #print('TOCEntry', (level, text, pageNum, key))
                # if level % 2 == 1:
                self.notify('TOCEntry', (level, text, pageNum, key))
                # else:
                #     self.notify('TOCEntry', (level, text, pageNum))
