from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm, cm
from reportlab.platypus import (Flowable, Paragraph,
                                SimpleDocTemplate, Spacer)
from reportlab.lib.colors import yellow, green, red, black, white, Color

class CHICircle(Flowable):
    def __init__(self, x=0, y=0,width=1,height = 1 ,radius=5, text="", color='#FF0000'):
        Flowable.__init__(self)
        self.x = x
        self.y = y
        self.radius = radius
        self.height = height
        self.width = width 
        self.text = text
        self.color = color
        self.styles = getSampleStyleSheet()

    def coord(self, x, y, unit=2):
        """
        http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
        Helper class to help position flowables in Canvas objects
        """
        x, y = x * unit, self.height -  y * unit
        return x, y
        
    def draw(self):
        """
        Draw the shape, text, etc
        """
        ##Rectangle
        # self.canv.setFillColor(colors.lavender)

        # self.canv.setFillColor('#BBBBBB')
        # self.canv.rect(self.x, self.y, self.width, self.height, fill=1, stroke=0)
        # self.canv.setFillColor(colors.black)
        # self.canv.rect(self.x, self.y, self.width*self.percentage, self.height, fill=1, stroke=0)


        canvas = self.canv
        canvas.setFillColor(self.color)
        #canvas.circle(self.x+self.radius/2, self.y-self.radius/2, self.radius, fill=1, stroke = 0)
        canvas.circle(0, 0, self.radius, fill=1, stroke = 0)

        p = Paragraph(self.text, style=self.styles["Normal"])
        # p.hAlign = "CENTER"
        # p.vAlign = "MIDDLE"
        #p.wrapOn(self.canv, self.width, self.height)
        
        p.wrapOn(self.canv, 500, 500)
        
        p.drawOn(self.canv,  self.radius/4,-self.radius)

        # p.drawOn(self.canv, *self.coord(self.x-0.8-0.8, 2.5+2.5, mm))
        #p.drawOn(self.canv,  self.radius/4,-self.radius)
        # p.drawOn(self.canv, *self.coord(38, 3, mm))

# doc = SimpleDocTemplate("circle.pdf",pagesize=letter)
# story=[]
# styles = getSampleStyleSheet()
# # px = Paragraph("this is noew text s" * 10, styles["Normal"])
# # story.append(px)
# box = CHICircle(0, 0, radius = 5, text="foobar")
# story.append(box)
# # story.append(Spacer(0, 1*inch))
# # box = BoxyLine(text="bar")
# # story.append(box)
# doc.build(story)
