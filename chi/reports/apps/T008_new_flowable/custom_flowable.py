from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm, cm
from reportlab.platypus import (Flowable, Paragraph,
                                SimpleDocTemplate, Spacer)
from reportlab.lib.colors import yellow, green, red, black, white, Color

class BoxyLine(Flowable):
    def __init__(self, x=0, y=0, width=100, height=20, text=""):
        Flowable.__init__(self)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.styles = getSampleStyleSheet()
        self.aw = 0
        self.ah = 0
        self.hAlign = 'CENTER'
        
    def coord(self, x, y, unit=1):
        """
        http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
        Helper class to help position flowables in Canvas objects
        """
        x, y = x * unit, self.height -  y * unit
        return x, y

    # def wrap(self,availWidth,availHeight):
    #     print("w,h ", availWidth, availHeight)

    #     self.aw = availWidth
    #     self.ah = availHeight
    #     self.width = availWidth *.8
    #     self.height = availHeight *.8
    #     return self.width, self.height

    #----------------------------------------------------------------------
    def draw(self):
        canvas = self.canv
        print("drawing")
        
        canvas.circle(0, 0, 5, stroke=1, fill=1)
        self.canv.rect(self.x, self.y, self.width, self.height)
        # # self.canv.line(self.x, 0, 500, 0)

        # u = inch/10.0
        px = canvas.beginPath()p = c.beginPath()
        p.circle(50, 50, 50)
        canvas.setLineWidth(20)
        c.clipPath(p)
        

        print("aw,ah ", self.aw, self.aw)
        m = self.height
        if(m > self.width) : m = self.width
        r = 0.4 *m
        px.arc(self.width/2-r, self.height/2-r, self.width/2+r, self.height/2+r, startAng=180, extent=359.5)
        canvas.setLineWidth(20)
        self.canv.setStrokeColorRGB(1, 0, 0)
        canvas.drawPath(px, fill=0, stroke=1)

        self.canv.setStrokeColorRGB(1, 1, 0)
        red50transparent = Color( 1, .5, 0, alpha=0.8)
        self.canv.setStrokeColor(red50transparent)
        c = canvas
        c.saveState()
        p = c.beginPath()
        p.circle(50, 50, 50)
        canvas.setLineWidth(20)
        c.clipPath(p)
        c.drawImage("drdima.png", 0,0,100,100, mask='auto')
        c.restoreState()


        p = c.beginPath()
        p.circle(250, 250, 50)
        canvas.setLineWidth(20)
        c.clipPath(p)
        c.drawImage("drhamed.jpg", 200,200,100,100, mask='auto')
        # canvas.setAlpha(1)
        # c.addLiteral('%Begin clip path')
        

        # p = Paragraph(self.text, style=self.styles["Normal"])
        # p.wrapOn(self.canv, self.width, self.height)
        # p.drawOn(self.canv, *self.coord(self.x+2, 10, mm))



doc = SimpleDocTemplate("test3.pdf",pagesize=letter)
story=[]
styles = getSampleStyleSheet()
px = Paragraph("this is noew text s" * 10, styles["Normal"])
story.append(px)
box = BoxyLine(0, 0, 500, 500, text="foobar")
story.append(box)
story.append(Spacer(0, 1*inch))
# box = BoxyLine(text="bar")
# story.append(box)
doc.build(story)