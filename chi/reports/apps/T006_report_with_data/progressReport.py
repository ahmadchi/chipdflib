
from datetime import datetime

# ==========================publishing=====================
# dict_keys(['full_name', 'patient_id', 'user_id', 'hospital_no', 'title_id', 'image', 'first_name', 'last_name',
#  'father_name', 'gender', 'birth_date', 'system_language_id', 'height', 'weight', 'episode_status', 'date_added',
#   'date_updated', 'is_deleted', 'email', 'username', 'password', 'group_id', 'group_type', 'site_id', 
#   'rough_age', 'age', 'episode_id', 'onboard_date', 'episode_active', 'avg_score'])


def patientHeader(canvas, doc):
	paint = doc.paint
	rightEdge = doc.pagesize[0]-doc.rightMargin
	topEdge = doc.pagesize[1]-doc.topMargin
	leftEdge = doc.leftMargin

	pData = doc.data["patient_info"]
	# print(pData.keys())
	# exit()

	paint.setFillColor("#555555")

	# paint.drawBoundary()
	paint.setAlign("LEFT")
	fontSize = 50
	paint.setAnchor(leftEdge, topEdge-fontSize)
	paint.setFontSize(fontSize)
	paint.paintImage("../../images/logo1x.png")

	paint.setAnchor(leftEdge + paint.fontSize*1.2, topEdge)
	paint.setFontSize(18)
	paint.setFontName("Helvetica-Bold")
	paint.paintText(["ESCALATION", "REPORT", "SUMMARY"])

	paint.setAlign("RIGHT")
	paint.setAnchor(rightEdge, topEdge)  
	paint.setFontSize(12)

	full_name = pData["full_name"]
	hospital_no = pData["hospital_no"]
	gender = pData["gender"]
	birth_date = pData["birth_date"]
	age = pData["age"]
	paint.paintText([full_name, age + " Years| " + gender,  "Hospital : " + hospital_no])

		

	paint.setFontName("Helvetica")

	paint.setAlign("CENTER")
	# paint.paintText("")
	paint.setAnchor(doc.pagesize[0]/2, topEdge)
	paint.setFontSize(10)
	paint.paintText(["Escalated by Command Center","Report date : Oct 31, 2020"])

	paint.curY = topEdge - 65   
	paint.separator()


	# Footer
	paint.setAlign("RIGHT")
	paint.setAnchor(rightEdge, doc.bottomMargin+15)
	pageNumber = "Page %s of %s" % (doc.pageNumber, doc.pageNumber)
	paint.paintText(pageNumber)

	paint.setAlign("LEFT")
	paint.setAnchor(leftEdge, doc.bottomMargin+15)
	footnote = f"Sara Qarab, Added by Admin CHI on {datetime.date(datetime.now())}"
	paint.paintText(footnote)

	paint.curY = doc.bottomMargin+20    
	paint.separator()



def titlePageAnnual(canvas, doc):
	paint = doc.paint
	rightEdge = doc.pagesize[0]-doc.rightMargin
	topEdge = doc.pagesize[1]-doc.topMargin
	leftEdge = doc.leftMargin

	pw = doc.pagesize[0]
	ph = doc.pagesize[1]
	sc = .8
	y = ph/2 - doc.width*sc/2# - doc.bottomMargin

	paint.paintImageRect("building3.jpg", 0, y, doc.pagesize[0], doc.width*sc)
	paint.drawRect(leftEdge, topEdge, doc.width, doc.height, line_width=5)
	paint.drawRect(pw/3+20, ph, pw/2+20, ph, color="#ffd000",transparent = 0.8, line_width = 0)
	paint.drawRect(pw/3+40, ph-285, 10, 300, color="black", line_width= 1)

	paint.setAlign("LEFT")
	paint.setAnchor(pw/2.5+20, ph-350)  
	paint.setFontSize(60)

	paint.setFontName("Helvetica-Bold")
	paint.paintText(["ANNUAL", "REPORT"])
	paint.setFontSize(110)
	paint.setFillColor("white")
	paint.paintText("2020")

	paint.setFontSize(20)
	paint.paintText([""])
	paint.setFillColor("black")
	paint.setFontName("Helvetica")
	paint.paintText(["Position for", "Global Growth"])
	canvas.showPage()

