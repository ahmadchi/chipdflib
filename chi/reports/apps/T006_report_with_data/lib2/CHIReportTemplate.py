from reportlab.rl_config import defaultPageSize
from reportlab.platypus import PageBreak, Frame, PageTemplate, Paragraph, NextPageTemplate
from reportlab.lib.units import inch
from reportlab.platypus.doctemplate import BaseDocTemplate
from reportlab.lib.pagesizes import letter, A4
from lib2.CHIPaint import CHIPaint

class CHIReportTemplate(BaseDocTemplate):

    def __init__(self, cwd, headerHeight=65, pagesize = A4):
        BaseDocTemplate.__init__(self, cwd, 
            pagesize = pagesize, 
            # pagesize = (1700,900), 
            leftMargin = 20, 
            rightMargin = 20,
            topMargin = 20,
            bottomMargin=20)

        self.story = []
        self.pageNumber = 0


        self.pageCallback = None

        self.paint = CHIPaint(self)  
        
    def appendPara(self, text, style):
        self.story.append(Paragraph(text, style))

    def handle_afterPage(self):
        print("...... after page")

    def handle_nextPageTemplate(self, PageTemplateName):
        # pass
        BaseDocTemplate.handle_nextPageTemplate(self, PageTemplateName)
        print("template here I am = ", self.pageNumber, PageTemplateName)

    def handle_pageBegin(self):
        BaseDocTemplate.handle_pageBegin(self)
        # self.canv.showPage()
        self.pageNumber += 1
        if(self.pageCallback):
            self.pageCallback(self.canv, self)


    def handle_frameBegin(self, **kwargs):
        # pass
        BaseDocTemplate.handle_frameBegin(self, **kwargs)
        
        if hasattr(self.frame, 'background'):
            self.frame.drawBackground(self.canv)

    def breakPage(self):
        self.story.append(PageBreak())

    def changePageStyle(self, pageStyleName):
        print("Changing Page style to: ", pageStyleName)
        self.story.append(NextPageTemplate([pageStyleName]))

    def publish(self):
        self.build(self.story)
