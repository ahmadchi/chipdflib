from reportlab.platypus import Frame #, PageTemplate, Image, Paragraph, Table, FrameBreak

class CHIFrame(Frame):
    
    def __init__(self, x1, y1, width,height, leftPadding=6, bottomPadding=6,
            rightPadding=6, topPadding=6, id=None, showBoundary=0,
            overlapAttachedSpace=None,_debug=None,background=None, backgroundColor="#333333"):
        
        Frame.__init__(self, x1, y1, width, height, leftPadding,
            bottomPadding, rightPadding, topPadding, id, showBoundary,
            overlapAttachedSpace, _debug)
        
        self.background = background
        self.backgroundColor = backgroundColor

    def drawBackground(self, canv):
       
        canv.saveState()
        canv.setFillColor(self.backgroundColor)
        canv.rect(
            self._x1, self._y1, self._x2 - self._x1, self._y2 - self._y1,
            stroke=0, fill=1
        )
        canv.restoreState()

    # def addFromList(self, drawlist, canv):
    #     print("-----ddd")
    #     if self.background:
    #         self.drawBackground(canv)
    #     Frame.addFromList(self, drawlist, canv)
