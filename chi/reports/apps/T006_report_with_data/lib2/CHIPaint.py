

class CHIPaint():
    def __init__(self, doc, anchorX=0, anchorY=0, 
            fontName= 'Helvetica', fontSize =  10,  align = "LEFT"):
        # self.canv = doc.canv
        self.doc = doc
        self.setAlign(align)
        self.setFont(fontName, fontSize)
        self.setAnchor(anchorX, anchorY)
        self.lineSpacing = .2

    def drawBoundary(self):
        doc = self.doc
        canvas = doc.canv
        canvas.rect(doc.leftMargin, doc.bottomMargin, doc.width, doc.height)

    def setPreviousStyle(self):
        self.setFillColor("black")
        self.doc.canv.setLineWidth(1)
        self.doc.canv.setFillAlpha(1)

    def setAlign(self, align):
        self.align = align

    def setAnchorX(self, anchorX):
        self.anchorX = anchorX
        self.curX = anchorX

    def setAnchorY(self, anchorY):
        self.anchorY= anchorY
        self.curY = anchorY

    def setFillColor(self, color):
        self.doc.canv.setFillColor(color)

    def setAnchor(self, anchorX, anchorY):
        self.anchorX = anchorX
        self.anchorY= anchorY
        self.curX = anchorX
        self.curY = anchorY

    def setFontName(self, fontName):
        self.fontName = fontName

    def setFontSize(self, fontSize):
        self.fontSize = fontSize

    def setFont(self, fontName, fontSize):
        self.setFontName(fontName)
        self.setFontSize(fontSize)
        
    def textOut(self, msg):
        pass

    def paintImageRect(self, fileName, x, y, w, h):
        canvas = self.doc.canv
        canvas.drawImage(fileName, x, y, w, h, mask='auto')

    def paintImage(self, fileName):
        msg = fileName
        canvas = self.doc.canv
        canvas.drawImage(fileName, self.curX, self.curY, self.fontSize, self.fontSize, mask='auto')

    def drawRect(self, x, y, w, h, color=None, line_width=1, transparent = 1):
        doc = self.doc
        p = doc.canv.beginPath()
        p.moveTo(x, y)
        p.lineTo(x+w, y)
        p.lineTo(x+w, y-h)
        p.lineTo(x, y-h)
        p.close()
        fill = 0
        doc.canv.setLineWidth(line_width)
        if (color is not None):
            fill = 1
            doc.paint.setFillColor(color)      
        doc.canv.setFillAlpha(transparent)
        if(line_width): 
            stroke = 1
        else: 
            stroke = 0
        doc.canv.drawPath(p, fill = fill, stroke=stroke)
        doc.paint.setPreviousStyle() 

    def paintTextStr(self, msg):
        self.curY -= (1-self.lineSpacing)*self.fontSize
        canvas = self.doc.canv
        canvas.saveState()
        canvas.setFont(self.fontName, self.fontSize)
        if self.align == "LEFT":
            canvas.drawString(self.curX, self.curY, msg)
        if self.align == "CENTER":
            canvas.drawCentredString(self.curX, self.curY,  msg)
        if self.align == "RIGHT":
            canvas.drawRightString(self.curX, self.curY, msg)
        self.curY -= 1.2 * self.fontSize
        canvas.restoreState()
        self.curY += (1-self.lineSpacing)*self.fontSize

    def paintText(self, msg):

        if isinstance(msg, str):
            self.paintTextStr(msg)
        else:
            for st in msg:
                self.paintTextStr(st)

    def separator(self):
        doc = self.doc
        canvas = doc.canv

        y = self.curY
        rightEdge = doc.pagesize[0]-doc.rightMargin
        topEdge = doc.height+doc.bottomMargin
        leftEdge = doc.leftMargin

        canvas.line(leftEdge, y, leftEdge+doc.width, y)
