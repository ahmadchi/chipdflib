from lib2.CHIFrame import CHIFrame
from lib2.CHIReportTemplate import CHIReportTemplate
from ProgressReportStyles import ProgressReportStyles
from reportlab.lib import colors
from reportlab.lib.colors import Color
from progressReport import patientHeader, titlePageAnnual
from data.processJson import *

from reportlab.platypus import Frame, PageTemplate, Image, Paragraph, Table, FrameBreak
from reportlab.platypus.flowables import ParagraphAndImage
from reportlab.lib.units import inch
from reportlab.lib.styles import getSampleStyleSheet
from svglib.svglib import svg2rlg


def addPageTemplates(doc):
        sb = 0
        footerHeight = 20 + 10

        doc.headerHeight = 65


        leftMargin = doc.leftMargin
        bottomMargin = doc.bottomMargin + 20 +10
        width2 = doc.width / 2 
        width3 = doc.width / 3 
        frameHeight = doc.height - doc.headerHeight - footerHeight -10
        # doc.showBoundary = 1
        incWidth = 50
        frame1 = Frame(leftMargin + 0 * width2, bottomMargin, width2+incWidth, frameHeight, id='col1', showBoundary=sb)
        frame2 = CHIFrame(leftMargin + 1 * width2+incWidth, bottomMargin-10, width2-incWidth, frameHeight+20, 
            id='col2', showBoundary=sb, backgroundColor = "#EEEEEE")
        # , backgroundColor ="#DDDDFF"

        frame3 = Frame(leftMargin + 0 * width3, bottomMargin, width3, frameHeight, id='col3', showBoundary=sb)
        frame4 = Frame(leftMargin + 1 * width3, bottomMargin, width3, frameHeight, id='col4', showBoundary=sb)
        frame5 = Frame(leftMargin + 2 * width3, bottomMargin, width3, frameHeight, id='col5', showBoundary=sb)
        # doc.addPageTemplates(template)
        twoColumnPage = PageTemplate(id='two_col', frames=[frame1, frame2], autoNextPageTemplate = None)
        threeColumnPage = PageTemplate(id='three_col', pagesize=(500,500), frames=[frame3, frame4, frame5])

        #print(dir(twoColumnPage))
        doc.addPageTemplates([twoColumnPage, threeColumnPage])

def addTable():
    styles = ProgressReportStyles()
    styleSheet = getSampleStyleSheet()
    I = Image('h1-sv-icon-3.png')
    I.drawHeight = 30
    I.drawWidth = 20
    P0 = Paragraph('''
    <b>A pa<font color=red>r</font>a<i>graph</i></b>
    <super><font color=yellow>1</font></super>''',
    styleSheet["BodyText"])
    P = Paragraph('''
    <para align=center spaceb=3>The <b>ReportLab Left
    <font color=red>Logo</font></b>
    Image</para>''',
    styleSheet["BodyText"])
    p1a = Paragraph('Sept 27, 2020  11:14AM', styles.p1a)
    p1b = Paragraph('Blood Glucose', styles.p1b)
    p1c = Paragraph('82.0 mg/ml', styles.p1c)
    data= [
    [   I, [p1a, p1b], p1c],
    # ["1", "2", "3"]
    ]

    t=Table(data, style=[
        # ('GRID',(0,0),(3,3),1,colors.green)
        ], 
    colWidths=[30, 100, 80]
    )
    # ,style=[('GRID',(1,1),(-2,-2),1,colors.green),
    # ('BOX',(0,0),(1,-1),2,colors.red),
    # ('LINEABOVE',(1,2),(-2,2),1,colors.blue),
    # ('LINEBEFORE',(2,1),(2,-2),1,colors.pink),
    # ('BACKGROUND', (0, 0), (0, 1), colors.pink),
    # ('BACKGROUND', (1, 1), (1, 2), colors.lavender),
    # ('BACKGROUND', (2, 2), (2, 3), colors.orange),
    # ('BOX',(0,0),(-1,-1),2,colors.black),
    # ('GRID',(0,0),(-1,-1),0.5,colors.black),
    # ('VALIGN',(3,0),(3,0),'BOTTOM'),
    # ('BACKGROUND',(3,0),(3,0),colors.limegreen),
    # ('BACKGROUND',(3,1),(3,1),colors.khaki),
    # ('ALIGN',(3,1),(3,1),'CENTER'),
    # ('BACKGROUND',(3,2),(3,2),colors.beige),
    # ('ALIGN',(3,2),(3,2),'LEFT'),
    # ])
    # t._argW[2]=1.5*inch
    return t


def SVGtoImage(image_path, xsize=50, ysize=50, fitType="fit"):
    drawing = svg2rlg(image_path)
    xL, yL, xH, yH = drawing.getBounds()
    print(xL, yL, xH, yH)
    sx = xsize / (xH - xL)
    sy = ysize / (yH - yL)
    
    drawing.height = ysize
    drawing.width = xsize

    s = sx
    if(sy<sx): s = sy

    drawing.width = (xH - xL) * s
    drawing.height = (yH - yL) * s
    
    drawing.scale(s, s)
    xL, yL, xH, yH = drawing.getBounds()
    print(xL, yL, xH, yH)
    return drawing

def PatientProgressReport(data):
    
    # print(data["active_medications"][0].keys())
    activeMedications = processActiveMediations(data["active_medications"])
    print(activeMedications)
    # print()
    # # for msg in data["messages"]:
    # #     processMessages(msg["subjects"])
    # exit()

    headerHeight = 65
    report = CHIReportTemplate('pdf/progressReport.pdf', headerHeight)
    report.data = data
    report.pageCallback = patientHeader
    addPageTemplates(report)
    styles = ProgressReportStyles()
    report.changePageStyle("three_col")



    image = SVGtoImage( '../../images/progress-report/pill.svg', 10,10, 'fit')
    # report.story.append(image)

    report.story.append(ParagraphAndImage(Paragraph("Current Medication", styles.activeMedsH1),image,side='left'))
    # for k in range(1,20):
    count = 1
    for med in activeMedications:
        report.appendPara(str(count) + ". " +med, styles.activeMeds)
        count += 1

    ww= 350;
    hh = 300

    
    report.story.append(Paragraph("", styles.activeMedsH1))
    # GRAPH
    image = SVGtoImage( '../../images/progress-report/bsr.svg', 15,15, 'fit')
    report.story.append(ParagraphAndImage(Paragraph("Blood Glucose Chart", styles.activeMedsH1),image,side='left'))
    image = SVGtoImage( 'data/seaborn-dark_plot.svg', ww, hh, 'fit')
    image.hAlign = 'CENTER'
    report.story.append(image)



    # report.story.append(Paragraph('<para autoLeading="off" fontSize=12> test "<img src="../../images/h1-sv-icon-2.png"</para>', styles.activeMeds))
    t = addTable()
    report.story.append(FrameBreak())
    report.story.append(t)

    for page in range(1,1):
        print("page ", page);
        if(page %2):
            report.changePageStyle("two_col")
        else:
            report.changePageStyle("three_col")
            
        for i in range(0, 15):
                report.appendPara("RBG WARNING HIGH", styles.message_subject)
                report.appendPara("", styles.message_to)
                # report.breakPage()    
                for k in range(30):        
                    if k%2:
                        report.appendPara("Dr. Abbas", styles.message_to)
                        report.appendPara("Patient is serious please do something soon. " *5, styles.message_body_to)
                    else:
                        report.appendPara("Patient Bilal", styles.message_from)
                        report.appendPara("I am sweathing what can you do. " * 4, styles.message_body_from)

            # report.breakPage()
    print("==========================publishing=====================");
    report.publish()


def customReport():
    report = CHIReportTemplate('pdf/progressReport.pdf')
    report.pageCallback = titlePageAnnual
    styles = ProgressReportStyles()
    report.changePageStyle("three_col")
    
    report.publish()

# customReport()

# progessReport()



# print(data["messages"])
# print(data["medicine_history"])
# print(data["patient_info"])
# print(data["doctor_info"])
# print(data["diagnoses"])

# for vital in data["recent_result"]:
#   print("==============")
#   print(vital["title"])
#   print("==============")
#   for k in vital:
# print("  > ", k, vital[k])

# print(data["allergies"])
# print(data["compliance_data"])
# print(data["active_medications"])


# processMessages(data["messages"])

# processVitals(data["recent_result"])



# report = ReportGenerator("progress.pdf")

# report.addHeader()

# report.addProgressTitlePage(patientData);

# report.addVitalData(messageData);


# report.addMessageData(messageData);

