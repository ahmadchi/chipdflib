# import sys
# sys.path.append("../../")

# from lib.composer import Composer
# from io import BytesIO

# def main():

#     buffer = BytesIO()

#     composer = Composer(buffer, 'Letter')
#     pdf = composer.print_users(["ahmad", "ali"])
#     f = open("composer.pdf", 'wb')
#     f.write(pdf)
#     print("done")

# main()


from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, ActionFlowable
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
from reportlab.lib.colors import blue, red

PAGE_HEIGHT = defaultPageSize[1];
PAGE_WIDTH = defaultPageSize[0]
styles = getSampleStyleSheet()

yourStyle = ParagraphStyle('yourtitle',
                           fontName="Helvetica-Bold",
                           fontSize=16,
                           backColor='#FF9000',
                           borderRadius=10,
                           borderColor='#80909050',
                           borderWidth=1,
                           borderPadding=5,
                           parent=styles['Heading2'],
                           alignment=1,
                           spaceAfter=14)

# First we import some constructors, some paragraph styles and other conveniences from other modules.
Title = "Hello2 world"
pageinfo = "platypus example"


def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.setFillColor(blue)
    canvas.setFont('Times-Bold', 36)
    canvas.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 108, Title)
    canvas.setFont('Times-Roman', 9)
    # image = Image("assets/images/logo2x.png", width=2*inch, height=2*inch)
    # canvas.drawInlineImage(image, 10,10, width=None,height=None)
    canvas.drawString(inch, 0.75 * inch, "First Page / %s" % pageinfo)
    canvas.restoreState()


def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman', 9)
    canvas.drawString(inch, 0.75 * inch, "Page %d %s" % (doc.page, pageinfo))
    canvas.restoreState()


class CHIReportTemplate(SimpleDocTemplate):

    def __init__(self, cwd):
        SimpleDocTemplate.__init__(self, cwd)
        self.pageNumber = 0

    def handle_afterPage(self):
        print("...... after page")

    def handle_pageBegin(self):
        SimpleDocTemplate.handle_pageBegin(self)
        # self.canv.showPage()
        self.canv.saveState()
        self.pageNumber += 1
        if self.pageNumber % 2 == 0:
            myFirstPage(self.canv, self)
        else:
            myLaterPages(self.canv, self)
        print("here I am = ", self.pageNumber)
        self.canv.restoreState()


def go():
    doc = CHIReportTemplate("phello.pdf")
    Story = [Spacer(1, 2 * inch)]
    style = styles["Normal"]

    for i in range(10):
        bogustext = ("This is Paragraph number %s. " % i) * 200
        im = Image("../../images/logo1x.png", width=2 * inch, height=2 * inch)
        im.hAlign = 'CENTER'
        p1 = Paragraph(bogustext, style)
        p2 = Paragraph("Here you go", yourStyle)

        Story.append(im)
        Story.append(p1)
        Story.append(p2)
        Story.append(Spacer(1, 0.2 * inch))

    doc.build(Story)  # , onFirstPage=myFirstPage, onLaterPages=myLaterPages)


go()
