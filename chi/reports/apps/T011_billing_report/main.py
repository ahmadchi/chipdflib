import os
import json

from chi.reports.lib.billing.billing import Billing


# TODO: remove following 2 functions to utils
def getAppPath():
    base_path = os.getenv('BASE_PATH', '')
    app_path = os.path.join(base_path, 'apps/T011_billing_report')
    return app_path


def loadJsonData(appPath, fName):
    jsonPath = appPath + "/" + fName
    jsonFile = open(jsonPath, "r")
    prescData = json.load(jsonFile)
    return prescData

'''
    Pre-reqs for BLUE_LETTERHEAD
        - doctor info should be present (full_name)
        - upto 3 lines of details, s
    
'''
def blueLetterhead():
    app_path = getAppPath()
    prescData = loadJsonData(app_path, "data/data1.json")
 
    report = Billing(prescData, "BLUE_LETTERHEAD")
    # report = Prescription(prescData, "RED_LETTERHEAD")
    pdfData = report.getPDF()
    report.close()

    file = open(f"{app_path}/pdf/billing.pdf", 'wb')
    file.write(pdfData)
    print("done")



def main():
    blueLetterhead()
