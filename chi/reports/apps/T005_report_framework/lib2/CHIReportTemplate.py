from reportlab.rl_config import defaultPageSize
from reportlab.platypus import PageBreak, Frame, PageTemplate, Paragraph, NextPageTemplate
from reportlab.lib.units import inch
from reportlab.platypus.doctemplate import BaseDocTemplate
from reportlab.lib.pagesizes import letter, A4
from lib2.CHIPaint import CHIPaint

class CHIReportTemplate(BaseDocTemplate):

    def __init__(self, cwd, pagesize = A4):
        BaseDocTemplate.__init__(self, cwd, 
            pagesize = (700,900), 
            # pagesize = (1700,900), 
            leftMargin = 20, 
            rightMargin = 20,
            topMargin = 20,
            bottomMargin=20)

        self.story = []
        self.pageNumber = 0
        
        leftMargin = self.leftMargin
        bottomMargin = self.bottomMargin
        width2 = self.width / 2 
        width3 = self.width / 3 
        height = self.height
        # self.showBoundary = 1
        frame1 = Frame(leftMargin + 0 * width2, bottomMargin, width2, height, id='col1')
        frame2 = Frame(leftMargin + 1 * width2, bottomMargin, width2, height, id='col2')

        frame3 = Frame(leftMargin + 0 * width3, bottomMargin, width3, height, id='col3')
        frame4 = Frame(leftMargin + 1 * width3, bottomMargin, width3, height, id='col4')
        frame5 = Frame(leftMargin + 2 * width3, bottomMargin, width3, height, id='col5')
        # self.addPageTemplates(template)
        twoColumnPage = PageTemplate(id='two_col', frames=[frame1, frame2], autoNextPageTemplate = None)
        threeColumnPage = PageTemplate(id='three_col', pagesize=(500,500), frames=[frame3, frame4, frame5])

        #print(dir(twoColumnPage))
        self.addPageTemplates([twoColumnPage, threeColumnPage])
        self.pageCallback = None

        self.paint = CHIPaint(self)  
        
    def appendPara(self, text, style):
        self.story.append(Paragraph(text, style))

    def handle_afterPage(self):
        print("...... after page")

    def handle_nextPageTemplate(self, PageTemplateName):
        # pass
        BaseDocTemplate.handle_nextPageTemplate(self, PageTemplateName)
        print("template here I am = ", self.pageNumber, PageTemplateName)

    def handle_pageBegin(self):
        BaseDocTemplate.handle_pageBegin(self)
        # self.canv.showPage()
        self.pageNumber += 1
        if(self.pageCallback):
            self.pageCallback(self.canv, self)


    def breakPage(self):
        self.story.append(PageBreak())

    def changePageStyle(self, pageStyleName):
        print("Changing Page style to: ", pageStyleName)
        self.story.append(NextPageTemplate([pageStyleName]))

    def publish(self):
        self.build(self.story)
