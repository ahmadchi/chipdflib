from datetime import datetime
from lib2.CHIReportTemplate import CHIReportTemplate
from ProgressReportStyles import ProgressReportStyles
from reportlab.lib import colors
from reportlab.lib.colors import Color


def patientHeader(canvas, doc):
    paint = doc.paint
    rightEdge = doc.pagesize[0]-doc.rightMargin
    topEdge = doc.pagesize[1]-doc.topMargin
    leftEdge = doc.leftMargin

    paint.setFillColor("#555555")

    # paint.drawBoundary()
    paint.setAlign("LEFT")
    fontSize = 50
    paint.setAnchor(leftEdge, topEdge-fontSize)
    paint.setFontSize(fontSize)
    paint.paintImage("../../images/logo1x.png")

    paint.setAnchor(leftEdge + paint.fontSize*1.2, topEdge)
    paint.setFontSize(18)
    paint.setFontName("Helvetica-Bold")
    paint.paintText(["ESCALATION", "REPORT", "SUMMARY"])

    paint.setAlign("RIGHT")
    paint.setAnchor(rightEdge, topEdge)  
    paint.setFontSize(12)
    paint.paintText(["Sara Qarab", "25 Years | Female", "Hospital : CHI-HN-0090", "Ph : 090078601"])

    paint.setFontName("Helvetica")

    paint.setAlign("CENTER")
    # paint.paintText("")
    paint.setAnchor(doc.pagesize[0]/2, topEdge)
    paint.setFontSize(10)
    paint.paintText(["Escalated by Command Center","Report date : Oct 31, 2020"])

    paint.curY = topEdge - 65   
    paint.separator()

    paint.setAlign("RIGHT")
    paint.setAnchor(rightEdge, doc.bottomMargin+15)
    pageNumber = "Page %s of %s" % (doc.pageNumber, doc.pageNumber)
    paint.paintText(pageNumber)

    paint.setAlign("LEFT")
    paint.setAnchor(leftEdge, doc.bottomMargin+15)
    footnote = f"Sara Qarab, Added by Admin CHI on {datetime.date(datetime.now())}"
    paint.paintText(footnote)

    paint.curY = doc.bottomMargin+20    
    paint.separator()


    # L3tme1n@CHI
def progessReport():
    report = CHIReportTemplate('pdf/progressReport.pdf')
    report.pageCallback = patientHeader
    styles = ProgressReportStyles()
    report.changePageStyle("three_col")
    for page in range(1,5):
        print("page ", page);
        # if(page %2):
        #     report.changePageStyle("two_col")
        # else:
        #     report.changePageStyle("three_col")
            
        for i in range(0, 15):
                # report.appendPara("RBG WARNING HIGH", styles.message_subject)
                report.appendPara("", styles.message_to)
                report.breakPage()    
                # for k in range(30):        
                #     if k%2:
                #         report.appendPara("Dr. Abbas", styles.message_to)
                #         report.appendPara("Patient is serious please do something soon. " *5, styles.message_body_to)
                #     else:
                #         report.appendPara("Patient Bilal", styles.message_from)
                #         report.appendPara("I am sweathing what can you do. " * 4, styles.message_body_from)

            # report.breakPage()
    print("==========================publishing=====================");
    report.publish()


def titlePageAnnual(canvas, doc):
    paint = doc.paint
    rightEdge = doc.pagesize[0]-doc.rightMargin
    topEdge = doc.pagesize[1]-doc.topMargin
    leftEdge = doc.leftMargin

    pw = doc.pagesize[0]
    ph = doc.pagesize[1]
    sc = .8
    y = ph/2 - doc.width*sc/2# - doc.bottomMargin

    paint.paintImageRect("building3.jpg", 0, y, doc.pagesize[0], doc.width*sc)
    paint.drawRect(leftEdge, topEdge, doc.width, doc.height, line_width=5)
    paint.drawRect(pw/3+20, ph, pw/2+20, ph, color="#ffd000",transparent = 0.8, line_width = 0)
    paint.drawRect(pw/3+40, ph-285, 10, 300, color="black", line_width= 1)

    paint.setAlign("LEFT")
    paint.setAnchor(pw/2.5+20, ph-350)  
    paint.setFontSize(60)

    paint.setFontName("Helvetica-Bold")
    paint.paintText(["ANNUAL", "REPORT"])
    paint.setFontSize(110)
    paint.setFillColor("white")
    paint.paintText("2020")
    
    paint.setFontSize(20)
    paint.paintText([""])
    paint.setFillColor("black")
    paint.setFontName("Helvetica")
    paint.paintText(["Position for", "Global Growth"])
    canvas.showPage()


def customReport():
    report = CHIReportTemplate('pdf/progressReport.pdf')
    report.pageCallback = titlePageAnnual
    styles = ProgressReportStyles()
    report.changePageStyle("three_col")
    
    report.publish()

# customReport()

progessReport()
