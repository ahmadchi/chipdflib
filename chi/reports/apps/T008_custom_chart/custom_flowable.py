from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm, cm
from reportlab.platypus import (Flowable, Paragraph,
                                SimpleDocTemplate, Spacer)
from reportlab.lib.colors import yellow, green, red, black, white, Color

class LineGraphShaded(Flowable):
    def __init__(self, x=0, y=0, width=100, height=20, text=""):
        Flowable.__init__(self)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        # self.text = text
        # self.styles = getSampleStyleSheet()
        self.aw = 0
        self.ah = 0
        # self.hAlign = 'CENTER'
        
    def coord1(self, x, y, unit=1):
        """
        http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
        Helper class to help position flowables in Canvas objects
        """
        x, y = x * unit, self.height -  y * unit
        return x, y

    # def wrap(self,availWidth,availHeight):
    #     print("w,h ", availWidth, availHeight)

    #     self.aw = availWidth
    #     self.ah = availHeight
    #     self.width = availWidth *.8
    #     self.height = availHeight *.8
    #     return self.width, self.height

    #----------------------------------------------------------------------
    

    def draw(self):
        self.canv.rect(self.x, self.y, self.width, self.height)
        self.canv.circle(0, 0, 5, stroke=1, fill=1)
        pass

    def draw1(self):
        canvas = self.canv
        print("drawing")
        
        canvas.circle(0, 0, 5, stroke=1, fill=1)
        self.canv.rect(self.x, self.y, self.width, self.height)
        # # self.canv.line(self.x, 0, 500, 0)

        # u = inch/10.0
        px = canvas.beginPath()
        p = canvas.beginPath()
        p.circle(50, 50, 50)
        canvas.setLineWidth(20)
        # canvas.clipPath(p)
        

        print("aw,ah ", self.aw, self.aw)
        m = self.height
        if(m > self.width) : m = self.width
        r = 0.4 *m
        px.arc(self.width/2-r, self.height/2-r, self.width/2+r, self.height/2+r, startAng=90, extent=359.5)
        canvas.setLineWidth(20)
        self.canv.setStrokeColorRGB(1, 0, 0)
        canvas.drawPath(px, fill=0, stroke=1)

        # self.canv.setStrokeColorRGB(1, 1, 0)
        # red50transparent = Color( 1, .5, 0, alpha=0.8)
        # self.canv.setStrokeColor(red50transparent)
        # c = canvas
        

        # p = Paragraph(self.text, style=self.styles["Normal"])
        # p.wrapOn(self.canv, self.width, self.height)
        # p.drawOn(self.canv, *self.coord(self.x+2, 10, mm))



