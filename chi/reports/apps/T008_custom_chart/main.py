from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm, cm
from reportlab.platypus import (Flowable, Paragraph,
                                SimpleDocTemplate, Spacer, TopPadder)
from reportlab.lib.colors import yellow, green, red, black, white, Color

from text_container import TextContainer
from reportlab.pdfbase import pdfmetrics

doc = SimpleDocTemplate("pdf/test3.pdf",pagesize=letter, showBoundary=True)
story=[]
styles = getSampleStyleSheet()

px = Paragraph("this is noew text s" * 10, styles["Normal"])
h1 = Paragraph("Heading", styles["h1"])

story.append(px);story.append(px);story.append(px);story.append(px);story.append(px);

# rectDigit = TextContainer("120", "Times-NewRoman", 80, backColor = "#0000FF")
rectDigit1 = TextContainer("1g", "Helvetica-Bold", 80, backColor = "#0000FF", shape = "CIRCLE")
rectDigit2 = TextContainer("120mg", "Times-BoldItalic", 80, backColor = "#FF0000", shape = "RECTANGLE")
# cirleDigit = TextContainer("120", color="#FF00FF", shape = "CIRCLE", fontSize = 12)
# triangleDigit = TextContainer("120", color="#00FF00", shape = "TRIANGLE", fontSize = 12)

px = Paragraph("test" * 10, styles["Normal"])
rectDigit1.hAlign = "LEFT"
rectDigit2.hAlign = "LEFT"

story.append(rectDigit1)
story.append(rectDigit2)
story.append(px)
story.append(Spacer(0, 1*inch))

doc.build(story)