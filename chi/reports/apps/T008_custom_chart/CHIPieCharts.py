from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm
from reportlab.platypus import (Flowable, Paragraph,
                                SimpleDocTemplate, Spacer)


class CHIPieChart(Flowable):
    def __init__(self, x=0, y=-15, width=140, height=115, text=""):
        Flowable.__init__(self)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.styles = getSampleStyleSheet()
        self.aw = 0
        self.ah = 0

    def wrap(self, availWidth, availHeight):
        print("w,h ", availWidth, availHeight)
        self.aw = availWidth
        self.ah = availHeight
        return self.width, self.height

    def pencil(self, canvas):
        u = inch / 10.0
        p = canvas.beginPath()
        p.moveTo(inch, inch)
        print("aw,ah ", self.aw, self.aw)
        p.arc(0, 0, self.aw, self.aw, startAng=180, extent=270)
        canvas.setLineWidth(10)
        self.canv.setStrokeColorRGB(1, 0, 0)

        canvas.drawPath(p, fill=0, stroke=1)

    def draw(self):
        """
        Draw the shape, text, etc
        """
        print("drawing")
        self.pencil(self.canv)
        p = Paragraph(self.text, style=self.styles["Normal"])
        p.wrapOn(self.canv, self.width, self.height)


doc = SimpleDocTemplate("pie_chart.pdf", pagesize=letter)
story = []
styles = getSampleStyleSheet()
circle = CHIPieChart()
story.append(circle)

doc.build(story)
