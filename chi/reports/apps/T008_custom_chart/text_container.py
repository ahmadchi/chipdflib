from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm, cm
from reportlab.platypus import (Flowable, Paragraph,
                                SimpleDocTemplate, Spacer)
from reportlab.lib.colors import yellow, green, red, black, white, Color
from reportlab.pdfbase.pdfmetrics import getAscent, stringWidth
from reportlab.pdfbase import pdfmetrics 
'''
If you set fontsize then set font name also
'''
class TextContainer(Flowable):
    def __init__(self, 
            text, 
            fontName = None, 
            fontSize = None, 
            fontColor = None, 
            backColor = None, 
            shape = "CIRCLE"):
        Flowable.__init__(self)
        if (fontSize == None):
            self.fontSize = 12
        else:
            self.fontSize = fontSize

        self.fontColor = fontColor
        self.backColor = backColor
        
        if (fontName == None):
            self.fontName = "Helvetica"
        else:
            self.fontName = fontName
        self.shape = shape
        self.x = 0
        self.y = 0
        self.width = 250
        self.height = 250
        self.text = text
        # self.compactFontSize = .69 * self.fontSize

        face = pdfmetrics.getFont(self.fontName).face
        ascent = (face.ascent * self.fontSize) / 1000.0
        descent = (face.descent * self.fontSize) / 1000.0
        descent = -descent
        height = ascent + descent # <-- descent it's negative
        # print(height, self.fontSize)
        # exit()

        vGap = .1*fontSize
        # self.vGap = descent#0*.4 * self.compactFontSize
        self.vertTextPosition = descent + vGap

        self.totalHeight = height + 2 * vGap
        
        w = stringWidth(self.text,self.fontName,self.fontSize)
        self.hGap = 0
        if (self.shape == "RECTANGLE"):
            self.totalWidth = w
            self.internalWidth = w
            self.hGap = 0
            self.hGap = self.totalHeight/8
        elif self.shape == "CIRCLE":
            self.hGap = self.totalHeight/2
            self.internalWidth = w
            
            # self.totalHeight # half on left, and half on right
        self.totalWidth = w + 2*self.hGap



    def wrap(self,availWidth,availHeight):
        #print("w,h ", availWidth, availHeight)
        self.aw = availWidth
        self.ah = availHeight
        self.width = availWidth
        print(self.aw, self.ah)
        return self.totalWidth, self.totalHeight
       

    def draw(self):
        print(self.fontSize)

       
        # print(w)
        if( self.fontSize != None and self.fontName!=None):
            self.canv.setFont(self.fontName, self.fontSize)
        
        # self.canv.rect(self.x, self.y, self.width, self.height)


        
        # self.canv.setFillColorRGB(0,0,1)
        # self.canv.rect(self.x, self.y, self.totalWidth, self.totalHeight, stroke=0, fill=1) #
        
        #self.canv.setFillColorRGB(1,0,0)
        self.canv.setFillColor(self.backColor)
        
            
        if self.shape == "CIRCLE":
            self.canv.rect(self.x+self.hGap, self.y, self.internalWidth, self.totalHeight, stroke=0, fill=1) #
            self.canv.circle(self.x + self.hGap, self.totalHeight/2, self.totalHeight/2, stroke=0, fill=1)
            self.canv.circle(self.x + self.hGap+self.internalWidth, self.totalHeight/2, self.totalHeight/2, stroke=0, fill=1)
        else:
            self.canv.rect(self.x, self.y, self.totalWidth, self.totalHeight, stroke=0, fill=1) #
        # self.canv.drawString(0,w/2 - self.fontSize*0, self.text)
        self.canv.setFillColorRGB(0,1,1)
        # self.canv.drawString(self.hGap,self.vGap, self.text)
        self.canv.drawString(self.hGap,self.vertTextPosition, self.text)




        # self.styles = getSampleStyleSheet()
        # self.aw = 0
        # self.ah = 0
        # self.hAlign = 'CENTER'


        # print(self.aw)
        # print(self.ah)
        # aw, ah = self.wrap(50, 60)
        # print(aw, ah)
        # exit()
        # self.text = "iiiiiiiiiiiii"
        
        # print(self.canv.getFont())
        # exit()



        
        # self.canv.setFillColorRGB(0,1,0)
        # self.canv.circle(w/2, w/2, w/2, stroke=1, fill=1)
        # if( self.fontColor != None):
        #     self.canv.setFillColor(self.fontColor)

        
        # face = pdfmetrics.getFont(self.fontName).face
        # ascent = (face.ascent * self.fontSize) / 1000.0
        # descent = (face.descent * self.fontSize) / 1000.0
        # print(ascent, descent)
        # exit()

