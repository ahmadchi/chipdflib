import os
# os.chdir("../../../..")
path = os.getcwd()
print(path)
# exit()
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch, mm
from reportlab.platypus import (Flowable, Paragraph,
                                SimpleDocTemplate, Spacer)
from reportlab.lib.colors import yellow, green, red, black, white
from main import HyperlinkedImage

# from chi.reports.lib.core.chi_image import SVGtoImage
# exit()

# path = os.getcwd()
# print(path)
# os.chdir("../../../..")
# path = os.getcwd()
# print(path)
# exit()

class CHIPiChart(Flowable):
    def __init__(self, x=0, y=-15, width=140, height=115, text=""):
        Flowable.__init__(self)
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.styles = getSampleStyleSheet()
        self.aw = 0
        self.ah = 0
        
    def coord(self, x, y, unit=1):
        """
        http://stackoverflow.com/questions/4726011/wrap-text-in-a-table-reportlab
        Helper class to help position flowables in Canvas objects
        """
        x, y = x * unit, self.height -  y * unit
        return x, y

    def wrap(self,availWidth,availHeight):
        #print("w,h ", availWidth, availHeight)
        self.aw = availWidth
        self.ah = availHeight
        self.width = availWidth
        self.height = availHeight
        return self.width, self.height
       

    def draw(self):
        """
        Draw the shape, text, etc
        """
        canvas = self.canv
        print("drawing")
        # self.bezier(self.canv)
        u = inch/10.0
        p = canvas.beginPath()
        p.moveTo(inch, inch)
        # print("aw,ah ", self.aw, self.aw)
        p.arc(0, 0, self.aw,self.aw, startAng=180, extent=270)
        canvas.setLineWidth(10)
        self.canv.setStrokeColorRGB(1, 0, 0)
        canvas.drawPath(p, fill=0, stroke=1)

        p = Paragraph(self.text, style=self.styles["Normal"])
        p.wrapOn(self.canv, self.width, self.height)
        p.drawOn(self.canv, *self.coord(self.x+2, 10, mm))

    def prescription_images(self):
        image_path = "/home/ubuntu/work/PDFReporting/chipdflib/chi/reports/lib/images/BG.svg"
        # image = SVGtoImage(image_path, 10, 10, 'fit')
        x = 90
        y = 90


doc = SimpleDocTemplate("test3.pdf",pagesize=letter)
story=[]
styles = getSampleStyleSheet()
px = Paragraph("this is noew text s" * 10, styles["Normal"])
story.append(px)
box = CHIPiChart(text="foo")
story.append(box)
story.append(Spacer(0, 1*inch))
# box = BoxyLine(text="bar")
# story.append(box)
doc.build(story)


#exit()

# def bezier(self, canvas):
#     i = inch
#     d = i/4
#     # define the bezier curve control points
#     x1,y1, x2,y2, x3,y3, x4,y4 = d,1.5*i, 1.5*i,d, 3*i,d, 5.5*i-d,3*i-d
#     # draw a figure enclosing the control points
#     canvas.setFillColor(yellow)
#     p = canvas.beginPath()
#     p.moveTo(x1,y1)
#     for (x,y) in [(x2,y2), (x3,y3), (x4,y4)]:
#         p.lineTo(x,y)
#     canvas.drawPath(p, fill=1, stroke=0)
#     # draw the tangent lines
#     canvas.setLineWidth(inch*0.1)
#     canvas.setStrokeColor(green)
#     canvas.line(x1,y1,x2,y2)
#     canvas.setStrokeColor(red)
#     canvas.line(x3,y3,x4,y4)
#     # finally draw the curve
#     canvas.setStrokeColor(black)
#     canvas.bezier(x1,y1, x2,y2, x3,y3, x4,y4)

# canvas.setStrokeColor(black)
# canvas.setLineWidth(4)
# # draw erasor
# canvas.setFillColor(red)
# canvas.circle(30*u, 5*u, 5*u, stroke=1, fill=1)
# # draw all else but the tip (mainly rectangles with different fills)
# canvas.setFillColor(yellow)
# canvas.rect(10*u,0,20*u,10*u, stroke=1, fill=1)
# canvas.setFillColor(black)
# canvas.rect(23*u,0,8*u,10*u,fill=1)
# canvas.roundRect(14*u, 3.5*u, 8*u, 3*u, 1.5*u, stroke=1, fill=1)
# canvas.setFillColor(white)
# canvas.rect(25*u,u,1.2*u,8*u, fill=1,stroke=0)
# canvas.rect(27.5*u,u,1.2*u,8*u, fill=1, stroke=0)
# canvas.setFont("Times-Roman", 3*u)
# canvas.drawCentredString(18*u, 4*u, text)
# # now draw the tip
# # penciltip(canvas,debug=0)
# # draw broken lines across the body.
# canvas.setDash([10,5,16,10],0)
# canvas.line(11*u,2.5*u,22*u,2.5*u)
# canvas.line(22*u,7.5*u,12*u,7.5*u)    

# self.canv.rect(self.x, self.y, self.width, self.height)
# self.canv.line(self.x, 0, 500, 0)
# def arcs(canvas):

# self.canv.setLineWidth(4)
# self.canv.setStrokeColorRGB(0.8, 1, 0.6)
# self.canv.rect(inch, inch, 1.5*inch, inch)
# self.canv.rect(3*inch, inch, inch, 1.5*inch)
# self.canv.setStrokeColorRGB(0, 0.2, 0.4)

