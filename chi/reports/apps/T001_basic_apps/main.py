import sys
sys.path.append("../../")

from lib.report_generator import ReportGenerator
from io import BytesIO

def main():

    buffer = BytesIO()

    report = ReportGenerator(buffer, 'Letter')
    pdf = report.print_users(["ahmad", "ali"])
    f = open("composer.pdf", 'wb')
    f.write(pdf)
    print("done")
    
main()