import arabic_reshaper
from bidi.algorithm import get_display
from reportlab.platypus import SimpleDocTemplate, Paragraph
from reportlab.pdfbase import pdfmetrics
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_RIGHT
from reportlab.lib.pagesizes import A4
from reportlab.pdfbase.ttfonts import TTFont
arabic_text = u'Test ABC إذا أخذنا بعين الإعتبار طبيعة تقلب المناخ و المتغيرات البينية السنوية و تلك على المدى الطويل إضافة إلى عدم دقة القياسات والحسابات المتبعة'

# arabic_text = u'Test ABC sdfsdf '
# for c in arabic_text:
#     print(int(c))
# exit()

arabic_text = arabic_reshaper.reshape(arabic_text) # join characters
arabic_text = get_display(arabic_text) # change orientation by using bidi

# pdf_file=open('disclaimer.pdf','w')
pdf_doc = SimpleDocTemplate('disclaimer.pdf', pagesize=A4)
pdfmetrics.registerFont(TTFont('Arabic-normal', 'fonts/trado.ttf'))
style = ParagraphStyle(name='Normal', fontName='Arabic-normal', fontSize=32, leading=32. * 1.2)
style.alignment=TA_RIGHT

# arabic_text = get_display(arabic_text)
pdf_doc.build([Paragraph(arabic_text, style)])
# pdf_file.close()