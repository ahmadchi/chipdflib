

from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import letter
from reportlab.platypus import Paragraph, SimpleDocTemplate, Spacer,PageBreak
from reportlab.lib.styles import ParagraphStyle
from reportlab.pdfbase import pdfmetrics
import reportlab
from reportlab.pdfbase.ttfonts import TTFont


import arabic_reshaper
from bidi.algorithm import get_display

# pdfmetrics.registerFont(TTFont('Arabic', 'fonts/FontsFree-Net-bokra.ttf'))

# tradbdo
pdfmetrics.registerFont(TTFont('Arabic', 'fonts/trado.ttf'))
#init the style sheet
styles = getSampleStyleSheet()


arabic_text_style = ParagraphStyle(
    'border', # border on
    parent = styles['BodyText'] , # Normal is a defaul style  in  getSampleStyleSheet
    borderColor= '#333333',
    borderWidth =  1,
    borderPadding  =  2,
    # fontSize = 30,
    lineSpacing = 35,
    lineHeight = 35,
    fontName="Arabic" #previously we named our custom font "Arabic"
)

storys = []



rehaped_text =""" عندما يريد العالم أن ‪يتكلّم ‬ ، فهو يتحدّث بلغة
 يونيكود. تسجّل الآن لحضور المؤتمر الدولي العاشر ليونيكود (Unicode Conference)، الذي سيعقد في 10-12 آذار 1997 بمدينة مَايِنْتْس، ألمانيا. و سيجمع المؤتمر بين خبراء
  من كافة قطاعات الصناعة على الشبكة العالمية انترنيت ويونيكود، حيث ستتم، على الصعيدين الدولي والمحلي على حد سواء مناقشة سبل استخدام يونكود في النظم القائمة وفيما يخص التطبيقات الحاسوبية، الخطوط، تصميم النصوص والحوسبة متعددة اللغات."""

# reshape the text 
rehaped_text = arabic_reshaper.reshape(rehaped_text)
bidi_text = get_display(rehaped_text)
# bidi_text = rehaped_text

# add the text to pdf
 ## dont forget to add the style arabic_text_style
storys.append(Paragraph(bidi_text,arabic_text_style))
storys.append(Spacer(1,8)) # set the space here
storys.append(Paragraph(bidi_text,arabic_text_style))


doc = SimpleDocTemplate('mydoc.pdf',pagesize = letter)
## add the storys array to the pdf document
doc.build(storys)




# rehaped_text = arabic_reshaper.reshape(arabic_text)
# storys.append(Paragraph(rehaped_text,arabic_text_style))
# storys.append(Spacer(1,8))
# storys.append(Paragraph(rehaped_text,arabic_text_style))



# bidi_text = get_display(arabic-text)

# storys.append(Paragraph(bidi_text,arabic_text_style))
# storys.append(Spacer(1,8))
# storys.append(Paragraph(bidi_text,arabic_text_style))