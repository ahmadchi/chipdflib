import os
import json

from chi.reports.lib.patient_progress_report.patient_progress_report import PatientProgressReport

def main(type = "single"):
    base_path = os.getenv('BASE_PATH', '')
    app_path = os.path.join(base_path, 'apps/T007_progress_report')
    jsonPath = app_path + "/json/"


    patientDataArr = []

    if type == "single":
    
        jsonFiles = [
            # "000_GeorgeBush.json",
            # "000_DonaldTrump.json",
            # "000_GeorgeBush.json",
            # "000_DonaldTrump.json",
            # "000_ImranKhan.json",
            # "json_temp.json",
            "newDataformat.json"
        ]

        count = 0
        for fileName in jsonFiles:
            jsonFile = open(jsonPath + fileName, "r")
            patientData = json.load(jsonFile)
            patientData['plot_file_path'] = f'{app_path}/temp_svg/tempGraph.svg'
            patientDataArr.append(patientData)
            jsonFile.close()
            count += 1

    else:
        fileName = "20nov1110_patient_report_data.json"
        jsonFile = open(jsonPath + fileName, "r")
        patientDataArr = json.load(jsonFile)
        # patientDataArr = [patientDataArr[21],patientDataArr[22],patientDataArr[23]]

    count = 0
    for patientData in patientDataArr:
        patientData['plot_file_path'] = f'{app_path}/temp_svg/tempGraph.svg'
        count += 1
        name = patientData['patient_info']["full_name"]
        name = name.replace(" ", "")
        with open("json_data_cleaned/"+str(count)+"_"+ name+".json", "w") as jsonFile:
            json.dump(patientData, jsonFile, indent=4, sort_keys=True)

    report = PatientProgressReport(patientDataArr)
    pdfData = report.getPDF()
    report.close()

    file = open(f"{app_path}/pdf/main.pdf", 'wb')
    file.write(pdfData)
    print("done")

