from reportlab.rl_config import defaultPageSize
from reportlab.platypus import BaseDocTemplate, Frame, Paragraph, PageBreak, PageTemplate, SimpleDocTemplate, Spacer, \
    NextPageTemplate
from reportlab.lib.styles import getSampleStyleSheet
import random
from reportlab.lib.units import inch
from reportlab.lib.colors import blue, red

# from resources.examples.main1 import CHIReportTemplate

PAGE_HEIGHT = defaultPageSize[1]
PAGE_WIDTH = defaultPageSize[0]
styles = getSampleStyleSheet()

Title = "Do something awesome"
pageinfo = "platypus example"

def myFirstPage(canvas, doc):
 canvas.saveState()
 canvas.setFillColor(blue)
 canvas.setFont('Times-Bold',36)
 canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-108, Title)
 canvas.setFont('Times-Roman',9)
 # image = Image("assets/images/logo2x.png", width=2*inch, height=2*inch)
 # canvas.drawInlineImage(image, 10,10, width=None,height=None) 
 canvas.drawString(inch, 0.75 * inch, "First Page / %s" % pageinfo)
 canvas.restoreState()

def myLaterPages(canvas, doc):
 canvas.saveState()
 canvas.setFont('Times-Roman',9)
 canvas.drawString(inch, 0.75 * inch, "Page %d %s" % (doc.page, pageinfo))
 canvas.restoreState()

class CHIReportTemplate(SimpleDocTemplate):

  def __init__(self, cwd, showBoundary=1):
    SimpleDocTemplate.__init__(self, cwd, showBoundary=1)
    self.pageNumber = 0

  def handle_afterPage(self):
    print("...... after page")


  def handle_nextPageTemplate(self, PageTemplateName):
    SimpleDocTemplate.handle_nextPageTemplate(self, PageTemplateName)
    print("template here I am = ", self.pageNumber, PageTemplateName);
    
  def handle_pageBegin(self):
    SimpleDocTemplate.handle_pageBegin(self)
    # self.canv.showPage()
    self.canv.saveState()
    self.pageNumber+=1
    if self.pageNumber %2 ==0:
      myFirstPage(self.canv, self)
    else:
      myLaterPages(self.canv, self)
    print("here I am = ", self.pageNumber);
    self.canv.restoreState()

def divide_page():
    words = "In number theory, Fermat's Last Theorem (sometimes called Fermat's conjecture, especially in older " \
            "texts) states that no three positive integers a, b, and c satisfy the equation an + bn = cn for any " \
            "integer value of n greater than 2. The cases n = 1 and n = 2 have been known since antiquity to have " \
            "infinitely many solutions.[1] "

    wordsTxt = words * 10

    text = []
    doc = CHIReportTemplate('splitting_pages2.pdf', showBoundary=1)
    # Two Columns

    leftMargin = doc.leftMargin
    frame1 = Frame(leftMargin, doc.bottomMargin, doc.width / 2 - 6, doc.height, id='col1')
    frame2 = Frame(leftMargin + doc.width / 2 + 6, doc.bottomMargin, doc.width / 2 - 6, doc.height, id='col2')
    
    frame3 = Frame(leftMargin + 0 * doc.width / 3 + 6, doc.bottomMargin, 1 * doc.width / 3 -6, doc.height, id='col3')
    frame4 = Frame(leftMargin + 1 * doc.width / 3 + 6, doc.bottomMargin, 1 * doc.width / 3 -6, doc.height, id='col4')
    frame5 = Frame(leftMargin + 2 * doc.width / 3 + 6, doc.bottomMargin, 1 * doc.width / 3 -6, doc.height, id='col5')
    
    twoColumnPage = PageTemplate(id='two_col', frames=[frame2, frame1])
    threeColumnPage = PageTemplate(id='three_col', frames=[frame3, frame4, frame5])

    doc.addPageTemplates([twoColumnPage, threeColumnPage])
    text.append(Paragraph(wordsTxt, styles['Normal']))
    text.append(PageBreak())
    text.append(NextPageTemplate('three_col'))
    text.append(Paragraph(wordsTxt, styles['Italic']))
    text.append(PageBreak())
    text.append(Paragraph(wordsTxt, styles['BodyText']))
    text.append(PageBreak())
    
    text.append(Paragraph(words * 2, styles['Normal']))
    text.append(PageBreak())
    doc.build(text)


def main():
    divide_page()


main()
