import sys

sys.path.append("../../")
# from data.processJson import *
import json
from lib.report_generator import ReportGenerator
from io import BytesIO

f = open("data/progress_report_ref.json", "r")
data = json.load(f)


def main():
    buffer = BytesIO()

    report = ReportGenerator(buffer, 'Letter')
    pdf = report.print_users(["ahmad", "ali"])
    f = open("pdf/repGenerator.pdf", 'wb')
    f.write(pdf)
    print("done")


main()

# print(data["messages"])
# print(data["medicine_history"])
# print(data["patient_info"])
# print(data["doctor_info"])
# print(data["diagnoses"])

# for vital in data["recent_result"]:
# 	print("==============")
# 	print(vital["title"])
# 	print("==============")
# 	for k in vital:
# print("  > ", k, vital[k])

# print(data["allergies"])
# print(data["compliance_data"])
# print(data["active_medications"])


# processMessages(data["messages"])

# processVitals(data["recent_result"])

# for msg in data["messages"]:
# processMessages(msg["subjects"])


report = ReportGenerator("progress.pdf")

report.addHeader()

# report.addProgressTitlePage(patientData);

# report.addVitalData(messageData);


# report.addMessageData(messageData);
