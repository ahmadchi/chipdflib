
def processVitals(vitalData):
	for vital in vitalData:
		print("==============")
		print(vital["title"])
		print("==============")
		for k in vital:
			print("  > ", k ,":",  vital[k])


def processMessages(msgData):
	for item in msgData:
		print("==============")
		print(item["title"])
		print("==============")
		for k in item:
			if ( k !="messages"):
				print("  > ", k, "::" ,item[k])
			if ( k =="messages"):
				for v in item[k]:
					print("\n     ----", v.keys())
					for k1 in v:
						val = v[k1]
						if(k1 == "message"):
							val = val.strip()
						print("           >>>>>> ", k1, ": ", val)
