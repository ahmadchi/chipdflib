from reportlab.lib.enums import TA_LEFT, TA_RIGHT
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus.frames import ShowBoundaryValue
from reportlab.rl_config import defaultPageSize

from reportlab.platypus import Frame, Paragraph, PageBreak, PageTemplate, SimpleDocTemplate, Spacer, \
    NextPageTemplate, FrameBreak

from reportlab.platypus import Frame, Paragraph, PageBreak, PageTemplate, SimpleDocTemplate, Spacer, NextPageTemplate, FrameBreak

from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
import random
from reportlab.lib.units import inch

from reportlab.lib.colors import blue, red

from reportlab.lib.colors import blue, red, black

import datetime

PAGE_HEIGHT = defaultPageSize[1]
PAGE_WIDTH = defaultPageSize[0]
styles = getSampleStyleSheet()

Title = ""
# Title = "Do something awesome"

Title = "Let's Do it"

pageinfo = "platypus example"


def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.setFillColor(blue)
    canvas.setFont('Times-Bold', 36)

    canvas.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 108, Title)
    canvas.setFont('Times-Roman', 9)

    canvas.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - 50, Title)
    canvas.setFont('Times-Roman', 9)
    margin = 10
    canvas.setFillColor(black)

    canvas.setFont('Helvetica-Oblique', 20)
    canvas.drawRightString(PAGE_WIDTH - 2 * margin, PAGE_HEIGHT - 40, "PROGRESS")
    canvas.drawRightString(PAGE_WIDTH - 2 * margin, PAGE_HEIGHT - 60, "REPORT")
    canvas.drawImage("../../images/logo1x.png", margin, PAGE_HEIGHT - 70 - margin, 70, 70, mask='auto')
    canvas.setFont('Times-Roman', 9)
    # S = A4
    # print(S)
    # margin = 10

    # image = Image("assets/images/logo2x.png", width=2*inch, height=2*inch)
    # canvas.drawInlineImage(image, 10,10, width=None,height=None)
    canvas.drawString(inch, 0.75 * inch, "First Page / %s" % pageinfo)
    canvas.restoreState()

    # print("here I am = ", self.pageNumber)


def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman', 9)
    canvas.drawString(inch, 0.75 * inch, "Page %d %s" % (doc.page, pageinfo))
    canvas.restoreState()


class CHIReportTemplate(SimpleDocTemplate):

    def __init__(self, cwd, showBoundary=1):
        SimpleDocTemplate.__init__(self, cwd, showBoundary=1)

        self.pageNumber = 0

        self.pageNumber = 5

    def handle_afterPage(self):
        print("...... after page")

    def handle_nextPageTemplate(self, PageTemplateName):
        SimpleDocTemplate.handle_nextPageTemplate(self, PageTemplateName)
        print("template here I am = ", self.pageNumber, PageTemplateName)

    def handle_pageBegin(self):
        SimpleDocTemplate.handle_pageBegin(self)
        # self.canv.showPage()
        self.canv.saveState()
        self.pageNumber += 1
        if self.pageNumber % 2 == 0:
            myFirstPage(self.canv, self)

        else:
            myLaterPages(self.canv, self)
        print("here I am = ", self.pageNumber)

        print("...... first page", self.pageNumber)

        # else:
        # myLaterPages(self.canv, self)
        # print("...... second page", self.pageNumber)

        # self.canv.restoreState()


message_subject = ParagraphStyle('yourtitle',

                                 fontName="Helvetica",
                                 fontSize=12,
                                 backColor='#FFFF00',
                                 borderRadius=0,
                                 borderColor=None,
                                 borderWidth=1,
                                 borderPadding=5,
                                 parent=styles['Normal'],
                                 alignment=TA_LEFT,
                                 spaceAfter=20,
                                 textcolor='#FFFFFF')

message_body = ParagraphStyle('yourtitle',
                              fontName="Helvetica",
                              fontSize=12,
                              backColor='#D3D3D3',
                              borderColor=None,
                              borderWidth=0.5,
                              borderPadding=5,
                              parent=styles['Normal'],
                              alignment=1,
                              spaceAfter=20,
                              )


def divide_page():
    words = "In number theory, Fermat's Last Theorem (sometimes called Fermat's conjecture, especially in older " \
            "texts) states that no three positive integers a, b, and c satisfy the equation an + bn = cn for any " \
            "integer value of n greater than 2. The cases n = 1 and n = 2 have been known since antiquity to have " \
            "infinitely many solutions.[1] "

    wordsTxt = words[:60]
    message = wordsTxt
    text = []

    doc = CHIReportTemplate('pdf/messages.pdf', showBoundary=0)
    boundary = ShowBoundaryValue(color="transparent");
    leftMargin = doc.leftMargin
    frame1 = Frame(leftMargin, doc.bottomMargin-40, doc.width / 2 - 6, doc.height, id='col1', showBoundary=boundary)
    frame2 = Frame(leftMargin + doc.width / 2 + 6, doc.bottomMargin-40, doc.width / 2 - 6, doc.height, id='col2', showBoundary=boundary)
    twoColumnPage = PageTemplate(id='two_col', frames=[frame1, frame2])
    doc.addPageTemplates([twoColumnPage])
    # Two Columns
    for i in range(0, 10):
        subject = f" {i}. RBG WARNING HIGH"

        leftMargin = doc.leftMargin

        frame1 = Frame(leftMargin, doc.bottomMargin, doc.width / 2 - 6, doc.height, id='col1',
                       showBoundary=ShowBoundaryValue(color="transparent"))
        frame2 = Frame(leftMargin + doc.width / 2 + 6, doc.bottomMargin, doc.width / 2 - 6, doc.height, id='col2',
                       showBoundary=ShowBoundaryValue(color="transparent"))

        boundary = ShowBoundaryValue(color="transparent");
        # boundary = None
        frame1 = Frame(leftMargin, doc.bottomMargin - 40, doc.width / 2 - 6, doc.height, id='col1',
                       showBoundary=boundary)
        frame2 = Frame(leftMargin + doc.width / 2 + 6, doc.bottomMargin - 40, doc.width / 2 - 6, doc.height, id='col2',
                       showBoundary=boundary)

        text.append(Paragraph(subject, message_subject))
        text.append(Paragraph(message, message_body))
        text.append(Paragraph(message, message_body))
        text.append(NextPageTemplate('two_col'))

    doc.build(text)


def main():
    divide_page()


main()

if __name__ == '__main__':
    divide_page()
