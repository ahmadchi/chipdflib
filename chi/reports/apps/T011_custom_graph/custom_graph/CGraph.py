from reportlab.platypus import (Flowable, Paragraph, SimpleDocTemplate, Spacer)
from reportlab.lib.pagesizes import letter
from reportlab.graphics.charts.axes import Color

class CGraph(Flowable):
    def __init__(self, story, width, height):
        Flowable.__init__(self)
        self.width = width
        self.height = height
        self.story = story
        self.Padding = {"left": 30, "right": 30, "top": 30, "bottom": 20}
        self.Init()

    def Init(self):
        self.pX = self.Padding['left']
        self.pY = self.Padding['bottom']
        self.pHeight = self.height - (self.Padding['top'] + self.Padding['bottom'])
        self.pWidth = self.width - (self.Padding['left'] + self.Padding['right'])

    def wrap(self, availWidth, availHeight):
        print("w,h ", availWidth, availHeight)
        self.aw = availWidth
        self.ah = availHeight
        return self.width, self.height

    def DrawRectangle(self, xy, wh, stroke_color = (0.1, 0.1, 0.1, 0.9), color=(0.8, 0.1, 0.1, 0.9), fill=0, stroke=1):
        self.canv.saveState()
        self.canv.setStrokeColor(Color(*stroke_color))
        self.canv.setFillColor(Color(*color))
        p = self.canv.beginPath()
        p.rect(*xy, *wh)
        self.canv.drawPath(p, fill=fill, stroke=stroke)
        p.close()
        self.canv.restoreState()

    def draw(self):
        """
        Draw the shape, text, etc
        """
        self.DrawRectangle((0, 0), (self.width, self.height))
        self.DrawRectangle((self.pX, self.pY), (self.pWidth, self.pHeight), color=(0.1, 0.2, 0.1, 0.9), fill=1)


doc = SimpleDocTemplate("custom_graph.pdf", pagesize=letter)
story = []
p = Paragraph("This is a table. " * 10)
story.append(p)
graph = CGraph(story, width=500, height=250)
story.append(graph)
story.append(p)
s = Spacer(width=0, height=60)
doc.build(story)