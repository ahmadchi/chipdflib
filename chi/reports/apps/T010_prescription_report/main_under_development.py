import os
import json

from chi.reports.lib.prescription.prescription import Prescription

# TODO: remove following 2 functions to utils
def getAppPath():
    base_path = os.getenv('BASE_PATH', '')
    app_path = os.path.join(base_path, 'apps/T010_prescription_report')
    return app_path


def loadJsonData(appPath, fName):
    jsonPath = appPath + "/" + fName
    jsonFile = open(jsonPath, "r")
    prescData = json.load(jsonFile)
    return prescData


def configureBlueLetterhead():
    letterheadDict = {
        "page_size" : [595, 842],
        "leftMargin" : 20,
        "rightMargin" : 20,
        "topMargin" : 20,
        "bottomMargin" : 20,
        "blocks": [
            {
                "type": "TEXT",
                "anchorX": 440,
                "anchorY": 806,
                "fontName": 'Helvetica-Bold',
                "fontSize": 15,
                "align": "LEFT",
                "text": ["Dr. Hakim Ali"],
            },
            {
                "type": "TEXT",
                "anchorX": 440,
                "anchorY": 790,
                "fontName": 'Helvetica',
                "fontSize": 10,
                "align": "LEFT",
                "text": ["Neuro Physician", "Ph: 051-9988554 Ext 3323", "+92-300-7766776"]
            },
            {
                "type": "TEXT",
                "anchorX": 250,
                "anchorY": 80,
                "fontName": 'Helvetica',
                "fontSize": 10,
                "align": "LEFT",
                "text": ["Printed by Maryam Raza Akhtar on Dated: 28-Nov-2001", "For Appointments call",
                         "(051) 111 121 154 -Ext : 3232 "]
            },

            {
                "type": "TEXT",
                "anchorX": 440,
                "anchorY": 750,
                "fontName": 'Helvetica-Bold',
                "fontSize": 10,
                "align": "LEFT",
                "text": ["Dated: 16/06/2020 12:45 PM"]
            },

            {
                "type": "TEXT",
                "anchorX": 297,
                "anchorY": 767,
                "fontName": 'Helvetica-Bold',
                "fontSize": 10,
                "align": "CENTER",
                "text": ["Patient: Imran Khan"]
            },

            {
                "type": "SEPARATOR",
                "anchorY": 720
            },

            {
                "type": "RECT",
                "x": 30,
                "y": 30,
                "w": 100,
                "h": 200,
                "color": "#448888",
                "line_width": 1,
                "transparent": 1
            },
            {
                "type": "RECT",
                "x": 30,
                "y": 30,
                "w": 100,
                "h": 200,
                "color": "#448888",
                "line_width": 1,
                "transparent": 1
            },

            {
                "type": "IMAGE",
                "filename": "logo1x.png",
                "x": 30,
                "y": 771,
                "w": 50,
                "h": 50,
                "file_type": "png"
            },
            {
                "type": "LOGO_TEXT",
                "anchorX": 55,
                "anchorY": 767,
                "fontName": 'Helvetica',
                "fontSize": 7,
                "align": "CENTER",
                "text": ["Cognitive", "Healthcare", "International"]
            },
            {
                "type": "IMAGE_LOGO",
                "filename": "updated-logo.svg",
                "x": 0,
                "y": 0,
                "w": 200,
                "h": 200,
                "file_type": "svg"
            },
            {
                "type": "IMAGE",
                "filename": "Left_Bottom_Arch.svg",
                "x": 0,
                "y": 0,
                "w": 300,
                "h": 300,
                "file_type": "svg"
            },
            {
                "type": "IMAGE_GREY",
                "filename": "BG_Arch.svg",
                "x": 0,
                "y": 60,
                "w": 700,
                "h": 600,
                "file_type": "svg"
            },

            {
                "type": "IMAGE_GREY",
                "filename": "BG_Arch.svg",
                "x": 0,
                "y": 60,
                "w": 700,
                "h": 600,
                "file_type": "svg"
            }
        ]
    }
    return letterheadDict


def blueLetterhead():
    app_path = getAppPath()
    prescData = loadJsonData(app_path, "data/data1.json")
    patientDataArr = [prescData]
    letterheadDict = configureBlueLetterhead()

    # # physician data
    # letterheadDict["blocks"][0]["text"]    = "Ahmad Hassan"
    # letterheadDict["blocks"][1]["text"][0] = "Neuro Physician"
    # letterheadDict["blocks"][1]["text"][1] = "Ph: 051-9988554 Ext 3323"
    # letterheadDict["blocks"][1]["text"][2] = "+92-300-7766776"

    # Patient Data


    report = Prescription(patientDataArr, letterheadDict)

    pdfData = report.getPDF()
    report.close()

    file = open(f"{app_path}/pdf/prescription.pdf", 'wb')
    file.write(pdfData)
    print("done")



def main():
    blueLetterhead()
