from dotenv import load_dotenv

if __name__ == "__main__":
    load_dotenv(verbose=True)

    from chi.reports.apps.T010_prescription_report.main import main
    main()
