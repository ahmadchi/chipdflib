from dotenv import load_dotenv

if __name__ == "__main__":
    load_dotenv(verbose=True)

    from chi.reports.apps.T007_progress_report.main import main 
    #main("single")
    main("multiple")
