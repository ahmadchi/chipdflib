from reportlab.platypus import SimpleDocTemplate
from svglib.svglib import svg2rlg
import matplotlib.pyplot as pyplot

def svg_demo(image_path, output_path):
    drawing = svg2rlg(image_path)
    
    doc = SimpleDocTemplate(output_path,
                            rightMargin=0,
                            leftMargin=0)
    
    story = []
    story.append(drawing)
    
    doc.build(story)

def create_matplotlib_svg(plot_path):
    pyplot.plot(list(range(5)))
    pyplot.title = 'matplotlib SVG + ReportLab'
    pyplot.ylabel = 'Increasing numbers'
    pyplot.savefig(plot_path, format='svg')


if __name__ == '__main__':
    svg_path = 'matplot.svg'
    create_matplotlib_svg(svg_path)
    (x,y) = textobject.getCursor()
    svg_demo(svg_path, 'matplot.pdf')