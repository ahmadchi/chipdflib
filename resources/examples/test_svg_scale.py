#https://dzone.com/articles/adding-svgs-to-pdfs-with-python-and-reportlab


from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
import os
from reportlab.graphics import renderPDF, renderPM
from reportlab.platypus import SimpleDocTemplate
from svglib.svglib import svg2rlg

def svg_demo(image_path, output_path):
    drawing = svg2rlg(image_path)
    xL, yL, xH, yH = drawing.getBounds()

    xsize = 50;
    ysize = 50;
    sx = xsize / (xH - xL)
    sy = ysize / (yH - yL)
    
    drawing.scale(sx, sy)
    c = canvas.Canvas("test_svg_scale.pdf")
    renderPDF.draw(drawing, c, 60, 500)
    c.showPage()
    c.save()


lyrics = [ 
	"well she hit Net Solutions well",
	"and she registered her own .com site now",
	"and filled it up with yahoo profile pics",
	"she snarfed in one night now",
	"and she made 50 million when Hugh Hefner",
	"bought up the rights now",
	"and she'll have fun fun fun",
	"til her Daddy takes the keyboard away",
	"With many apologies to the Beach Boys",
	"and anyone else who finds this objectionable",
]

def cursormoves1(canvas):
	textobject = canvas.beginText()
	textobject.setTextOrigin(inch, 5.5*inch)
	textobject.setFont("Helvetica-Oblique", 14)
	for line in lyrics:
		textobject.textLine(line)
		textobject.moveCursor(14,0)
	textobject.setFillGray(0.4)
	textobject.textLines('''
	With many apologies to the Beach Boys and anyone else who finds this objectionable
	''')
	canvas.drawText(textobject)

c = canvas.Canvas("hello.pdf")
cursormoves1(c)
# c.showPage()
# hello(c)
# c.showPage()


c.save()


svg_demo('assets/images/bp_2.svg', 'svg_demo2.pdf')