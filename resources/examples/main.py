# from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.platypus import Image

from reportlab.lib.colors import blue
from reportlab.lib.pagesizes import LETTER
from reportlab.lib.units import inch
from reportlab.pdfgen.canvas import Canvas

from reportlab.lib.styles import getSampleStyleSheet

from reportlab.platypus import Paragraph, Frame
from reportlab.lib.pagesizes import A4,LETTER
S = A4
print(S)
margin = 10

def textBox(canvas, strArray, startx, starty, fontName, fontSize, align = "RIGHT"):
	textobject = canvas.beginText()
	textobject.setTextOrigin(startx, starty)
	textobject.setFont("Helvetica-Oblique", 14)
	for line in strArray:
		textobject.textLine(line)
		textobject.moveCursor(fontSize,0)
	# textobject.setFillGray(0.4)
	textobject.textLines('''
	With many apologies to the Beach Boys and anyone else who finds this objectionable
	''')
	canvas.drawText(textobject)
styles = getSampleStyleSheet()
styleN = styles['Normal']
styleH = styles['Heading1']
story = []
#add some flowables
im = Image("assets/images/logo1x.png", width=inch, height=inch)
# im.hAlign = 'CENTER'
# story.append(im)

story.append(Paragraph("This is a Heading",styleH))
story.append(Paragraph("This is a paragraph in <i>Normal</i> style. This is a paragraph in <i>Normal</i> style. This is a paragraph in <i>Normal</i> style. This is a paragraph in <i>Normal</i> style. This is a paragraph in <i>Normal</i> style. This is a paragraph in <i>Normal</i> style. ",
styleN))

c = Canvas('main.pdf', pagesize=A4)
c.drawImage("assets/images/logo1x.png", margin, S[1]-70-margin, 70, 70, mask='auto')

# c.setFont(c._fontname, 20)
c.setFont('Helvetica-Oblique', 20)
c.drawRightString(S[0]-2*margin,S[1]-40,"PROGRESS")
c.drawRightString(S[0]-2*margin,S[1]-60,"REPORT")
c.showPage()
# f = Frame(inch, inch, 6*inch, 9*inch, showBoundary=1)
f = Frame(0, 0, 6*inch, 9*inch, showBoundary=1)
f.addFromList(story, c)


c.saveState()
c.setStrokeColor((1,0,0))
c.rect(0,0,c._pagesize[0],c._pagesize[1],stroke=1)
c.restoreState()
c.save()



# [7:37 AM] Ahmad Hassan
#     https://eric.sau.pe/reportlab-and-django-–-part-3-–-paragraphs-and-tables
# ReportLab and Django – Part 3 – Paragraphs and Tables | Eric SaupeReportLab is full of different objects that you can place anywhere around the screen.  Following our previous code, we are making a list of elements that we want to draw onto a document.  ReportLab...eric.sau.pe


from reportlab.pdfbase import pdfmetrics
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.pagesizes import A4

top_margin = A4[1] - cm

def run(mode):
	enc = 'WinAnsi'
	canv = canvas.Canvas('out_a.pdf')
	canv.setPageCompression(0)
	faceName = 'Symbol'
	encLabel = 'StandardEncoding'
	symbolFontName = faceName + '-' + encLabel
	pdfmetrics.registerFont(pdfmetrics.Font(symbolFontName,faceName,encLabel))
	canv.setFont(symbolFontName, 14)
	textobj = canv.beginText(inch, top_margin)
	textobj.textOut("str(chr(191)) + str(chr(190))")
	canv.drawText(textobj)
	lastX = textobj.getX()
	print(lastX)
	canv.showPage()
	canv.save()
	
run(0)

