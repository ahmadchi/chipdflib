
import json

json1 = "[{\"start_date\": 1600236089, \"repeat_type\": \"Repeat\", \"repeat_cycle\": \"Day\", \"repeat_unit\": 1, \"frequency\": \"Daily\", \"days\": [{\"type\": \"Daily\", \"day\": \"Day\", \"times\": [{\"time\": 65700, \"dosage\": 1, \"index\": 89, \"h_index\": 22, \"m_index\": 1}]}], \"dosage_type\": \"Cumulative\", \"dosage\": 12, \"repeat_cycle_custom\": \"Day\", \"repeat_unit_custom\": 1, \"is_advance\": false, \"end_type\": \"Never\", \"end_value\": null}]"

x = json.loads(json1)
# print(len(x))

# 'start_date', 'repeat_type', 'repeat_cycle', 'repeat_unit', 'frequency', 'days', 'dosage_type', 'dosage', 'repeat_cycle_custom', 'repeat_unit_custom', 'is_advance', 'end_type', 'end_value']
# print(x[0].keys